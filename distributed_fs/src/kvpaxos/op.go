package kvpaxos

import (
	"util"
	"paxos"
	"time"
)


/*
The base op that all ops need to implement. The dispatchers go routines can only execute ops that follow this standard.
*/
type BaseOp interface {
	Initialize();
	Execute();
	DoneCallback();
	IsDone() bool;
	WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool;
}


/**********************************************************************************************************************/
// This struct defines the Put Operation.
/**********************************************************************************************************************/
type PutOp struct {
	clientArgs *PutArgs;
	dispatcher *util.Dispatcher;
}

func NewPutOp(clientArgs *PutArgs, // The client args.
						  dispatcher *util.Dispatcher, // The dispatcher used by the KV store.
						 ) *PutOp {
	putOp := new(PutOp);
	putOp.clientArgs = clientArgs;
	putOp.dispatcher = dispatcher;
	putOp.Initialize();
	return putOp;
}

func (putOp *PutOp) Initialize() {
	return;
}

func (putOp *PutOp) Execute() {
	return;
}

func (putOp *PutOp) DoneCallback() {
	return;
}

func (putOp *PutOp) IsDone() bool {
	return false;
}

func (putOp *PutOp) WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool {
	return false;
}


/**********************************************************************************************************************/
// This struct defines the Get Operation.
/**********************************************************************************************************************/
type GetOp struct {
	clientArgs *PutArgs;
	dispatcher *util.Dispatcher;
}

func NewGetOp(clientArgs *GetArgs, // The client's get args.
						  dispatcher *util.Dispatcher, // The dispatcher used by the KV store.
						 ) *GetOp {
	getOp := new(GetOp);
	getOp.clientArgs = clientArgs;
	getOp.dispatcher = dispatcher;
	getOp.Initialize();
	return getOp;
}

func (getOp *GetOp) Initialize() {
	return;
}

func (getOp *GetOp) Execute() {
	return;
}

func (getOp *GetOp) DoneCallback() {
	return;
}

func (getOp *GetOp) IsDone() bool {
	return false;
}

func (getOp *GetOp) WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool {
	return false;
}


/**********************************************************************************************************************/
// This struct defines the Delete Operation.
/**********************************************************************************************************************/
type DeleteOp struct {
	clientArgs *PutArgs;
	dispatcher *util.Dispatcher;
}

func NewDeleteOp(clientArgs *DeleteArgs, // The client's delete args.
								 dispatcher *util.Dispatcher, // The dispatcher used by the client.
								) *DeleteOp {
	deleteOp := new(DeleteOp);
	deleteOp.clientArgs = clientArgs;
	deleteOp.dispatcher = dispatcher;
	deleteOp.Initialize();
	return deleteOp;
}

func (deleteOp *DeleteOp) Initialize() {
	return;
}

func (deleteOp *DeleteOp) Execute() {
	return;
}

func (deleteOp *DeleteOp) DoneCallback() {
	return;
}

func (deleteOp *DeleteOp) IsDone() bool {
	return false;
}

func (deleteOp *DeleteOp) WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool {
	return false;
}


/**********************************************************************************************************************/
// This struct defines No Operation.
/**********************************************************************************************************************/
type NoOp struct {
	dispatcher *util.Dispatcher;
}

func NewNoOp(dispatcher *util.Dispatcher) *NoOp {
	noOp := new(NoOp);
	noOp.dispatcher = dispatcher;
	noOp.Initialize();
	return noOp;
}

func (noOp *NoOp) Initialize() {
	return;
}

func (noOp *NoOp) Execute() {
	return;
}

func (noOp *NoOp) DoneCallback() {
	return;
}

func (noOp *NoOp) IsDone() bool {
	return false;
}

func (noOp *NoOp) WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool {
	return false;
}


/**********************************************************************************************************************/
// This struct defines the PopulateDecidedValuesInOplogOp. This op queries Paxos and finds all the recent values that
// have been decided but are currently not present in operations log.
/**********************************************************************************************************************/
type PopulateDecidedValuesInOplogOp struct {
	dispatcher *util.Dispatcher;
	opLog *OpLog;
	pxManager *paxos.Paxos;
}

func NewPopulateDecidedValuesInOplogOp(dispatcher *util.Dispatcher, // The dispatcher used by KV Store.
         															 opLog *OpLog, // The OpLog object used by the KV Store.
																			 pxManager *paxos.Paxos, // The Paxos manager used by the KV Store.
                                      ) *PopulateDecidedValuesInOplogOp {
	pdvOp := new(PopulateDecidedValuesInOplogOp);
	pdvOp.dispatcher = dispatcher;
	pdvOp.pxManager = pxManager;
	pdvOp.opLog = opLog;
	pdvOp.Initialize();
	return pdvOp;
}

func (pdvOp *PopulateDecidedValuesInOplogOp) Initialize() {
	return;
}

func (pdvOp *PopulateDecidedValuesInOplogOp) Execute() {
	return;
}

func (pdvOp *PopulateDecidedValuesInOplogOp) DoneCallback() {
	return;
}

func (pdvOp *PopulateDecidedValuesInOplogOp) IsDone() bool {
	return false;
}

func (pdvOp *PopulateDecidedValuesInOplogOp) WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool {
	return false;
}


/**********************************************************************************************************************/
// This struct defines the FillHolesInOpLogOp. This op queries the Paxos manager associated with the key value store and
// fills up all holes in the operations log. If instances are still undecided, this op starts no ops for those ops to
// bring them to completion as soon as possible or to find out any missed values.
/**********************************************************************************************************************/
type FillHolesInOpLogOp struct {
	dispatcher *util.Dispatcher;
	opLog *OpLog;
	pxManager *paxos.Paxos;
	fillUptoInstance int64;
}

func NewFillHolesInOpLogOp(dispatcher *util.Dispatcher, // The dispatcher used by KV Store.
													 opLog *OpLog, // The OpLog object used by the KV Store.
													 pxManager *paxos.Paxos, // The Paxos manager used by the KV Store.
													 fillUptoInstance int64, // The instance up to which the operations log needs to be filled.
									 			  ) *FillHolesInOpLogOp {
	fhOp := new(FillHolesInOpLogOp);
	fhOp.dispatcher = dispatcher;
	fhOp.pxManager = pxManager;
	fhOp.opLog = opLog;
	fhOp.fillUptoInstance = fillUptoInstance;
	fhOp.Initialize();
	return fhOp;
}

func (fhOp *FillHolesInOpLogOp) Initialize() {
	return;
}

func (fhOp *FillHolesInOpLogOp) Execute() {
	return;
}

func (fhOp *FillHolesInOpLogOp) DoneCallback() {
	return;
}

func (fhOp *FillHolesInOpLogOp) IsDone() bool {
	return false;
}

func (fhOp *FillHolesInOpLogOp) WaitOrTimeout(timeout *time.Time, shouldTimeout bool) bool {
	return false;
}
