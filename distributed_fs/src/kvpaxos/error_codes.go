package kvpaxos

const (
	KSuccess = "Success";
	KFailure = "Failure"
	KErrNoKey = "ErrNoKey";
	KCASError = "CASError";
	KRetry = "Retry";
	KTimeout = "Timeout";
)
type Err string
