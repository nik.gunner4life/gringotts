package kvpaxos


/*
The args that are supplied by the client to the KV server to perform a Put/Write operation.
*/
type PutArgs struct {
	// The key that needs to be updated.
  Key string;
	// The value associated with the key.
  Value string;
	// Flag that is set if the client is retrying the same request. The client may retry the same request if the
	// previous attempt failed or timed out.
	Retry bool;
	// Client ID.
	ClientID uint64;
	// Client's request ID.
	ClientRequestID uint64;
	// Previous logical timestamp for the given key. All updates are CAS updates unless the Blind flag is set.
	PreviousLogicalTimestamp uint64;
	// If true, the KV server does not consult the logical timestamp but blindly updates the value.
	Blind bool;
}


/*
The value returned by the KV Server to the client after performing a Put/Write operation.
*/
type PutReply struct {
	// Error code.
  Err Err;
	// Client ID. This is not required. Just for testing purposes.
	ClientID uint64;
	// Client Request ID. This is not required. Just for testing purposes.
	ClientRequestID uint64;
	// The logical timestamp after updating the key.
	NewLogicalTimestamp uint64;
}


/*
The args that are supplied by the client to the KV server to perform a delete operation.
*/
type DeleteArgs struct {
	// The key that needs to be deleted.
	Key string;
	// Flag that is set if the client is retrying the same request. The client may retry the same request if the
	// previous attempt failed or timed out.
	Retry bool;
	// Client ID.
	ClientID uint64;
	// Client's request ID.
	ClientRequestID uint64;
	// Previous logical timestamp for the given key. All updates are CAS updates unless the Blind flag is set.
	PreviousLogicalTimestamp uint64;
	// If true, the KV server does not consult the logical timestamp but blindly updates the value.
	Blind bool;
}


/*
The value returned by the KV Server to the client after performing a delete operation.
*/
type DeleteReply struct {
	// Error code.
	Err Err;
	// Client ID. This is not required. Just for testing purposes.
	ClientID uint64;
	// Client Request ID. This is not required. Just for testing purposes.
	ClientRequestID uint64;
}


/*
The args that are supplied by the client to the KV server to perform a Get/Read operation.
*/
type GetArgs struct {
	// The key to lookup.
  Key string;
	// Flag that is set if the client is retrying the same request. The client may retry the same request if the
	// previous attempt failed or timed out.
	Retry bool;
	// Client ID.
	ClientID uint64;
	// Client's request ID.
	ClientRequestID uint64;
}


/*
The value returned by the KV Server to the client after performing a Get/Read operation.
*/
type GetReply struct {
	// Error code.
  Err Err;
	// Value of the requested key.
  Value string;
	// Client ID. This is not required. Just for testing purposes.
	ClientID uint64;
	// Client's request ID. This is not required. Just for testing purposes.
	ClientRequestID uint64;
	// Current logical timestamp associated with the key, value pair.
	CurrentLogicalTimestamp uint64;
}

