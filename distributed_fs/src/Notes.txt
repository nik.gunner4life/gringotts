Map Reduce Notes
================

At first we will fire the master with the command:
go run wc.go master x.txt localhost:7777

Initial Steps of the master
----------------------------
    1) We will call MakeMapReduce function that will first create a map reduce
    object. We initialize the state of the map reduce object
    func InitMapReduce(nmap int, nreduce int,
                       file string, master string) *MapReduce {
      mr := new(MapReduce)
      mr.nMap = nmap
      mr.nReduce = nreduce
      mr.file = file
      mr.MasterAddress = master
      mr.alive = true
      mr.registerChannel = make(chan string)
      mr.DoneChannel = make(chan bool)

      // initialize any additional state here
      return mr
    }

    2) We will then start a registration server. The registration server is one
    where we start listening in on a port and host. In this case localhost 7777
    At the same time we fork off other threads that will manage other connections
    and we can simply focus on the current one.

    3) We then fire off concurrently mr.RunMaster() which is what I HAVE TO CODE.

Initial Steps of the worker
----------------------------
    1) We call RunWorker() separately. The Map function and reduce function is
    passed to the worker. It then starts a new server and registers the worker
    object with it and hence the master has access to all the worker functions
    via RPC.

    2) The worker then registers with the master. It registers with the master
    by making an RPC to the master. The function called is MapReduce.Register
    and passes the arguments(it's address) and a reply address.

    3) The MapReduce.Register function takes the arguments from the previous
    step and passes it to the register channel and returns the reply.


Initial Thoughts
-----------------

a) I first need to read the worker's credentials from the register channel. This
will have the worker name and port on which I can speak to it.

b) Once I get the worker credentials, I should store them.
AI 1: Create a new variable in the map reduce struct that has the worker credentials.
After those values are stored, we can close the register channel as we will no
longer need it.

c) Once I have the worker credentials, I need to call the worker with the task
that it has to perform. This will involve calling the Worker.DoJob with
the job number, filename and number of tasks in the OTHER phase(nMap, nReduce)
Based on the filename and job number, DoMap/DoReduce are smart enough to perform
the correct task. We do not need to worry about this.

