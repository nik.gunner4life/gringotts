package mapreduce

import "fmt"
import "os"
import "log"
import "strconv"
import "encoding/json"
import "sort"
import "container/list"
import "net/rpc"
import "net"
import "bufio"
import "hash/fnv"
import "os/exec"

// A simple mapreduce library with a sequential implementation.
//
// The application provides an input file f, a Map and Reduce function,
// and the number of nMap and nReduce tasks.
//
// Split() splits the file f in nMap input files:
//    f-0, f-1, ..., f-<nMap-1>
// one for each Map job.
//
// DoMap() runs Map on each map file and produces nReduce files for a
// map file.  Thus, there will be nMap x nReduce files after all map
// jobs are done:
//    f-0-0, ..., f-0-0, f-0-<nReduce-1>, ...,
//    f-<nMap-1>-0, ... f-<nMap-1>-<nReduce-1>.
//
// DoReduce() collects <nReduce> reduce files from each map (f-*-<reduce>),
// and runs Reduce on those files.  This produces <nReduce> result files,
// which Merge() merges into a single output.

// Debugging
const Debug = 0

func DPrintf(format string, a ...interface{}) (n int, err error) {
  	if Debug > 0 {
	    n, err = fmt.Printf(format, a...)
  	}
  	return
}


type WorkerInfo struct {
	address string;
}


// Map and Reduce deal with <key, value> pairs:
type KeyValue struct {
  	Key string;
  	Value string;
}


type MapReduce struct {
  	nMap int; // Number of Map jobs
  	nReduce int;  // Number of Reduce jobs
  	file string; // Name of input file
  	MasterAddress string;
  	registerChannel chan string;
  	DoneChannel chan bool;
  	alive bool;
  	l net.Listener;
  	stats *list.List;

  	// Map of registered workers that you need to keep up to date.
  	Workers map[string]*WorkerInfo;
  	// List of workers that are available to do work.
  	AvailableWorkers chan *WorkerInfo;

	mapJobs *Jobs;
	reduceJobs *Jobs;
  	jobsChannel chan *JobInfo;
	jobsStatusChannel chan *JobStatus;
}

func CreateMapJobs(file string, nMap int, nReduce int) *Jobs {
	jobs := NewJobs()
	for i := 0; i < nMap; i++ {
		job := new(JobInfo);
		job.id  = i;
		job.file = file;
		job.operation = "Map";
		job.otherPhase = nReduce;
		job.completionStatus = kIncomplete;
		jobs.AddJob(job)
	}
	return jobs;
}

func CreateReduceJobs(file string, nMap int, nReduce int) *Jobs {
	jobs := NewJobs();
	for i := 0; i < nReduce; i++ {
		job := new(JobInfo);
		job.id  = i;
		job.file = file;
		job.operation = "Reduce";
		job.otherPhase = nMap;
		job.completionStatus = kIncomplete;
		jobs.AddJob(job);
	}
	return jobs;
}

func InitMapReduce(nmap int, nreduce int,
                   file string, master string) *MapReduce {
	mr := new(MapReduce);
  	mr.nMap = nmap;
  	mr.nReduce = nreduce;
  	mr.file = file;
  	mr.MasterAddress = master;
  	mr.alive = true;
  	mr.registerChannel = make(chan string, 1);
  	mr.DoneChannel = make(chan bool);
	mr.Workers = make(map[string]*WorkerInfo);
	mr.AvailableWorkers = make(chan *WorkerInfo, 100);

	// Initialize the map and reduce jobs that need to be performed.
	mr.mapJobs = CreateMapJobs(mr.file, mr.nMap, mr.nReduce);
	mr.reduceJobs = CreateReduceJobs(mr.file, mr.nMap, mr.nReduce);
  	return mr;
}

func MakeMapReduce(nmap int, nreduce int,
                   file string, master string) *MapReduce {
	log.Printf("Making mapreduce job")
  	mr := InitMapReduce(nmap, nreduce, file, master)
  	mr.StartRegistrationServer()
  	go mr.Run()
  	return mr
}

func (mr *MapReduce) Register(args *RegisterArgs, res *RegisterReply) error {
  	log.Printf("Registering worker %s", args.Worker)
  	mr.registerChannel <- args.Worker

  	// Update the list of workers and add the worker to the pool
  	// of available workers.
  	newWorker := new(WorkerInfo)
  	newWorker.address = <- mr.registerChannel
	mr.Workers[newWorker.address] = newWorker

	// Add worker to the list of available workers.
	mr.AvailableWorkers <- newWorker
	log.Printf("Completed registering worker %s", newWorker.address)

	// Let the worker know that he is registered.
	res.OK = true
  	return nil
}

func (mr *MapReduce) Shutdown(args *ShutdownArgs, res *ShutdownReply) error {
	log.Printf("Shutdown: registration server\n")
  	mr.alive = false
  	mr.l.Close()    // causes the Accept to fail
  	return nil
}

func (mr *MapReduce) StartRegistrationServer() {
  	rpcs := rpc.NewServer()
  	rpcs.Register(mr)
  	os.Remove(mr.MasterAddress)   // only needed for "unix"
  	l, e := net.Listen("unix", mr.MasterAddress)
  	if e != nil {
	    log.Fatal("RegstrationServer", mr.MasterAddress, " error: ", e)
  	}
  	mr.l = l

  	// now that we are listening on the master address, can fork off
  	// accepting connections to another thread.
	  go func() {
    	for mr.alive {
      	conn, err := mr.l.Accept()
      	if err == nil {
	        go func() {
	          rpcs.ServeConn(conn)
	          conn.Close()
	        }()
	      } else {
	        DPrintf("RegistrationServer: accept error", err)
	        break
	      }
	    }
	    DPrintf("RegistrationServer: done\n")
  	}()
}


// Name of the file that is the input for map job <MapJob>
func MapName(fileName string, MapJob int) string {
  return "mrtmp." +  fileName + "-" + strconv.Itoa(MapJob)
}

// Split bytes of input file into nMap splits, but split only on white space
func (mr *MapReduce) Split(fileName string) {
	fmt.Printf("Split %s\n", fileName)
  	infile, err := os.Open(fileName);
  	if err != nil {
	    log.Fatal("Split: ", err);
  	}
  	defer infile.Close()
  	fi, err := infile.Stat();
  	if err != nil {
	    log.Fatal("Split: ", err);
  	}
  	size := fi.Size()
  	nchunk := size / int64(mr.nMap);
  	nchunk += 1

  	outfile, err := os.Create(MapName(fileName, 0))
  	if err != nil {
	    log.Fatal("Split: ", err);
  	}
  	writer := bufio.NewWriter(outfile)
  	m := 1
  	i := 0

  	scanner := bufio.NewScanner(infile)
  	for scanner.Scan() {
	    if (int64(i) > nchunk * int64(m)) {
	      writer.Flush()
	      outfile.Close()
	      outfile, err = os.Create(MapName(fileName, m))
	      writer = bufio.NewWriter(outfile)
	      m += 1
	    }
	    line := scanner.Text() + "\n"
	    writer.WriteString(line)
	    i += len(line)
  	}
  	writer.Flush()
  	outfile.Close()
}

func ReduceName(fileName string, MapJob int, ReduceJob int) string {
  	return MapName(fileName, MapJob) + "-" + strconv.Itoa(ReduceJob)
}

func hash(s string) uint32 {
  	h := fnv.New32a()
  	h.Write([]byte(s))
  	return h.Sum32()
}

// Read split for job, call Map for that split, and create nreduce
// partitions.
func DoMap(JobNumber int, fileName string,
           nreduce int, Map func(string) *list.List) {
	name := MapName(fileName, JobNumber)
  	file, err := os.Open(name)
  	if err != nil {
	    log.Fatal("DoMap: ", err);
  	}
  	fi, err := file.Stat();
  	if err != nil {
	    log.Fatal("DoMap: ", err);
  	}
  	size := fi.Size()
  	fmt.Printf("DoMap: read split %s %d\n", name, size)
  	b := make([]byte, size);
  	_, err = file.Read(b);
  	if err != nil {
	    log.Fatal("DoMap: ", err);
  	}
  	file.Close()
  	res := Map(string(b))
  	// XXX a bit inefficient. could open r files and run over list once
  	for r := 0; r < nreduce; r++ {
	    file, err = os.Create(ReduceName(fileName, JobNumber, r))
	    if err != nil {
	      log.Fatal("DoMap: create ", err);
	    }
	    enc := json.NewEncoder(file)
	    for e := res.Front(); e != nil; e = e.Next() {
	      kv := e.Value.(KeyValue)
	      if hash(kv.Key) % uint32(nreduce) == uint32(r) {
	        err := enc.Encode(&kv);
	        if err != nil {
	          log.Fatal("DoMap: marshall ", err);
	        }
	      }
	    }
	    file.Close()
  	}
}

func MergeName(fileName string,  ReduceJob int) string {
	return "mrtmp." +  fileName + "-res-" + strconv.Itoa(ReduceJob)
}

// Read map outputs for partition job, sort them by key, call reduce for each
// key
func DoReduce(job int, fileName string, nmap int,
              Reduce func(string,*list.List) string) {
	kvs := make(map[string]*list.List)
  	for i := 0; i < nmap; i++ {
	    name := ReduceName(fileName, i, job)
	    fmt.Printf("DoReduce: read %s\n", name)
	    file, err := os.Open(name)
	    if err != nil {
	      log.Fatal("DoReduce: ", err);
	    }
	    dec := json.NewDecoder(file)
	    for {
	      var kv KeyValue
	      err = dec.Decode(&kv);
	      if err != nil {
	        break;
	      }
	      _, ok := kvs[kv.Key]
	      if !ok {
	        kvs[kv.Key] = list.New()
	      }
	      kvs[kv.Key].PushBack(kv.Value)
	    }
	    file.Close()
  	}
  	var keys []string
  	for k := range kvs {
	    keys = append(keys, k)
  	}
  	sort.Strings(keys)
  	p := MergeName(fileName, job)
  	file, err := os.Create(p)
  	if err != nil {
	    log.Fatal("DoReduce: create ", err);
  	}
  	enc := json.NewEncoder(file)
  	for _, k := range keys {
	    res := Reduce(k, kvs[k])
	    enc.Encode(KeyValue{k, res})
  	}
  	file.Close()
}

// Merge the results of the reduce jobs
// XXX use merge sort
func (mr *MapReduce) Merge() {
	DPrintf("Merge phase")
  	kvs := make(map[string]string)
  	for i := 0; i < mr.nReduce; i++ {
	    p := MergeName(mr.file, i)
	    fmt.Printf("Merge: read %s\n", p)
	    file, err := os.Open(p)
	    if err != nil {
	      log.Fatal("Merge: ", err);
	    }
	    dec := json.NewDecoder(file)
	    for {
	      var kv KeyValue
	      err = dec.Decode(&kv);
	      if err != nil {
	        break;
	      }
	      kvs[kv.Key] = kv.Value
	    }
	    file.Close()
  	}
  	var keys []string
  	for k := range kvs {
	    keys = append(keys, k)
  	}
  	sort.Strings(keys)

  	file, err := os.Create("mrtmp." + mr.file)
  	if err != nil {
	    log.Fatal("Merge: create ", err);
  	}
  	w := bufio.NewWriter(file)
  	for _, k := range keys {
	    fmt.Fprintf(w, "%s: %s\n", k, kvs[k])
  	}
  	w.Flush()
  	file.Close()
}

func RemoveFile(n string) {
	err := os.Remove(n)
  	if err != nil {
	    log.Fatal("CleanupFiles ", err);
  	}
}

func (mr *MapReduce) CleanupFiles() {
	for i := 0; i < mr.nMap; i++ {
	    RemoveFile(MapName(mr.file, i))
	    for j := 0; j < mr.nReduce; j++ {
	      RemoveFile(ReduceName(mr.file, i, j))
	    }
  	}
  	for i := 0; i < mr.nReduce; i++ {
	    RemoveFile(MergeName(mr.file, i))
  	}
  	RemoveFile("mrtmp." + mr.file)
}

// Run jobs sequentially.
func RunSingle(nMap int, nReduce int, file string,
               Map func(string) *list.List,
               Reduce func(string,*list.List) string) {
	mr := InitMapReduce(nMap, nReduce, file, "")
  	mr.Split(mr.file)
  	for i := 0; i < nMap; i++ {
	    DoMap(i, mr.file, mr.nReduce, Map)
  	}
  	for i := 0; i < mr.nReduce; i++ {
	    DoReduce(i, mr.file, mr.nMap, Reduce)
  	}
  	mr.Merge()
}

func (mr *MapReduce) CleanupRegistration() {
	args := &ShutdownArgs{}
  	var reply ShutdownReply
  	ok := call(mr.MasterAddress, "MapReduce.Shutdown", args, &reply)
  	if ok == false {
	    log.Printf("Cleanup: RPC %s error", mr.MasterAddress)
  	}
  	log.Printf("CleanupRegistration: done");
}

func (mr *MapReduce) GetAnAvailableWorker() *WorkerInfo {
	for worker := range mr.AvailableWorkers {
		return worker
	}
	return nil
}

// The master will do the following:
// 	a) Now make an RPC to the worker by calling Worker.DoJob
// 	b) Pass the following arguments: job number, filename and number of tasks
// 	   in the OTHER phase(nMap, nReduce)
//	c) Once all the tasks have been completed, ask the workers to die.
func (mr *MapReduce) RunMaster() {
	log.Printf("Starting Map jobs.")
	mr.ProcessJobs(mr.mapJobs)
	log.Printf("Starting Reduce jobs.")
	mr.ProcessJobs(mr.reduceJobs)
}

func (mr *MapReduce) TestWorkerFailures() {
	for i := 0; i < 50; i++ {
		mr.Run();
		mr.mapJobs = CreateMapJobs(mr.file, mr.nMap, mr.nReduce);
		mr.reduceJobs = CreateReduceJobs(mr.file, mr.nMap, mr.nReduce);
		var cmd string;
		cmd = "./diff.sh"
		out, err := exec.Command(cmd).Output();
		if err != nil {
			log.Fatal("Could not diff outputs. Fataling")
		}
		output := string(out[:len(out)])
		if output != "" {
			log.Fatal(output)
		}
	}
	mr.DoneChannel <- true
}

// Run jobs in parallel, assuming a shared file system
func (mr *MapReduce) Run() {
	fmt.Printf("Run mapreduce job %s %s\n", mr.MasterAddress, mr.file)
  	mr.Split(mr.file)
  	mr.RunMaster()
  	mr.Merge()
  	// mr.CleanupRegistration()
  	log.Printf("%s: MapReduce done", mr.MasterAddress)
	mr.DoneChannel <- true
}

// Clean up all workers by sending a Shutdown RPC to each one of them Collect
// the number of jobs each work has performed.
func (mr *MapReduce) KillWorkers() *list.List {
	l := list.New()
	for _, w := range mr.Workers {
		log.Printf("DoWork: shutdown %s", w.address)
		args := &ShutdownArgs{}
		var reply ShutdownReply;
		ok := call(w.address, "Worker.Shutdown", args, &reply)
		if ok == false {
			log.Printf("DoWork: RPC %s shutdown error", w.address)
		} else {
			l.PushBack(reply.Njobs)
		}
	}
	return l
}

func (mr *MapReduce) FarmJobs(jobsChannel chan *JobInfo,
	jobsStatusChannel chan *JobStatus) {
	for {
		job, isChannelOpen := <- jobsChannel;
		if isChannelOpen {
			worker := mr.GetAnAvailableWorker();
			if worker == nil {
				log.Fatal("There are no workers available")
			}
			args := &DoJobArgs{job.file, job.operation, job.id, job.otherPhase};
			var reply DoJobReply;
			go func() {
				ok := call(worker.address, "Worker.DoJob", args, &reply)
				jobStatus := new(JobStatus);
				jobStatus.id = job.id;
				if !ok {
					// There was a failure. Update the status accordingly
					jobStatus.status = kFail;
				} else {
					jobStatus.status = kSuccess;
				}
				jobsStatusChannel <- jobStatus;
				// Add worker to pool of workers.
				mr.AvailableWorkers <- worker;
			} ()
		} else {
			log.Printf("All jobs have been completed.")
			break
		}
	}
}

func (mr *MapReduce) AreJobsDone(jobs *Jobs) bool {
	jobsMap := jobs.jobs;
	for _, job := range jobsMap {
		if job.completionStatus != kSuccess {
			return false;
		}
	}
	return true;
}

func (mr *MapReduce) QueueFailedJobs(jobs *Jobs, jobsChannel chan *JobInfo) {
	jobsMap := jobs.jobs;
	for _, job := range jobsMap {
		if job.completionStatus == kFail {
			// Add failed jobs to the queue again.
			log.Printf("Queueing job %d that had previously failed", job.id)
			jobsChannel <- job;
			job.completionStatus = kWaiting;
		}
	}
}

// This function will process jobs that have been completed and based on
// the status, it will act accordingly
func (mr *MapReduce) ProcessJobs(jobs *Jobs) {
	jobsChannel := make(chan *JobInfo, 100)
	jobsStatusChannel := make(chan *JobStatus, 100)
	// Farm out the jobs on a separate thread.
	go mr.FarmJobs(jobsChannel, jobsStatusChannel);

	// Add all the jobs for the first time.
	jobsMap := jobs.jobs;
	for _, job := range jobsMap {
		if job.completionStatus == kIncomplete {
			jobsChannel <- job;
			job.completionStatus = kWaiting;
		}
	}

	for {
		if !mr.AreJobsDone(jobs) {
			// Some jobs are either in waiting state or have failed.
			// Add all jobs that had failed previously.
			mr.QueueFailedJobs(jobs, jobsChannel);

			// Wait on the status channel for a job to complete.
			jobStatus := <- jobsStatusChannel;
			jobs.UpdateJobStatus(jobStatus.id, jobStatus.status);
			// Update the jobs map with the status.
		} else {
			// All jobs are done. Close the jobsChannel and jobsStatusChannel
			close(jobsChannel);
			close(jobsStatusChannel);
			break;
		}
	}
}

