package mapreduce

type JobInfo struct {
	id int;
	operation string;
	otherPhase int;
	file string;
	completionStatus int;
}

type JobStatus struct {
	id int;
	status int;
}

func NewJobInfo(jobID int, jobOperation string, jobNumOtherPhase int,
	jobFile string) *JobInfo {
	job := new(JobInfo);
	job.completionStatus = kIncomplete;
	job.file = jobFile;
	job.id = jobID;
	job.operation = jobOperation;
	job.otherPhase = jobNumOtherPhase;
	return job;
}

type Jobs struct {
	jobs map[int]*JobInfo;
}

func NewJobs() *Jobs {
	jobs := new(Jobs);
	jobs.jobs = make(map[int]*JobInfo);
	return jobs;
}

func (jobs *Jobs) AddJob(job *JobInfo) {
	_, ok := jobs.jobs[job.id];
	if !ok {
		// Job does not exist. Add the job.
		jobs.jobs[job.id] = job;
	}
}

func (jobs *Jobs) GetJob(jobID int) *JobInfo {
	_, ok := jobs.jobs[jobID];
	if !ok {
		return nil;
	}
	return jobs.jobs[jobID];
}

func (jobs *Jobs) UpdateJobStatus(jobID int, status int) {
	_, ok := jobs.jobs[jobID];
	if !ok {
		return
	}
	jobs.jobs[jobID].completionStatus = status;
}


