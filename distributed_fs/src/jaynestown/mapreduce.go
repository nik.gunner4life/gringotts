package jaynestown

/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/22/2015
Description : This file contains details of the map reduce job. This is
			  essentially a file containing the umbrella of ops that will
			  be performed. It should expose exactly 4 functions i.e.
			  	a) Split
			  	b) GetMapJobs
			  	c) GetReduceJobs
			  	d) Merge
			  The magistrate will call each and every one of these functions
			  serially.
*******************************************************************************/

import "fmt"
import "os"
import "log"
import "strconv"
import "encoding/json"
import "sort"
import "container/list"
import "bufio"
import "hash/fnv"


/*
Added purely to test the map reduce framework.
TODO: Will be removed in the future.
*/
type KeyValue struct {
  	Key string;
  	Value string;
}


/*
Defines the jobs that need to be performed.
TODO: Will be modified in the future. This is just a placeholder.
*/
type MapReduce struct {
  	nMap int; // Number of Map jobs
  	nReduce int;  // Number of Reduce jobs
  	file string; // Name of input file
}


/*
Initialize a map reduce object.
*/
func InitMapReduce(nmap int, nreduce int, file string) *MapReduce {
	mr := new(MapReduce);
	mr.nMap = nmap;
	mr.nReduce = nreduce;
	mr.file = file;
	return mr;
}

/*
Generate a list of MapJobs that need to be performed and return this list.
*/
func (mr *MapReduce) GetMapJobs() *Jobs {
	jobs := NewJobs()
	for i := 0; i < mr.nMap; i++ {
		job := new(JobInfo);
		job.id  = i;
		job.file = mr.file;
		job.operation = "Map";
		job.otherPhase = mr.nReduce;
		job.completionStatus = kIncomplete;
		jobs.AddJob(job)
	}
	return jobs;
}

/*
Generate a list of ReduceJobs that need to be performed.
*/
func (mr *MapReduce) GetReduceJobs() *Jobs {
	jobs := NewJobs();
	for i := 0; i < mr.nReduce; i++ {
		job := new(JobInfo);
		job.id  = i;
		job.file = mr.file;
		job.operation = "Reduce";
		job.otherPhase = mr.nMap;
		job.completionStatus = kIncomplete;
		jobs.AddJob(job);
	}
	return jobs;
}

/*
Divide the given input into multiple smaller inputs.
*/
func (mr *MapReduce) Split(fileName string) {
	log.Printf("Split %s", fileName)
	infile, err := os.Open(fileName);
	if err != nil {
		log.Fatal("Split: ", err);
	}
	defer infile.Close()
	fi, err := infile.Stat();
	if err != nil {
		log.Fatal("Split: ", err);
	}
	size := fi.Size()
	nchunk := size / int64(mr.nMap);
	nchunk += 1

	outfile, err := os.Create(MapName(fileName, 0))
	if err != nil {
		log.Fatal("Split: ", err);
	}
	writer := bufio.NewWriter(outfile)
	m := 1
	i := 0

	scanner := bufio.NewScanner(infile)
	for scanner.Scan() {
		if (int64(i) > nchunk * int64(m)) {
			writer.Flush()
			outfile.Close()
			outfile, err = os.Create(MapName(fileName, m))
			writer = bufio.NewWriter(outfile)
			m += 1
		}
		line := scanner.Text() + "\n"
		writer.WriteString(line)
		i += len(line)
	}
	writer.Flush()
	outfile.Close()
}

/*
Merge the outputs of map reduce jobs into a single output.
*/
func (mr *MapReduce) Merge() {
	DPrintf("Merge phase")
	kvs := make(map[string]string)
	for i := 0; i < mr.nReduce; i++ {
		p := MergeName(mr.file, i)
		log.Printf("Merge: read %s", p)
		file, err := os.Open(p)
		if err != nil {
			log.Fatal("Merge: ", err);
		}
		dec := json.NewDecoder(file)
		for {
			var kv KeyValue
			err = dec.Decode(&kv);
			if err != nil {
				break;
			}
			kvs[kv.Key] = kv.Value
		}
		file.Close()
	}
	var keys []string
	for k := range kvs {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	file, err := os.Create("mrtmp." + mr.file)
	if err != nil {
		log.Fatal("Merge: create ", err);
	}
	w := bufio.NewWriter(file)
	for _, k := range keys {
		fmt.Fprintf(w, "%s: %s\n", k, kvs[k])
	}
	w.Flush()
	file.Close()
}


/******************************************************************************
FUNCTIONS EXPOSED TO THE WORKER.
TODO: WILL BE MOVED ELSEWHERE IN FUTURE
*******************************************************************************/

/*
Return filename for map jobs based on overall input and job id.
TODO: Will be modified in the future
*/
func MapName(fileName string, MapJob int) string {
  return "mrtmp." +  fileName + "-" + strconv.Itoa(MapJob)
}

/*
Return filename for reduce jobs based on overall input and job id.
TODO: Will be modified in the future
*/
func ReduceName(fileName string, MapJob int, ReduceJob int) string {
  	return MapName(fileName, MapJob) + "-" + strconv.Itoa(ReduceJob)
}


/*
Generate a hash.
*/
func hash(s string) uint32 {
  	h := fnv.New32a()
  	h.Write([]byte(s))
  	return h.Sum32()
}


/*
Do the MapJob.
TODO: Will be modified in the future
*/
func DoMap(JobNumber int, fileName string,
           nreduce int, Map func(string) *list.List) {
	name := MapName(fileName, JobNumber)
  	file, err := os.Open(name)
  	if err != nil {
	    log.Fatal("DoMap: ", err);
  	}
  	fi, err := file.Stat();
  	if err != nil {
	    log.Fatal("DoMap: ", err);
  	}
  	size := fi.Size()
  	log.Printf("DoMap: read split %s %d", name, size)
  	b := make([]byte, size);
  	_, err = file.Read(b);
  	if err != nil {
	    log.Fatal("DoMap: ", err);
  	}
  	file.Close()
  	res := Map(string(b))
  	// XXX a bit inefficient. could open r files and run over list once
  	for r := 0; r < nreduce; r++ {
	    file, err = os.Create(ReduceName(fileName, JobNumber, r))
	    if err != nil {
	      log.Fatal("DoMap: create ", err);
	    }
	    enc := json.NewEncoder(file)
	    for e := res.Front(); e != nil; e = e.Next() {
	      kv := e.Value.(KeyValue)
	      if hash(kv.Key) % uint32(nreduce) == uint32(r) {
	        err := enc.Encode(&kv);
	        if err != nil {
	          log.Fatal("DoMap: marshall ", err);
	        }
	      }
	    }
	    file.Close()
  	}
}


/*
Generate file name for merge job based on overall input and job id.
TODO: Will be modified in the future
*/
func MergeName(fileName string,  ReduceJob int) string {
	return "mrtmp." +  fileName + "-res-" + strconv.Itoa(ReduceJob)
}


/*
Do the Reduce Job.
TODO: Will be modified in the future
*/
func DoReduce(job int, fileName string, nmap int,
              Reduce func(string,*list.List) string) {
	kvs := make(map[string]*list.List)
  	for i := 0; i < nmap; i++ {
	    name := ReduceName(fileName, i, job)
	    log.Printf("DoReduce: read %s", name)
	    file, err := os.Open(name)
	    if err != nil {
	      log.Fatal("DoReduce: ", err);
	    }
	    dec := json.NewDecoder(file)
	    for {
	      var kv KeyValue
	      err = dec.Decode(&kv);
	      if err != nil {
	        break;
	      }
	      _, ok := kvs[kv.Key]
	      if !ok {
	        kvs[kv.Key] = list.New()
	      }
	      kvs[kv.Key].PushBack(kv.Value)
	    }
	    file.Close()
  	}
  	var keys []string
  	for k := range kvs {
	    keys = append(keys, k)
  	}
  	sort.Strings(keys)
  	p := MergeName(fileName, job)
  	file, err := os.Create(p)
  	if err != nil {
	    log.Fatal("DoReduce: create ", err);
  	}
  	enc := json.NewEncoder(file)
  	for _, k := range keys {
	    res := Reduce(k, kvs[k])
	    enc.Encode(KeyValue{k, res})
  	}
  	file.Close()
}
