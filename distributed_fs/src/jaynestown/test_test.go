package jaynestown

/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/22/2015
Description : Unit test cases to check functionality of jaynestown.
*******************************************************************************/

import "testing"
import "log"
import "time"

const (
	magistrateAddress = "localhost:7777"
	mudder1Address = "localhost:7778"
	mudder2Address = "localhost:7779"
	mudder3Address = "localhost:7780"
	mudder4Address = "localhost:7781"
	file = "kjv12.txt"
)


func setup() *Canton {
	canton := MakeCanton(magistrateAddress)
	log.Printf("Magistrate set up successfully.")
	return canton
}



/*
Test Basic mudder registering and completion of jobs assigned to it.
*/
func TestBasic(t *testing.T) {
	// Stupid, but I want to use it so that I can add time as and when I require
	// without having to specifically add and remove the package.
	time.Sleep(time.Millisecond);
	log.Printf("************************* TEST BASIC *************************")
	canton := setup();
	mr := InitMapReduce(5, 3, file);
	signal := make(chan bool, 1);
	go canton.RunMagistrate(mr, signal);
	log.Printf("Starting Mudder")
	go RunMudder(magistrateAddress, mudder1Address, MapFunc, ReduceFunc);
	<- signal;
	verify();
}


/*
Test two mudders registering with the master and doing the jobs assigned to it.
*/
func TestTwoMudders(t *testing.T) {
	log.Printf("********************** TEST TWO MUDDERS **********************")
	canton := setup();
	mr := InitMapReduce(5, 3, file);
	signal := make(chan bool, 1);
	go canton.RunMagistrate(mr, signal);
	log.Printf("Starting Mudder 1")
	go RunMudder(magistrateAddress, mudder1Address, MapFunc, ReduceFunc);
	log.Printf("Starting Mudder 2")
	go RunMudder(magistrateAddress, mudder2Address, MapFunc, ReduceFunc);
	<- signal;
	verify();
}


/*
Test using multiple mudders and jobs should be split as evenly as possible.
*/
func TestMultipleMudders(t *testing.T) {
	log.Printf("******************* TEST MULTIPLE MUDDERS ********************")
	canton := setup();
	mr := InitMapReduce(5, 3, file);
	signal := make(chan bool, 1);
	go canton.RunMagistrate(mr, signal);
	log.Printf("Starting 4 mudders")
	go RunMudder(magistrateAddress, mudder1Address, MapFunc, ReduceFunc);
	go RunMudder(magistrateAddress, mudder2Address, MapFunc, ReduceFunc);
	go RunMudder(magistrateAddress, mudder3Address, MapFunc, ReduceFunc);
	go RunMudder(magistrateAddress, mudder4Address, MapFunc, ReduceFunc);
	<- signal;
	verify();
}


/*
Test running the same process multiple times. The mudders should all remain
registered and accept incoming requests from the magistrate.
*/
func TestMultipleIterations(t *testing.T) {
	log.Printf("***************** TEST MULTIPLE ITERATIONS ******************");
	canton := setup();
	mr := InitMapReduce(5, 3, file);
	signal := make(chan bool, 1);
	go canton.RunMagistrate(mr, signal);
	log.Printf("Starting 4 mudders");
	go RunMudder(magistrateAddress, mudder1Address, MapFunc, ReduceFunc);
	go RunMudder(magistrateAddress, mudder2Address, MapFunc, ReduceFunc);
	go RunMudder(magistrateAddress, mudder3Address, MapFunc, ReduceFunc);
	go RunMudder(magistrateAddress, mudder4Address, MapFunc, ReduceFunc);
	<- signal;
	verify();
	// Now start multiple iterations.
	time.Sleep(time.Second);
	status := make(chan bool, 1);
	go KeepDoingJobs(canton, mr, 5, status);
	<- status;
}
