package jaynestown

/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/22/2015
Description : This file contains the all jobs that are performed by the mudder
			  The mudder will first attempt to register with the magistrate.
			  Once registered, the mudder will accept any number of jobs that
			  are supplied to it by the magistrate.
*******************************************************************************/

import "fmt"
import "os"
import "log"
import "net/rpc"
import "net"
import "container/list"

/*
Mudder is a server waiting for DoJob or Shutdown RPCs
 */
type Mudder struct {
	name string
	Reduce func(string, *list.List) string
	Map func(string) *list.List
	l net.Listener
}

/*
The magistrate sent us a job. Do as we are told.
 */
func (md *Mudder) DoJob(arg *DoJobArgs, res *DoJobReply) error {
	switch arg.Operation {
	case Map:
		DoMap(arg.JobNumber, arg.File, arg.NumOtherPhase, md.Map)
	case Reduce:
		DoReduce(arg.JobNumber, arg.File, arg.NumOtherPhase, md.Reduce)
	}
	res.OK = true
	return nil
}

/*
Reply to ping sent by magistrate with status true.
 */
func (mudder *Mudder) Ping(args *PingArgs, res *PingArgs) error {
	res.OK = true;
	return nil;
}

/*
The magistrate is telling us to shutdown. Report the number of Jobs we
have processed.
 */
func (md *Mudder) Shutdown(args *ShutdownArgs, res *ShutdownReply) error {
	log.Printf("Shutdown %s", md.name);
	res.Njobs = 0;
	res.OK = true;
	return nil;
}

/*
Tell the magistrate we exist and ready to work.
 */
func Register(magistrate string, me string) {
	args := &RegisterArgs{}
	args.Mudder = me
	var reply RegisterReply
	log.Printf("Registering with magistrate.")
	ok := call(magistrate, "Canton.Register", args, &reply)
	if ok == false {
		fmt.Printf("Register: RPC %s register error\n", magistrate)
	}
}

/*
Set up a connection with the magistrate, register with the magistrate,
and wait for jobs from the magistrate.
 */
func RunMudder(MagistrateAddress string, me string,
	MapFunc func(string) *list.List,
	ReduceFunc func(string,*list.List) string) {
	log.Printf("Mudder %s", me);
	md := new(Mudder);
	md.name = me;
	md.Map = MapFunc;
	md.Reduce = ReduceFunc;
	rpcs := rpc.NewServer();
	rpcs.Register(md);
	// only needed for "unix"
	os.Remove(me);
	l, e := net.Listen("unix", me);
	if e != nil {
		log.Fatal("Mudder: mudder ", me, " error: ", e);
	}
	md.l = l;
	Register(MagistrateAddress, me);

	// DON'T MODIFY CODE BELOW
	for {
		conn, err := md.l.Accept();
		if err == nil {
			go rpcs.ServeConn(conn);
		} else {
			break;
		}
	}
	md.l.Close();
	log.Printf("Mudder %s exit", me);
}

