package jaynestown

/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/22/2015
Description : This file contains all constants that will be used across the
			  package. It will also contain the routines that will be needed
			  throughout the package.
*******************************************************************************/


import "fmt"
import "net/rpc"
import "time"

// Debug print constants
const Debug = 0

// Job Type constants
const (
  Map = "Map"
  Reduce = "Reduce"
)
// Alias JobType to string.
type JobType string

// Job Status constants
const (
	kSuccess = iota;
	kIncomplete = iota;
	kWaiting = iota;
	kFail = iota;
)

// Mudder Health Constants
const (
	kMudderFree = iota;
	kMudderBusy = iota;
	kMudderDead = iota;
)

// Ping Interval and DeadPings consts
const PingInterval = time.Millisecond * 100;
const DeadPings = 5;

// Function for debug printf. Prints output to console.
func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		n, err = fmt.Printf(format, a...)
	}
	return
}


// RPC arguments and replies.  Field names must start with capital letters,
// otherwise RPC will break.
type DoJobArgs struct {
  File string
  Operation string
  JobNumber int       // this job's number
  NumOtherPhase int   // total number of jobs in other phase (map or reduce)
}

type DoJobReply struct {
  OK bool
}

type ShutdownArgs struct {
}

type ShutdownReply struct {
  Njobs int
  OK bool
}

type RegisterArgs struct {
  Mudder string
}

type RegisterReply struct {
  OK bool
}

type PingArgs struct {
	OK bool;
}


/*
call() sends an RPC to the rpcname handler on server srv
with arguments args, waits for the reply, and leaves the
reply in reply. the reply argument should be the address
of a reply structure.

call() returns true if the server responded, and false
if call() was not able to contact the server. in particular,
reply's contents are valid if and only if call() returned true.

You can assume that call() will time out and return an
error after a while if it doesn't get a reply from the server.

Please use call() to send all RPCs, in magistrate.go and mudder.go.
*/

func call(srv string, rpcname string,
          args interface{}, reply interface{}) bool {
  c, errx := rpc.Dial("unix", srv)
  if errx != nil {
    return false
  }
  defer c.Close()

  err := c.Call(rpcname, args, reply)
  if err == nil {
    return true
  }

  fmt.Println(err)
  return false
}
