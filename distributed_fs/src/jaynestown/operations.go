package jaynestown

/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/25/2015
Description : List of operations that will be performed by the mudder.
*******************************************************************************/


import "fmt"
import "container/list"
import "strconv"
import "strings"
import "regexp"
import "log"
import "os/exec"


/*
This function will split the entire string into a splice of words and using a
map, we need to get count of every word in string. And then return a list of
KeyValue pairs. Look at KeyValue pair in mapreduce.go.
*/
func MapFunc(value string) *list.List {
	// Generate three regexes to assist in splitting the words.
	newLineRegex, _ := regexp.Compile("\\n");
	wordCheckRegex, _ := regexp.Compile("[a-zA-Z]+");
	cleanWordRegex, _ := regexp.Compile("\\W*");

	// Convert all newline characters to spaces
	safeValue := newLineRegex.ReplaceAllString(value, " ");

	// Split the string by space
	words := strings.Split(safeValue, " ");
	// Generate a regular expression to remove all special characters
	// from the word. This will keep it clean.
	wordMap := make(map[string]int);
	// Get a count of every word in words
	for _, word := range words {
		// Remove all special characters from the word
		word = cleanWordRegex.ReplaceAllString(word, "");
		// If a word is just blanks, then ignore it.
		if !wordCheckRegex.MatchString(word) {
			continue;
		}

		// The word is valid. Update wordMap count appropriately.
		if _, rv := wordMap[word]; rv {
			wordMap[word] += 1;
		} else {
			wordMap[word] = 1;
		}
	}
	// Convert wordMap to a list of KeyValue Pairs and return it.
	wordCountList := list.New();
	for word, count := range wordMap {
		var pair KeyValue;
		pair.Key = word;
		pair.Value = strconv.Itoa(count);
		wordCountList.PushBack(pair);
	}
	return wordCountList;
}


/*
Calculate the sum of values.
 */
func ReduceFunc(key string, values *list.List) string {
	sum := 0;
	for element := values.Front(); element != nil; element = element.Next() {
		// The .(int) is type conversion. Because, for go, the value is an interface
		// As a result, operators will not be permitted. Hence to access the value
		// we need to assert the type so that go can understand it. Rather neat!
		value, err := strconv.Atoi(element.Value.(string));
		if err != nil {
			fmt.Println("Invalid string was passed.");
			return "-1";
		}
		sum = sum + value;
	}
	sumString := strconv.Itoa(sum);
	return sumString;
}


/*
Diff the generated output with the expected output. Useful when testing.
TODO: Will be removed in the future.
*/
func diff() bool {
	cmd := "./diff.sh"
	out, err := exec.Command(cmd).Output();
	if err != nil {
		log.Fatal("Could not diff outputs. Fataling")
	}
	output := string(out[:len(out)])
	if output != "" {
		return false
	}
	return true
}


/*
Verify if diff() was successful. Panic if diff exists.
*/
func verify() {
	if(!diff()) {
		log.Fatal("Test Failed")
	}
	log.Printf("Test passed");
}


/*
Perform the same job over and over again up to a specified number of times.
*/
func KeepDoingJobs(canton *Canton, mr *MapReduce, n int, status chan bool) {
	for i := 0; i < n; i++ {
		log.Printf("Doing iteration %d", i + 1);
		go canton.StartFactory(mr);
		<- canton.DoneChannel;
		verify();
	}
	status <- true;
}
