package jaynestown
/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/22/2015
Description : This file contains the all the tasks that the magistrate
			  i.e. the mapreduce master will perform. The magistrate will
			  generate a list of jobs that need to be performed and will
			  assign it to the mudders that are currently available i.e.
			  alive. The magistrate is multithreaded i.e. there will be a
			  separate thread for all jobs that are currently being performed
			  and two other threads(mudderManager, milkManager) that will
			  maintain state. The magistrate is responsible for the following:
			  	a) Create a list of jobs that need to be performed
			  	b) The magistrate will create a thread(milkManager) that will
			  	handle the health of the mudders(are they getting enough
			  	mudders's milk?).milkManager will frequently ping the mudders
			  	and update the state of the mudder as dead, if it does not
			  	respond for sometime.
			  	d) The mudderManager maintains the state of the mapreduce job
			  	that needs to be performed. It will maintain the list of
			  	jobs and as a mudder becomes available, it will assign the
			  	next job in its job queue(if any).
*******************************************************************************/

import "log"
import "os"
import "net/rpc"
import "net"
import "sync"
import "time"


/*
A struct to maintain the Mudder information.
 */
type MudderInfo struct {
	// Mudder's host and port addresses
	Address string;

	// A lock to update the mudder's information
	healthMutex sync.Mutex;
	// Mudder's health. Do not modify this directly.
	health int;

	// Count of the number of consecutive pings that the mudder
	// has not replied to.
	DeadPingCount int;
}

/*
Initialize and return a new MudderInfo Object
 */
func InitMudderInfo(address string) *MudderInfo {
	mudder := new(MudderInfo);
	mudder.Address = address
	mudder.health = kMudderFree
	mudder.DeadPingCount = 0;
	return mudder
}

/*
Acquire a lock on the mudder health and return the value.
 */
func (mudder *MudderInfo) PrintMudderConfig() {
	log.Printf("Mudder: %s", mudder.Address)
	log.Printf("Mudder Health: %d", mudder.GetMudderHealth())
	log.Printf("Mudder DeadPingCount: %d", mudder.DeadPingCount)
}

/*
Acquire a lock on the mudder health and return the value.
 */
func (mudder *MudderInfo) GetMudderHealth() int {
	mudder.healthMutex.Lock()
	defer mudder.healthMutex.Unlock()
	return mudder.health
}

/*
Acquire a lock on the mudder health and update it
 */
func (mudder *MudderInfo) SetMudderHealth(health int) {
	mudder.healthMutex.Lock()
	defer mudder.healthMutex.Unlock()
	mudder.health = health
}

/*
This struct will maintain a map of mudders that have registered to work at
Canton. This struct will essentially maintains the entire state machine for
Jaynestown.
 */
type Canton struct {
	// Map of registered mudders
	mudders map[string]*MudderInfo;
	// Mudders that are available to do work.
	availableMudders chan string;
	// The magistrate's host and port address
	magistrateAddress string;
	// The channel on which new mudders will register
	registerChannel chan string;
	// The channel on which the job thread can indicate to the milk manager
	// that they are done using the mudder
	doneWithMudder chan string;
	// This channel will be used to indicate when the mapreduce jobs are done.
	DoneChannel chan bool;
	// The listener object i.e. the one that manages communication between
	// mudders and the magistrate
	listener net.Listener;
	alive bool;
}

func MakeCanton(address string) *Canton {
	log.Printf("Creating Canton")
	canton := InitCanton(address);
	return canton
}

/*
This function will create Canton for the very first time.
 */
func InitCanton(magistrate string) *Canton {
	canton := new(Canton);
	canton.magistrateAddress = magistrate;
	canton.alive = true
	canton.registerChannel = make(chan string, 10);
	canton.doneWithMudder = make(chan string, 10);
	canton.DoneChannel = make(chan bool, 1);
	canton.mudders = make(map[string]*MudderInfo);
	// Not more than 100 mudders will be made available
	canton.availableMudders = make(chan string, 100);
	return canton;
}


/*
This function handles the registration server i.e. it creates a separate thread
that will accept new registrations from mudders.
 */
func (canton *Canton) startRegistrationServer() {
	rpcs := rpc.NewServer()
	rpcs.Register(canton)
	// only needed for "unix"
	os.Remove(canton.magistrateAddress)
	l, e := net.Listen("unix", canton.magistrateAddress)
	if e != nil {
		log.Fatal("RegstrationServer", canton.magistrateAddress, " error: ", e)
	}
	canton.listener = l

	// Now that we are listening on the magistrate address, we can fork off
	// accepting connections to another thread.
	go func() {
		for canton.alive {
			conn, err := canton.listener.Accept()
			if err == nil {
				go func() {
					rpcs.ServeConn(conn)
					conn.Close()
				}()
			} else {
				log.Printf("Error: RegistrationServer: accept error", err)
				break
			}
		}
		log.Printf("RegistrationServer: done")
	}()
}

/*
Keep pinging the mudders every PingInterval and if the mudder does not reply for
DeadPings Intervals, mark the mudder as kMudderDead. This is the function that
milkManager will be performing.
 */
func (canton *Canton) monitorMudders() {
	args := PingArgs{true}
	reply := PingArgs{}
	for {
		// Ping all the mudders and ensure that they are alive
		for mudderAddress, mudderInfo := range canton.mudders {
			ok := call(mudderAddress, "Mudder.Ping", args, &reply)
			if !ok {
				mudderInfo.DeadPingCount++;
				if mudderInfo.DeadPingCount == DeadPings {
					// Keep the DeadPingCount at DeadPings. We do not want
					// this to become a big number.
					mudderInfo.DeadPingCount = DeadPings
					// It has been DeadPings and we have not heard back from
					// the mudder. Marking it as dead.
					mudderInfo.SetMudderHealth(kMudderDead)
					log.Printf("Marked Mudder %s as kMudderDead", mudderAddress)
				}
			} else {
				// Reset the count
				mudderInfo.DeadPingCount = 0;
			}
		}

		// Monitor the register channel and see if any new mudders have
		// registered. Wait upto a millisecond. Otherwise move on to the
		// next thing.
		for {
			exitStatus := 0;
			select {
			case mudderAddress := <- canton.registerChannel:
				// Update the map of mudders with the latest registered mudder.
				newMudder := new(MudderInfo);
				newMudder.Address = mudderAddress;
				newMudder.SetMudderHealth(kMudderFree);
				canton.mudders[newMudder.Address] = newMudder;
				log.Printf("Completed registering worker %s", newMudder.Address);
			case <- time.After(5 * time.Millisecond):
				exitStatus = 1;
				break;
			}
			if exitStatus == 1 {
				break;
			}
		}

		// Monitor the mudders that are sent on the doneWithMudder channel
		// and if alive, mark them as kMudderFree or else mark them as
		// kMudderDead. Wait upto 5 milliseconds and then return to pinging.
		for {
			exitStatus := 0;
			select {
			case mudderAddress := <- canton.doneWithMudder:
				if canton.mudders[mudderAddress].DeadPingCount < DeadPings {
					canton.mudders[mudderAddress].SetMudderHealth(kMudderFree);
				}
			case <- time.After(5 * time.Millisecond):
				exitStatus = 1;
				break;
			}
			if exitStatus == 1 {
				break;
			}
		}
		// Sleep for PingInterval.
		time.Sleep(PingInterval)
	}
}

/*
Print all mudders registered in Canton
 */
func (canton *Canton) PrintMudders() {
	for _, mudderInfo := range canton.mudders {
		mudderInfo.PrintMudderConfig()
	}
}

/*
Check to see if any mudder is kMudderFree. If so, return that mudder's address.
 */
func (canton *Canton) getAnAvailableMudder() string {
	for {
		for mudderAddress, mudderInfo := range canton.mudders {
			if mudderInfo.GetMudderHealth() == kMudderFree {
				log.Printf("Fetching worker %s", mudderAddress)
				mudderInfo.SetMudderHealth(kMudderBusy)
				return mudderAddress;
			}
		}
	}
}

/*
This performs the operations laid out in MapReduce
*/
func (canton *Canton) StartFactory(mr *MapReduce) {
	log.Printf("Splitting data source");
	mr.Split(mr.file);
	log.Printf("Completed splitting data source");
	log.Printf("Starting MapReduce jobs.");
	canton.processJobs(mr.GetMapJobs());
	canton.processJobs(mr.GetReduceJobs());
	log.Printf("Completed MapReduce jobs.");
	log.Printf("Merging MapReduce Results.");
	mr.Merge();
	log.Printf("Merged MapReduce Results.");
	// log.Printf("StartFactory: Waiting to put something into the DoneChannel")
	canton.DoneChannel <- true
	// log.Printf("StartFactory: Updated DoneChannel with status true")
}

/*
Magistrate Main Function
*/
func (canton *Canton) RunMagistrate(mr *MapReduce, signalComplete chan bool) {
	log.Printf("Starting registration server");
	canton.startRegistrationServer();
	log.Printf("Starting Milk Manager");
	go canton.monitorMudders();
	canton.StartFactory(mr);
	<- canton.DoneChannel;
	log.Printf("Run Magistrate done.")
	signalComplete <- true;
}

/*
Farm jobs as they keep coming in(from the jobs channel) to the first available
mudder. Collect the status of the job after it is done and pass it along on the
return channel.
*/
func (canton *Canton) farmJobs(jobsChannel chan *JobInfo,
	jobsStatusChannel chan *JobStatus) {
	for {
		job, isChannelOpen := <- jobsChannel;
		if isChannelOpen {
			mudderAddress := canton.getAnAvailableMudder();
			if mudderAddress == "" {
				log.Fatal("There are no mudders available")
			}
			args := &DoJobArgs{job.file, job.operation, job.id, job.otherPhase};
			var reply DoJobReply;
			go func() {
				ok := call(mudderAddress, "Mudder.DoJob", args, &reply)
				jobStatus := new(JobStatus);
				jobStatus.id = job.id;
				if !ok {
					// There was a failure. Update the status accordingly
					jobStatus.status = kFail;
				} else {
					jobStatus.status = kSuccess;
				}
				jobsStatusChannel <- jobStatus;
				// Let milk manager know that we are done with this mudder.
				canton.doneWithMudder <- mudderAddress;
			} ()
		} else {
			log.Printf("All jobs have been completed.")
			break
		}
	}
}

/*
Check if all jobs have been completed successfully.
 */
func (canton *Canton) areJobsDone(jobs *Jobs) bool {
	jobsMap := jobs.jobs;
	for _, job := range jobsMap {
		if job.completionStatus != kSuccess {
			return false;
		}
	}
	return true;
}

/*
Queue jobs that had previously failed.
 */
func (canton *Canton) queueFailedJobs(jobs *Jobs, jobsChannel chan *JobInfo) {
	jobsMap := jobs.jobs;
	for _, job := range jobsMap {
		if job.completionStatus == kFail {
			// Add failed jobs to the queue again.
			log.Printf("Queueing job %d that had previously failed", job.id)
			jobsChannel <- job;
			job.completionStatus = kWaiting;
		}
	}
}

/*
Process the jobs i.e. farm out the jobs to the mudders,
collect the status and queue failed jobs accordingly.
*/
func (canton *Canton) processJobs(jobs *Jobs) {

	// Since every job is handled by a separate thread we need
	// channels to communicate with the threads.
	// Create two channels one to communicate jobs to be done
	// and the second to communicate the job completion status.
	jobsChannel := make(chan *JobInfo, 100)
	jobsStatusChannel := make(chan *JobStatus, 100)

	// Farm out the jobs on a separate thread.
	go canton.farmJobs(jobsChannel, jobsStatusChannel);

	// Add all the jobs for the first time.
	jobsMap := jobs.jobs;
	for _, job := range jobsMap {
		if job.completionStatus == kIncomplete {
			jobsChannel <- job;
			job.completionStatus = kWaiting;
		}
	}

	// Process all jobs that have been farmed and re-queue those jobs
	// that have failed. If all jobs are complete, indicate to the farmer
	// thread that all jobs have been completed by closing the
	// communication channels.
	for {
		if !canton.areJobsDone(jobs) {
			// Some jobs are either in waiting state or have failed.
			// Add all jobs that had failed previously.
			canton.queueFailedJobs(jobs, jobsChannel);

			// Wait on the status channel for a job to complete.
			jobStatus := <- jobsStatusChannel;
			jobs.UpdateJobStatus(jobStatus.id, jobStatus.status);
			// Update the jobs map with the status.
		} else {
			// All jobs are done. Close the jobsChannel and jobsStatusChannel
			close(jobsChannel);
			close(jobsStatusChannel);
			break;
		}
	}
}

func (canton *Canton) Register(args *RegisterArgs, res *RegisterReply) error {
	log.Printf("Registering worker %s", args.Mudder)
	canton.registerChannel <- args.Mudder
	// Let the mudder know that he will be registered soon.
	res.OK = true
	return nil
}
