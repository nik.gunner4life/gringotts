package viewservice

import "net/rpc"
import "fmt"
import "log"
import "time"

//
// the viewservice Clerk lives in the client
// and maintains a little state.
//
type Clerk struct {
	// client's name (host:port)
	me string;
	// viewservice's host:port
  	server string;
	// The current view that I have.
	currentView View;

}

func MakeClerk(me string, server string) *Clerk {
	ck := new(Clerk)
  	ck.me = me
  	ck.server = server
	ck.currentView = View{0, "", ""};
  	return ck
}

//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the reply's contents are only valid if call() returned true.
//
// you should assume that call() will time out and return an
// error after a while if it doesn't get a reply from the server.
//
// please use call() to send all RPCs, in client.go and server.go.
// please don't change this function.
//
func call(srv string, rpcname string,
          args interface{}, reply interface{}) bool {
  c, errx := rpc.Dial("unix", srv)
  if errx != nil {
    return false
  }
  defer c.Close()
    
  err := c.Call(rpcname, args, reply)
  if err == nil {
    return true
  }

  fmt.Println(err)
  return false
}

func (ck *Clerk) Ping(viewnum uint) (View, error) {
	// prepare the arguments.
  	args := &PingArgs{}
  	args.Me = ck.me
  	args.Viewnum = viewnum
  	var reply PingReply
	log.Printf("Clerk %s pinging viewservice at %d with viewnum: %d",
		args.Me, (time.Now().UnixNano() / 1000000), viewnum)
  	// send an RPC request, wait for the reply.
  	ok := call(ck.server, "ViewServer.Ping", args, &reply)
  	if ok == false {
	    return View{}, fmt.Errorf("Ping(%v) failed", viewnum)
  	}
	ck.currentView = reply.View;
  	return reply.View, nil
}

func (ck *Clerk) Get() (View, bool) {
	args := &GetArgs{ck.me, ck.currentView.Viewnum}
  	var reply GetReply
	log.Printf("Clerk %s querying viewservice at %d with viewnum: %d", args.Me,
		(time.Now().UnixNano() / 1000000), ck.currentView.Viewnum)
  	ok := call(ck.server, "ViewServer.Get", args, &reply)
  	if ok == false {
	    return View{}, false
  	}
	ck.currentView = reply.View;
  	return reply.View, true
}

func (ck *Clerk) Primary() string {
	v, ok := ck.Get()
  	if ok {
	    return v.Primary
  	}
  	return ""
}
