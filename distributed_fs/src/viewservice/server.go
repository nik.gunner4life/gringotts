
package viewservice

import "net"
import "net/rpc"
import "log"
import "time"
import "sync"
import "fmt"
import "os"


/*
Stores all the relevant information for a clerk.
*/
type ClerkInfo struct {
	mu sync.Mutex;
	// Address of the clerk.
	clerkAddress string;
	// The last time the client pinged.
	lastPingTimestamp int64;
	// The last view of the clerk
	lastViewNum uint;
	// The status of the clerk.
	status int;
}


/*
A structure to maintain a map of clerks. Since maps in golang are not thread
safe by default, we need to explicitly handle the thread safety. Since multiple
threads can access the clerks information simultaneously, we wrap the map
with a mutex to ensure safe access.
*/
type Clerks struct {
	// 	DO NOT ACCESS THESE OUTSIDE OF THE CLASS. IT MIGHT BE UNSAFE.
	// A mutex to ensure all accesses to clerks are thread safe.
	clerksMutex sync.Mutex;
	// A map of the clerk address to their information.
	clerksMap map[string]*ClerkInfo;
}


/*
Initialize the Clerks data structure.
*/
func InitClerks() *Clerks {
	clerks := new(Clerks);
	clerks.clerksMap = make(map[string]*ClerkInfo);
	return clerks;
}


/*
Adds a new clerk to the clerksMap.
*/
func (clerks *Clerks) addClerk(clerkInfo ClerkInfo) {
	clerks.clerksMutex.Lock();
	defer clerks.clerksMutex.Unlock();
	clerkToAdd := new(ClerkInfo);

	clerkToAdd.clerkAddress = clerkInfo.clerkAddress;
	clerkToAdd.lastPingTimestamp = clerkInfo.lastPingTimestamp;
	clerkToAdd.lastViewNum = clerkInfo.lastViewNum;
	clerkToAdd.status = clerkInfo.status;

	clerks.clerksMap[clerkToAdd.clerkAddress] = clerkToAdd;
	log.Printf("Added clerk %s to clerks map",
		clerks.clerksMap[clerkToAdd.clerkAddress].clerkAddress)
}


/*
Update the ping timestamp and viewnum of the clerk with the specified values.
*/
func (clerks *Clerks) updateClerkWithPingInfo(address string, viewnum uint,
	timestamp int64) bool {
	clerks.clerksMutex.Lock();
	defer clerks.clerksMutex.Unlock();
	clerk, ok := clerks.clerksMap[address];
	if !ok {
		log.Panicf("ERROR: Cannot update clerk %s as it does not exist.",
			address)
		return false;
	} else {
		clerk.lastViewNum = viewnum;
		clerk.lastPingTimestamp = timestamp;
//		log.Printf("Updated clerk ping info for clerk: %s. View: %d, Ping: %d",
//			address, clerk.lastViewNum, clerk.lastPingTimestamp);
		return true;
	}
}


/*
Makes a copy of the clerk information and returns the copy. Copies are useful
as they ensure that state information is not modifiable from anywhere.
*/
func (clerks *Clerks) getcopy(clerk *ClerkInfo) ClerkInfo {
	clerkInfo := ClerkInfo{};
	clerkInfo.lastPingTimestamp = clerk.lastPingTimestamp;
	clerkInfo.lastViewNum = clerk.lastViewNum;
	clerkInfo.status = clerk.status;
	clerkInfo.clerkAddress = clerk.clerkAddress;
	return clerkInfo;
}


/*
Makes a copy of the clerk information and returns the copy. Copies are useful
as they ensure that state information is not modifiable from anywhere.
*/
func (clerks *Clerks) getobject(clerk *ClerkInfo) *ClerkInfo {
	clerkInfo := new(ClerkInfo);
	clerkInfo.lastPingTimestamp = clerk.lastPingTimestamp;
	clerkInfo.lastViewNum = clerk.lastViewNum;
	clerkInfo.status = clerk.status;
	clerkInfo.clerkAddress = clerk.clerkAddress;
	return clerkInfo;
}


/*
Returns copy of the clerk based on the address.
*/
func (clerks *Clerks) getClerk(address string) (ClerkInfo, bool) {
	clerks.clerksMutex.Lock();
	defer clerks.clerksMutex.Unlock();
	clerk, ok := clerks.clerksMap[address];
	if !ok {
		log.Printf("Cannot find clerk %s in clerks map.", address)
		return ClerkInfo{}, false;
	} else {
		return clerks.getcopy(clerk), true;
	}
}


/*
Return a copy of all clerks
*/
func (clerks *Clerks) getAllClerks() map[string]*ClerkInfo {
	clerks.clerksMutex.Lock();
	defer clerks.clerksMutex.Unlock();
	tmpClerks := make(map[string]*ClerkInfo);
	for address, clerk := range clerks.clerksMap {
		tmpClerks[address] = clerks.getobject(clerk);
	}
	return tmpClerks;
}


/*
Maintains the state for the view server. This includes maintaining
	a) The current View
	b) The new View
	c) The registered clerks
	d) Current clerk Status
	and so on.
*/
type ViewServer struct {
  	l net.Listener;
  	dead bool;
  	me string;
	// A map of clerks that have registered with the view server.
	clerks *Clerks;
	// Maintain the current view. Also maintain a state that tells us if the
	// current view is valid.
	currentViewMutex sync.Mutex;
	currentView View;
	currentViewValid bool;

	// Maintain the new view that might be updated to the current view.
	newViewMutex sync.Mutex;
	newView View;
	// Maintain a variable to let the threads handling Ping know that a
	// new view may or may not exist
	maybeNewView bool;

	// Maintain a variable that lets Ping know whether it can send the new view
	// to the backup in the new view.
	sendNewViewToBackupMutex sync.Mutex;
	sendNewViewToBackup bool;
}


/*
Initialize the view server and return a view server object.
*/
func InitViewServer(address string) *ViewServer {
	log.Printf("Initializing viewserver.")
	vs := new(ViewServer);
	vs.clerks = InitClerks();
	vs.dead = false;
	vs.me = address;
	vs.currentView = View{};
	vs.currentView.Viewnum = 0;
	vs.currentView.Primary = "";
	vs.currentView.Backup = "";
	vs.newView = View{};
	vs.maybeNewView = false;
	vs.currentViewValid = false;
	vs.sendNewViewToBackup = false;
	return vs;
}


/*
Return the newView.
*/
func (vs *ViewServer) getNewView() View {
	vs.newViewMutex.Lock();
	defer vs.newViewMutex.Unlock();
	return vs.newView;
}


/*
Check if there is a new view and return True if one exists. False otherwise.
*/
func (vs *ViewServer) isNewViewSet() bool {
	vs.newViewMutex.Lock();
	defer vs.newViewMutex.Unlock();
	return vs.maybeNewView;
}

/*
Enables the maybeNewView and sets newView with supplied value.
*/
func (vs *ViewServer) enableNewView(view View) {
	vs.newViewMutex.Lock();
	defer vs.newViewMutex.Unlock();
	vs.newView = view;
	vs.maybeNewView = true;
	log.Printf("Set new view as %s", vs.newView.AsString())
}


/*
Disables the maybeNewView
*/
func (vs *ViewServer) disableNewView() {
	vs.newViewMutex.Lock();
	defer vs.newViewMutex.Unlock();
	vs.maybeNewView = false;
}


/*
Return the current view.
*/
func (vs *ViewServer) getCurrentView() View {
	vs.currentViewMutex.Lock();
	defer vs.currentViewMutex.Unlock();
	return vs.currentView;
}


/*
vs.isCurrentViewValid holds the value that you seek. Take it.
*/
func (vs *ViewServer) isCurrentViewValid() bool {
	vs.currentViewMutex.Lock();
	defer vs.currentViewMutex.Unlock();
	return vs.currentViewValid;
}

/*
Enables current view and sets the current view as the supplied view.
*/
func (vs *ViewServer) enableCurrentView(view View) {
	vs.currentViewMutex.Lock();
	defer vs.currentViewMutex.Unlock();
	vs.currentView = view;
	vs.currentViewValid = true;
	log.Printf("Set current view as %s", vs.currentView.AsString())
}


/*
Disables the current view.
*/
func (vs *ViewServer) disableCurrentView() {
	vs.currentViewMutex.Lock();
	defer vs.currentViewMutex.Unlock();
	vs.currentViewValid = false;
}

/*
To be used by tick to let ping know that backup can receive new view.
*/
func (vs *ViewServer) enableBackupToReceiveNewView() {
	vs.sendNewViewToBackupMutex.Lock();
	defer vs.sendNewViewToBackupMutex.Unlock();
	vs.sendNewViewToBackup = true;
}


/*
To be used by tick to let Ping know that new view is not available to the backup.
*/
func (vs *ViewServer) disableBackupFromReceivingNewView() {
	vs.sendNewViewToBackupMutex.Lock();
	defer vs.sendNewViewToBackupMutex.Unlock();
	vs.sendNewViewToBackup = false;
}


/*
Used by ping to determine whether backup can receive new view.
*/
func (vs *ViewServer) canBackupReceiveNewView() bool {
	vs.sendNewViewToBackupMutex.Lock();
	defer vs.sendNewViewToBackupMutex.Unlock();
	return vs.sendNewViewToBackup;
}

/******************************************************************************
Captures all functions that will be performed by the tick() thread.
*******************************************************************************/

/*
Spin forever till primary acknowledges the view.
*/
func (vs *ViewServer) waitForPrimaryToAcknowledgeView(view View) bool {
	sleepTime := time.Nanosecond * 100;
	log.Printf("Waiting for primary to acknowledge view %s", view.AsString());
	// Aggressively monitor to find out whether primary has acknowledged the
	// view.
	for {
		clerk, ok := vs.clerks.getClerk(view.Primary);
		if !ok {
			log.Panic("Cannot find primary from new view in clerks map.")
			return false
		}
		if clerk.lastViewNum == view.Viewnum {
			log.Printf("Primary %s has acknowledged view %s",
				clerk.clerkAddress, view.AsString())
			return true
		}
		time.Sleep(sleepTime);
	}
}


/*
Here we check to see if the backup has acknowledged the given view. If not,
we wait for upto deadping intervals for backup to acknowledge. If backup does not
we return false. Otherwise, we return true.
*/
func (vs *ViewServer) waitForBackupToAcknowledgeView(view View) bool {
	if view.Backup == "" {
		return true;
	}
	clerk, ok := vs.clerks.getClerk(view.Backup);
	if !ok {
		log.Panic("Cannot find backup from new view in clerks map.")
		return false
	}
	if clerk.lastViewNum == view.Viewnum {
		log.Printf("Backup %s last viewnum matches the new viewnum. Clerk lastviewnum: %d, New View num: %d",
			clerk.clerkAddress, clerk.lastViewNum, view.Viewnum)
		return true
	}

	log.Printf("Waiting for backup to acknowledge view %s", view.AsString());
	sleepTime := time.Nanosecond * 100;
	startTime := time.Now().UnixNano() / 1000000;
	constFactor := (DeadPings * int64(PingInterval)) / 1000000;
	// Aggressively monitor to determine whether backup has acknowledged the
	// view.
	for {
		clerk, _ = vs.clerks.getClerk(view.Backup);
		if clerk.lastViewNum == view.Viewnum {
			log.Printf("Backup %s has also acknowledged view %s",
				clerk.clerkAddress, view.AsString())
			return true
		}
		currentTime := time.Now().UnixNano() / 1000000;
		if currentTime >= startTime + constFactor {
			log.Printf("ERROR: Backup %s has not acknowledged the view %s.",
				clerk.clerkAddress, view.AsString())
			return false;
		}
		time.Sleep(sleepTime);
	}
}


/*
Pick the first clerk that is alive. Ignore clerks in the ignoreClerks map.
*/
func (vs *ViewServer) pickALiveServer(allClerks map[string]*ClerkInfo,
	ignoreClerks map[string]bool) string {
	for address, clerkInfo := range allClerks {
		_, ok := ignoreClerks[address];
		if ok {
			continue;
		}
		if clerkInfo.status == kClerkAlive {
			return address;
		}
	}
	return "";
}


/*
Check if clerk is alive and it's viewnum matches the current viewnum.
*/
func (vs *ViewServer) checkClerk (allClerks map[string]*ClerkInfo,
	clerkAddress string, currentView View) bool {
	clerk, ok := allClerks[clerkAddress];
	if !ok {
		log.Panicf("Cannot check state of clerk %s", clerkAddress);
		return false
	} else {
		if (clerk.status == kClerkAlive &&
			clerk.lastViewNum == currentView.Viewnum) {
			return true;
		} else {
			log.Printf("ERROR: The clerk %s is not fine. LastViewNum: %d, Status: %d",
				clerkAddress, clerk.lastViewNum, clerk.status)
			return false;
		}
	}
}


/*
A helper function to handle the new view to current view transition.
*/
func (vs *ViewServer) performViewTransition(newView View) {
	currentView := vs.getCurrentView();
	log.Printf("Starting view transition from current view: %s to new view: %s",
		currentView.AsString(), newView.AsString())
	// We have a new view. Disable the current view and update the main new
	// view and enable new view.
	vs.disableBackupFromReceivingNewView();
	vs.enableNewView(newView);
	vs.disableCurrentView();

	// Once primary has acknowledged the view, replace the current view
	// with the new view and disable the new view.
	if vs.waitForPrimaryToAcknowledgeView(newView) {
//		log.Printf("Completed view transition. New view: %s",
//			vs.getCurrentView().AsString())
		vs.enableBackupToReceiveNewView();
		// Irrespective of what happens here, we will move forward.
		vs.waitForBackupToAcknowledgeView(newView);
		vs.enableCurrentView(newView);
		vs.disableNewView();
	} else {
		log.Panic("We should never have come here.")
	}
}


/*
Identify whether the primary or backup is kClerkDead. If so, change the
view. There are a few scenarios here. If the primary is dead, then
promote the backup to be the primary if one exists then pick a new backup
if one exists and update the new view.
If the backup is dead, pick a new backup if one exists and update the
new view.
Once a new view has been created, wait till the new primary
acknowledges the view. Once the new primary has acknowledged the view,
change the new view to the current view.
*/
func (vs *ViewServer) tick() {
	// Get a copy of the clerks. We will update the main map later.
	allClerks := vs.clerks.getAllClerks();
	currentView := vs.getCurrentView();
	currentTime := time.Now().UnixNano();
	pingInterval := int64(PingInterval);

	// Loop over all clerks and mark them as alive or dead.
	for _, clerkInfo := range allClerks {
		lastPingTimestamp := (clerkInfo.lastPingTimestamp / 1000000);
		constFactor := (DeadPings * pingInterval) / 1000000;
		if (currentTime / 1000000) >= (lastPingTimestamp + constFactor) {
			// The clerk has not pinged for at least DeadPings PingInterval.
			// Mark the clerk as dead.
			clerkInfo.status = kClerkDead;
			log.Printf("ERROR: Marked clerk %s as dead as clerk has not pinged in %d ms",
				clerkInfo.clerkAddress,
					(currentTime / 1000000) - lastPingTimestamp);
		} else {
			clerkInfo.status = kClerkAlive;
		}
	}

	//If the current viewnum is 0, we need to create a new view.
	if currentView.Viewnum == 0 {
		log.Printf("Choosing the first view");
		// Choose a new primary
		ignoreClerks := make(map[string]bool)
		primary := vs.pickALiveServer(allClerks, ignoreClerks);
		if primary == "" {
			log.Printf("WARNING: No primary found. Cannot create new view.")
			return
		}
		newView := View{currentView.Viewnum + 1, primary, ""};
		vs.performViewTransition(newView);
		return
	} else {
		// View number is greater than 0.
		if vs.checkClerk(allClerks, currentView.Primary, currentView) {
			if currentView.Backup != "" {
				// If in the previous iteration, we have changed the view, the
				// new backup may still not be aware of it. Wait for a little
				// while for backup to acknowledge. If backup does not,
				// change the view again.
				if vs.checkClerk(allClerks, currentView.Backup, currentView) {
					// The backup is fine as well. The current view does not
					// need to be changed.
				} else {
					// The backup is not fine. Remove the backup and pick a
					// spare server as backup.
					ignoreClerks := make(map[string]bool);
					ignoreClerks[currentView.Primary] = true;
					ignoreClerks[currentView.Backup] = true;
					// Irrespective of the backup string that we get, the
					// view needs to be changed.
					backup := vs.pickALiveServer(allClerks, ignoreClerks);
					log.Printf("Choosing clerk: %s as backup.", backup);
					newView := View{currentView.Viewnum + 1,
						currentView.Primary, backup};
					vs.performViewTransition(newView);
				}
			} else {
				// There is no backup. Pick a new backup.
				ignoreClerks := make(map[string]bool);
				ignoreClerks[currentView.Primary] = true;
				backup := vs.pickALiveServer(allClerks, ignoreClerks);
				if backup != "" {
					// There is a spare server that can act as backup. Create
					// a new view and transition to the new view.
					log.Printf("Choosing clerk: %s as backup.", backup);
					newView := View{currentView.Viewnum + 1,
						currentView.Primary, backup};
					vs.performViewTransition(newView);
				} else {
					// There is no spare server that can act as backup.
					// The view remains the same
				}
			}
		} else {
			// The primary is not fine. Check if backup is fine.
			if currentView.Backup == "" ||
					!vs.checkClerk(allClerks, currentView.Backup, currentView){
				log.Printf("We cannot make forward progress as the primary has died and backup has not responded.");
				return;
			} else {
				// The backup is fine. Create a new view promoting the current
				// backup to the new primary.
				newView := View{currentView.Viewnum + 1, currentView.Backup, ""};
				vs.performViewTransition(newView);
				return;
			}
		}
	}
}

/******************************************************************************/

/*******************************************************************************
Captures all functions that are performed by the Ping and Get thread(s). There
can be multiple threads that are executing these functions concurrently.
*******************************************************************************/

/*
When a ping is received from a clerk, if we do not have the clerk registered
earlier, create a new clerk with the necessary information and add the clerk
to the clerks map. If the clerk already exists, simply update the last
timestamp and last Viewnum that was received.
 */

func (vs *ViewServer) ping(clerkAddress string, clerkViewnum uint) View {
	_, ok := vs.clerks.getClerk(clerkAddress);
	currentTime := time.Now().UnixNano();
	if !ok {
		// Must be a new clerk. Register the new clerk.
		if clerkViewnum == 0 {
			log.Printf("Registering clerk %s", clerkAddress)
		} else {
			log.Panicf("Received viewnum %d (vs. 0) from an uregistered clerk %s.",
				clerkViewnum, clerkAddress);
		}
		newClerk := ClerkInfo{};
		newClerk.lastPingTimestamp = currentTime;
		newClerk.clerkAddress = clerkAddress;
		newClerk.lastViewNum = clerkViewnum;
		vs.clerks.addClerk(newClerk);
	} else {
		vs.clerks.updateClerkWithPingInfo(clerkAddress, clerkViewnum, currentTime)
	}

	// The view that needs to be sent to the clerk can be different for
	// different clerks. This can happen when we are changing the view.
	// To account for this, we will use a separate function that will return
	// view based on the clerk.
	replyView := vs.getReplyView(clerkAddress, clerkViewnum);
	return replyView;
}


/*
If new view is set, return the new view to the primary.
*/
func (vs *ViewServer) getReplyView(clerkAddress string, clerkViewnum uint) View {
	defaultView := View{0, "", ""};

	// If new view is valid, return the new view to the primary alone.
	if vs.isNewViewSet() {
		newView := vs.getNewView();
		// If the primary has died and recovered between pings, we should not
		// send it the new view. The primary's view should always be the
		// previous view or the new view.
		viewnumStatus := (clerkViewnum == newView.Viewnum) ||
				(clerkViewnum == newView.Viewnum - 1)
		if (newView.Primary == clerkAddress) && viewnumStatus {
			// The primary in the new view matches the clerkAddress. Send it the
			// new view.
			log.Printf("Returning NEW view %s to new PRIMARY clerk %s",
				newView.AsString(), clerkAddress)
			return newView;
		} else if newView.Backup == clerkAddress {
			if vs.canBackupReceiveNewView() {
				log.Printf("Returning NEW view %s to new BACKUP clerk %s",
					newView.AsString(), clerkAddress)
				return newView;
			} else {
				log.Printf("Returning DEFAULT view %s to may be new backup clerk %s ",
					defaultView.AsString(), clerkAddress)
				return defaultView;
			}
		} else {
			log.Printf("Returning DEFAULT view %s to non-primary clerk %s ",
				defaultView.AsString(), clerkAddress)
			return defaultView;
		}
	}

	// If current view is valid, return the current view to the primary/backup.
	if vs.isCurrentViewValid() {
		currentView := vs.getCurrentView();
		clerk, _ := vs.clerks.getClerk(clerkAddress);
		if ((currentView.Primary == clerkAddress ||
			currentView.Backup == clerkAddress) &&
				(currentView.Viewnum == clerk.lastViewNum)) {
			log.Printf("Returning CURRENT view %s to clerk %s",
				currentView.AsString(), clerkAddress)
			return currentView;
		} else {
			log.Printf("Returning DEFAULT view %s to clerk %s as it was not in current view or viewnums did not match.",
				defaultView.AsString(), clerkAddress)
			return defaultView;
		}
	} else {
		log.Printf("Returning DEFAULT view %s to clerk %s as current view is invalid.",
			defaultView.AsString(), clerkAddress)
		return defaultView;
	}
}


/*
"Ping" does exactly what "ping" does. Refer func ping.
*/
func (vs *ViewServer) Ping(args *PingArgs, reply *PingReply) error {
	replyView := vs.ping(args.Me, args.Viewnum)
	reply.View = replyView;
	return nil;
}


/*
Same as ping. Return the replyView.
*/
func (vs *ViewServer) Get(args *GetArgs, reply *GetReply) error {
	replyView := vs.ping(args.Me, args.Viewnum)
	reply.View = replyView;
	return nil;
}

func (vs *ViewServer) GetView(args *GetArgs, reply *GetReply) error {
	reply.View = vs.getNewView();
	return nil;
}


/******************************************************************************/

/*
Tell the server to shutdown. Purely for testing purposes.
*/
func (vs *ViewServer) Kill() {
	vs.dead = true
  	vs.l.Close()
}


/*
Start the View server.
*/
func StartServer(me string) *ViewServer {
	vs := InitViewServer(me)

  	// tell net/rpc about our RPC server and handlers.
  	rpcs := rpc.NewServer()
  	rpcs.Register(vs)

  	// prepare to receive connections from clients.
  	// change "unix" to "tcp" to use over a network.
  	os.Remove(vs.me) // only needed for "unix"
  	l, e := net.Listen("unix", vs.me);
  	if e != nil {
	    log.Fatal("listen error: ", e);
  	}
  	vs.l = l

  	// please don't change any of the following code,
  	// or do anything to subvert it.

  	// create a thread to accept RPC connections from clients.
  	go func() {
	    for vs.dead == false {
	      conn, err := vs.l.Accept()
	      if err == nil && vs.dead == false {
	        go rpcs.ServeConn(conn)
	      } else if err == nil {
	        conn.Close()
	      }
	      if err != nil && vs.dead == false {
	        fmt.Printf("ViewServer(%v) accept: %v\n", me, err.Error())
	        vs.Kill()
	      }
	    }
  	}()

  	// create a thread to call tick() periodically.
  	go func() {
	    for vs.dead == false {
	      vs.tick()
	      time.Sleep(PingInterval)
	    }
  	}()
  	return vs
}

