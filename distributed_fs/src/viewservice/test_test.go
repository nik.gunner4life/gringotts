package viewservice

import "testing"
import "runtime"
import "time"
import "fmt"
import "os"
import "strconv"

func check(t *testing.T, ck *Clerk, p string, b string, n uint) {
	view, _ := ck.Get()
  	if view.Primary != p {
    	t.Fatalf("wanted primary %v, got %v", p, view.Primary)
	}
  	if view.Backup != b {
		t.Fatalf("wanted backup %v, got %v", b, view.Backup)
  	}
  	if n != 0 && n != view.Viewnum {
    	t.Fatalf("wanted viewnum %v, got %v", n, view.Viewnum)
  	}
  	if ck.Primary() != p {
    	t.Fatalf("wanted primary %v, got %v", p, ck.Primary())
  	}
}

func port(suffix string) string {
	s := "/var/tmp/824-"
  	s += strconv.Itoa(os.Getuid()) + "/"
  	os.Mkdir(s, 0777)
  	s += "viewserver-"
  	s += strconv.Itoa(os.Getpid()) + "-"
  	s += suffix
  	return s
}


func Test1(t *testing.T) {
	runtime.GOMAXPROCS(4)
	vshost := port("v")
  	vs := StartServer(vshost)
	ck1 := MakeClerk(port("1"), vshost)
  	ck2 := MakeClerk(port("2"), vshost)
  	ck3 := MakeClerk(port("3"), vshost)
	if ck1.Primary() != "" {
		t.Fatalf("there was a primary too soon")
  	}

  	// very first primary
  	fmt.Println("\n\n\n*******************************************************")
  	fmt.Printf("Test 1: First primary ...\n\n")

  	for i := 0; i < DeadPings * 2; i++ {
		  view, _ := ck1.Ping(0)
	      if view.Primary == ck1.me {
	      	  break
	      }
	      time.Sleep(PingInterval)
  	}
  	check(t, ck1, ck1.me, "", 1)
  	fmt.Printf("  ... Passed\n")

  	// very first backup
  	fmt.Println("\n\n\n*******************************************************")
  	fmt.Printf("Test 2: First backup ...\n\n")
  	{
		vx, _ := ck1.Get()
		ck1ViewNum := uint(1);
		ck2ViewNum := uint(0);
	    for i := 0; i < DeadPings * 2; i++ {
	    	ck1View, _ := ck1.Ping(ck1ViewNum)
		  	ck1ViewNum = ck1View.Viewnum;
	      	view, _ := ck2.Ping(ck2ViewNum)
		  	ck2ViewNum = view.Viewnum;
	      	if view.Backup == ck2.me {
	        	break
			}
	      	time.Sleep(PingInterval)
		}
	    check(t, ck1, ck1.me, ck2.me, vx.Viewnum + 1)
  	}
    fmt.Printf("  ... Passed\n")

  	// primary dies, backup should take over
  	fmt.Println("\n\n\n*******************************************************")
  	fmt.Printf("Test 3: Backup takes over if primary fails ...\n\n")

  	{
		ck1.Ping(2)
      	vx, _ := ck2.Ping(2)
      	for i := 0; i < DeadPings * 2; i++ {
			v, _ := ck2.Ping(vx.Viewnum)
		  	if v.Primary == ck2.me && v.Backup == "" {
				break
      	  	}
      	  	time.Sleep(PingInterval)
	  	}
      	check(t, ck2, ck2.me, "", vx.Viewnum + 1)
  	}
  	fmt.Printf("  ... Passed\n")

  	// revive ck1, should become backup
  	fmt.Println("\n\n\n*******************************************************")
  	fmt.Printf("Test 4: Restarted server becomes backup ...\n\n")

  	{
		vx, _ := ck2.Get()
	  	ck1ViewNum := uint(0);
	  	ck2ViewNum := uint(vx.Viewnum)
      	ck2.Ping(ck2ViewNum)
      	for i := 0; i < DeadPings * 2; i++ {
			ck1View, _ := ck1.Ping(ck1ViewNum)
		  	ck1ViewNum = ck1View.Viewnum;
		  	ck2View, _ := ck2.Ping(ck2ViewNum)
		  	ck2ViewNum = ck2View.Viewnum;
		  	if ck2View.Primary == ck2.me && ck2View.Backup == ck1.me {
				// Just to ensure that my viewservice is in a fine stete.
				ck2.Ping(ck2ViewNum)
			  	break
		  	}
		  	time.Sleep(PingInterval)
	  	}
	  	check(t, ck2, ck2.me, ck1.me, vx.Viewnum + 1)
  	}
  	fmt.Printf("  ... Passed\n")

  	// start ck3, kill the primary (ck2), the previous backup (ck1)
  	// should become the server, and ck3 the backup
  	fmt.Println("\n\n\n*******************************************************")
  	fmt.Printf("Test 5: Idle third server becomes backup if primary fails ...\n\n")

  	{
		vx, _ := ck2.Get()
		ck1View, _ := ck1.Get()
	  	ck1ViewNum := uint(ck1View.Viewnum)
	  	ck3ViewNum := uint(0)
      	ck2.Ping(vx.Viewnum)
	  	// CK2 is now dead as we will not be pinging.
      	for i := 0; i < DeadPings * 2; i++ {
			ck3View, _ := ck3.Ping(ck3ViewNum);
			ck3ViewNum = ck3View.Viewnum;
		  	v, _ := ck1.Ping(ck1ViewNum)
		  	ck1ViewNum = v.Viewnum;
          	if v.Primary == ck1.me && v.Backup == ck3.me {
          		// So that the viewservice is sane again.
				ck1.Ping(ck1ViewNum);
				break;
          	}
      	  	vx = v
      	  	time.Sleep(PingInterval)
	  	}
      	check(t, ck1, ck1.me, ck3.me, vx.Viewnum + 1)
  	}
  	fmt.Printf("  ... Passed\n")


  	// kill and immediately restart the primary -- does viewservice
  	// conclude primary is down even though it's pinging?
	fmt.Println("\n\n\n*********************************************************")
  	fmt.Printf("Test 6: Restarted primary treated as dead ...\n\n")

  	{
		vx, _ := ck1.Get();
		ck3View, _ := ck3.Get();
		ck3ViewNum := ck3View.Viewnum;
    	ck1.Ping(vx.Viewnum);
    	for i := 0; i < DeadPings * 2; i++ {
			ck1.Ping(0);
      		ck3View, _ = ck3.Ping(ck3ViewNum);
			ck3ViewNum = ck3View.Viewnum;
      		v, _ := ck3.Get();
      		if v.Primary != ck1.me {
				break;
			}
		    time.Sleep(PingInterval);
	    }
    	vy, _ := ck3.Get();
    	if vy.Primary != ck3.me {
      		t.Fatalf("expected primary=%v, got %v\n", ck3.me, vy.Primary);
    	}
    }
    fmt.Printf("  ... Passed\n");




  	// set up a view with just 3 as primary,
  	// to prepare for the next test.
	fmt.Println("\n\n\n*********************************************************")
  	fmt.Printf("Setting up future tests\n\n")
  	{
    	for i := 0; i < DeadPings * 3; i++ {
      		vx, _ := ck3.Get()
      		ck3.Ping(vx.Viewnum)
      		time.Sleep(PingInterval)
    	}
    	v, _ := ck3.Get()
    	if v.Primary != ck3.me || v.Backup != "" {
      		t.Fatalf("wrong primary or backup")
    	}
  	}

  	// does viewserver wait for ack of previous view before
  	// starting the next one?
	fmt.Println("\n\n\n*********************************************************")
  	fmt.Printf("Test 7: Viewserver waits for primary to ack view ...\n")

  	{
	    // set up p=ck3 b=ck1, but
	    // but do not ack
	    vx, _ := ck3.Get();
	    for i := 0; i < DeadPings * 3; i++ {
			ck1.Ping(0)
      		ck3.Ping(vx.Viewnum)
      		v, _ := ck1.Get()
      		if v.Viewnum > vx.Viewnum {
        		break
      		}
      		time.Sleep(PingInterval)
    	}
		if vs.getCurrentView().Viewnum == vx.Viewnum + 1 {
			t.Fatalf("Primary never acknowledged view but view has changed.")
		}
    	// ck3 is the primary, but it never acked.
    	// let ck3 die. check that ck1 is not promoted.
    	for i := 0; i < DeadPings * 3; i++ {
      		v, _ := ck1.Ping(vx.Viewnum + 1)
      		if v.Viewnum > vx.Viewnum {
        		break
      		}
      		time.Sleep(PingInterval)
    	}
		if vs.getCurrentView().Viewnum == vx.Viewnum + 1 {
			t.Fatalf("Primary never acknowledged view but view has changed.")
		} else if vs.getCurrentView().Primary == ck1.me {
			t.Fatalf("Primary never acknowledged view but view has changed. Primary does not match.")
		}


  	}
  	fmt.Printf("  ... Passed\n")

	// set up a view with just 3 as primary,
	// to prepare for the next test.
	fmt.Println("\n\n\n*********************************************************")
	fmt.Printf("Setting up future tests\n\n")
	// Let primary acknowledge the new view now.
	vx, _ := ck3.Get()
	{
		for i := 0; i < DeadPings * 2; i++ {
			tmpViewnum := vx.Viewnum
			vx, _ = ck3.Ping(tmpViewnum)
			time.Sleep(PingInterval)
		}
	}
	// ck3 is the only primary. Kill Primary now and ensure no new/uninitialized
	// server takes over.
	fmt.Println("\n\n\n*********************************************************")
	fmt.Printf("Test 8: Uninitialized server can't become primary ...\n")
	// Let primary crash and recover.
	ck3.Ping(0)
	{
		for i := 0; i < DeadPings * 2; i++ {
			ck1.Ping(0)
			ck2.Ping(0)
			time.Sleep(PingInterval)
		}
	}

	// Check that the new view and current view do not have either ck1 or ck2
	// in the view.
	newView := vs.getNewView();
	currentView := vs.getCurrentView();
	if newView.Primary == ck1.me ||
		newView.Primary == ck2.me || newView.Backup == ck1.me ||
		newView.Backup == ck2.me {
		t.Fatalf("The new view has ck1/ck2 in the view");
	}
	if currentView.Primary == ck1.me ||
		currentView.Primary == ck2.me || currentView.Backup == ck1.me ||
		currentView.Backup == ck2.me {
		t.Fatalf("The current view has ck1/ck2 in the view");
	}
  	vs.Kill()
}
