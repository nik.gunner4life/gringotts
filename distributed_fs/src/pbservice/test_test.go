package pbservice


import "testing"
import "viewservice"
import "fmt"
//import "io"
//import "net"
import "time"
import "log"
import "runtime"
import "math/rand"
import "os"
import "sync"
import "strconv"
import "util"

/*
A simple function that gets me the viewnum in integer format. CHECKs use integers
but viewnums are uint.
*/
func getviewnum(view *viewservice.View) int {
	return (int)(view.Viewnum);
}

func populateKVStore(store map[string]*KVEntry) {
	newEntry := new(KVEntry);
	newEntry.value = "1"
	newEntry.lastRequestID = 1;
	newEntry.lastPutRequestID = 3;
	newEntry.entryLock = new(sync.Mutex);
	store["a"] = newEntry;
	newEntry = new(KVEntry);
	newEntry.value = "2"
	newEntry.lastRequestID = 1;
	newEntry.lastPutRequestID = 3;
	newEntry.entryLock = new(sync.Mutex);
	store["b"] = newEntry;
	newEntry = new(KVEntry);
	newEntry.value = "3"
	newEntry.lastRequestID = 1;
	newEntry.lastPutRequestID = 3;
	newEntry.entryLock = new(sync.Mutex);
	store["c"] = newEntry;
}

func populateLargeKVStore(store map[string]*KVEntry, num_keys int) {
	if num_keys < 0 {
		num_keys = 1000;
	}
	for i:= 0; i < num_keys; i++ {
		newEntry := new(KVEntry);
		newEntry.value = strconv.Itoa(i);
		newEntry.lastRequestID = 1;
		newEntry.lastPutRequestID = 3;
		newEntry.entryLock = new(sync.Mutex);
		store[strconv.Itoa(i)] = newEntry;
	}
}


func port(tag string, host int) string {
	s := "/var/tmp/824-"
	s += strconv.Itoa(os.Getuid()) + "/"
	os.Mkdir(s, 0777)
	s += "pb-"
	s += strconv.Itoa(os.Getpid()) + "-"
	s += tag + "-"
	s += strconv.Itoa(host)
	return s
}


func compareKeyValueStores(store1 map[string]*KVEntry, store2 map[string]*KVEntry) {
	util.CHECK_EQ(len(store1), len(store2),
		"The key value stores have different number of items.")
	for key, _ := range store1 {
		util.CHECK_EQ(store1[key].AsString(), store2[key].AsString(), "KV Entry mismatch in two stores.")
	}
}


func check(ck *Clerk, key string, value string) {
	v := ck.Get(key)
	util.CHECK_EQ(v, value, "Get %s, Expected %s")
}


func TestPBWithViewServer(t *testing.T) {
	log.Printf("---------- TestPBWithViewServer ----------")
	tag := "pbWithViewServer";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)

	s1 := StartServer(vshost, port(tag, 1))
	log.Printf("Current View: %s", s1.getCurrentView().AsString())
	time.Sleep(time.Second * 2)
	currView := s1.getCurrentView();
	log.Printf("Current View: %s", currView.AsString())
	util.CHECK_EQ(1, (int)(currView.Viewnum), "Viewnums do not match.")
	s1.kill()
	vs.Kill()
	time.Sleep(time.Second * 1)
}

func Test2PBsWithViewServer(t *testing.T) {
	log.Printf("---------- Test2PBsWithViewServer ----------")
	tag := "2pbWithViewServer";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)

	s1 := StartServer(vshost, port(tag, 1))
	time.Sleep(time.Second * 1)
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.")

	s2 := StartServer(vshost, port(tag, 2))
	time.Sleep(time.Second * 1)
	curr1View = s1.getCurrentView();
	curr2View := s2.getCurrentView();
	util.CHECK_EQ(getviewnum(curr1View), getviewnum(curr2View),
		"The two KV servers do not have the same view.")
	util.CHECK_EQ(2, getviewnum(curr1View),
		"KV servers do not have the updated viewnum.")
	s1.kill();
	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}

func TestGetWithPrimary(t *testing.T) {
	log.Printf("---------- TestGetWithPrimary ----------");
	tag := "getWithPrimary";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1))
	time.Sleep(time.Second * 1)
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.")
	populateKVStore(s1.keyValueStore);
	s1.printKeyValueStore();
	ck := MakeClerk(vshost, "clerk1")
	value := ck.Get("a");
	util.CHECK_EQ("1", value, "Values for Key: a did not match.")
	value = ck.Get("b");
	util.CHECK_EQ("2", value, "Values for Key: b did not match.")
	value = ck.Get("c");
	util.CHECK_EQ("3", value, "Values for Key: c did not match.")
	s1.printKeyValueStore();
	s1.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}


func TestGetWithPrimaryAndBackup(t *testing.T) {
	log.Printf("---------- TestGetWithPrimaryAndBackup ----------");
	tag := "getWithPnB";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1));
	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Second * 1);
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(2, (int)(curr1View.Viewnum), "Viewnums do not match.");
	populateKVStore(s1.keyValueStore);
	s1.printKeyValueStore();
	ck := MakeClerk(vshost, "clerk1");

	value := ck.Get("a");
	util.CHECK_EQ("1", value, "Values for Key[a] did not match.");
	value = ck.Get("b");
	util.CHECK_EQ("2", value, "Values for Key[b] did not match.");
	value = ck.Get("c");
	util.CHECK_EQ("3", value, "Values for Key[c] did not match.");
	s1.printKeyValueStore();
	s2.printKeyValueStore();
	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);

	// Get all values again. Since all values are present, only the request ID
	// must get updated. The values must remain the same.
	value = ck.Get("a");
	util.CHECK_EQ("1", value, "Values for Key[a] did not match.");
	value = ck.Get("b");
	util.CHECK_EQ("2", value, "Values for Key[b] did not match.");
	value = ck.Get("c");
	util.CHECK_EQ("3", value, "Values for Key[c] did not match.");
	s1.printKeyValueStore();
	s2.printKeyValueStore();
	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);


	// Corrupt a value and repeat the Get operations. Read repair must fix
	// everything.
	s2.keyValueStore["a"].lastPutRequestID = 2
	log.Printf("[Test] INFO: Changed put request ID of key[a] to 2.");
	s2.printKeyValueStore();
	value = ck.Get("a");
	util.CHECK_EQ("1", value, "Values for Key[a] did not match.");
	value = ck.Get("b");
	util.CHECK_EQ("2", value, "Values for Key[b] did not match.");
	value = ck.Get("c");
	util.CHECK_EQ("3", value, "Values for Key[c] did not match.");
	s1.printKeyValueStore();
	s2.printKeyValueStore();
	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);

	s1.kill();
	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}

func TestGetPBStoreShipment(t *testing.T) {
	log.Printf("\n---------- Test Get Primary and Backup with Store Shipment ----------\n");
	tag := "getWithPBStoreShipment";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1));
	time.Sleep(time.Millisecond * 500);
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.");
	populateLargeKVStore(s1.keyValueStore, 1000);

	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Millisecond * 500);
	curr1View = s1.getCurrentView();
	util.CHECK_EQ(2, (int)(curr1View.Viewnum), "Viewnums do not match.");
	time.Sleep(time.Second * 2);
	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);

	s1.kill();
	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}

func TestGetPBStoreShipmentWithConcurrentGets(t *testing.T) {
	log.Printf("\n---------- Test Get Primary and Backup with Store Shipment and Concurrent gets ----------\n\n");
	tag := "getWithPBStoreShipConGets";
	vshost := port(tag + "v", 1);
	totalKeys := 1000;
	// Time in seconds to issue get calls.
	totalTime := (int64)(10);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1));
	time.Sleep(time.Millisecond * 500);
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.");
	populateLargeKVStore(s1.keyValueStore, totalKeys);

	// Set the clerk lose.
	getCompleted := make(chan bool, 1);
	ck := MakeClerk(vshost, "clerk1");
	go func(totalKeys int, totalTime int64) {
		log.Println("Clerk has started");
		startTime := time.Now().Unix();
		count := 0;
		for (startTime + totalTime > time.Now().Unix()) {
			for i:= 1; i < totalKeys; i++ {
				key := strconv.Itoa(i);
				value := ck.Get(strconv.Itoa(i));
				// In our case the key and value are same.
				util.CHECK_EQ(key, value, "Values for key do not match.");
				// time.Sleep(time.Millisecond);
			}
			count++;
		}
		log.Printf("[clerk1][Test] INFO: Total get iterations: %d",
			(count * totalKeys))
		getCompleted <- true;
	}(totalKeys, totalTime);

	time.Sleep(time.Millisecond * 500)
	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Millisecond * 500);
	curr1View = s1.getCurrentView();
	util.CHECK_EQ(2, (int)(curr1View.Viewnum), "Viewnums do not match.");
	time.Sleep(time.Second * 1);

	// Wait for the gets to complete.
	<-getCompleted;

	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);

	s1.kill();
	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}


func TestConcGetPBWithPrimaryFailure(t *testing.T) {
	log.Printf("\n---------- Test Concurrent Gets with Primary and Backup and Primary failure. ----------\n\n");
	tag := "concGetPBWithPrimaryFailure";
	vshost := port(tag + "v", 1);
	totalKeys := 1000;
	// Time in seconds to issue get calls.
	totalTime := (int64)(10);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1));
	time.Sleep(time.Millisecond * 500);
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.");
	populateLargeKVStore(s1.keyValueStore, totalKeys);

	// Set the clerk lose.
	getCompleted := make(chan bool, 1);
	ck := MakeClerk(vshost, "clerk1");
	go func(totalKeys int, totalTime int64) {
		log.Println("Clerk has started");
		startTime := time.Now().Unix();
		count := 0;
		for (startTime + totalTime > time.Now().Unix()) {
			for i:= 1; i < totalKeys; i++ {
				key := strconv.Itoa(i);
				value := ck.Get(strconv.Itoa(i));
				// In our case the key and value are same.
				util.CHECK_EQ(key, value, "Values for key do not match.");
				// time.Sleep(time.Millisecond * 10);
			}
			count++;
		}
		log.Printf("[clerk1][Test] INFO: Total get iterations: %d",
			(count * totalKeys))
		getCompleted <- true;
	}(totalKeys, totalTime);

	time.Sleep(time.Millisecond * 500)
	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Millisecond * 500);
	curr1View = s1.getCurrentView();
	util.CHECK_EQ(2, (int)(curr1View.Viewnum), "Viewnums do not match.");
	time.Sleep(time.Second * 3);
	log.Printf("[Test] INFO: Killing primary server.")
	s1.kill();
	time.Sleep(time.Millisecond * 800);
	curr2View := s2.getCurrentView();
	util.CHECK_EQ(3, (int)(curr2View.Viewnum), "Viewnums do not match.");
	// Wait for the clerk to complete.
	<-getCompleted;

	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}


func TestConcGetPBWithPrimaryBackupFailure(t *testing.T) {
	log.Printf("\n---------- Test Concurrent Gets with Primary and Backup and Primary+Backup failure. ----------\n\n");
	tag := "concGetPBWithPrimaryBackupFailure";
	vshost := port(tag + "v", 1);
	totalKeys := 1000;
	// Time in seconds to issue get calls.
	totalTime := (int64)(10);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1));
	time.Sleep(time.Millisecond * 500);
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.");
	populateLargeKVStore(s1.keyValueStore, totalKeys);

	// Set the clerk lose.
	getCompleted := make(chan bool, 1);
	ck := MakeClerk(vshost, "clerk1");
	go func(totalKeys int, totalTime int64) {
		log.Println("Clerk has started");
		startTime := time.Now().Unix();
		count := 0;
		for (startTime + totalTime > time.Now().Unix()) {
			for i:= 1; i < totalKeys; i++ {
				if i % 2 == 0 {
					// We will not be accessing every key. This further
					// verifies if all keys get transferred correctly.
					continue;
				}
				key := strconv.Itoa(i);
				value := ck.Get(strconv.Itoa(i));
				// In our case the key and value are same.
				util.CHECK_EQ(key, value, "Values for key do not match.");
				// time.Sleep(time.Millisecond * 10);
			}
			count++;
		}
		log.Printf("[clerk1][Test] INFO: Total get iterations: %d",
			(count * totalKeys))
		getCompleted <- true;
	}(totalKeys, totalTime);

	time.Sleep(time.Millisecond * 500)
	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Millisecond * 500);
	curr1View = s1.getCurrentView();
	util.CHECK_EQ(2, (int)(curr1View.Viewnum), "Viewnums do not match.");
	time.Sleep(time.Second * 3);
	log.Printf("[Test] INFO: Killing primary server.")
	s1.kill();
	time.Sleep(time.Millisecond * 800);
	curr2View := s2.getCurrentView();
	util.CHECK_EQ(3, (int)(curr2View.Viewnum), "Viewnums do not match.");
	// Wait for the clerk to complete.
	log.Printf("[Test] INFO: Starting KV Server 3");
	s3 := StartServer(vshost, port(tag, 3));
	time.Sleep(time.Millisecond * 500);
	log.Printf("[Test] INFO: Killing KV Server 3");
	s3.kill();
	<-getCompleted;
	time.Sleep(time.Millisecond * 500);
	curr2View = s2.getCurrentView();
	util.CHECK_EQ(5, (int)(curr2View.Viewnum), "Viewnums do not match.");

	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}


func TestPutWithPrimary(t *testing.T) {
	log.Printf("---------- Test Put With Primary ----------");
	tag := "putWithPrimary";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1))
	time.Sleep(time.Second * 1)
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(curr1View.Viewnum), "Viewnums do not match.")
	ck := MakeClerk(vshost, "clerk1")
	ck.Put("nikhil", "nikhil");
	value := ck.Get("nikhil");
	util.CHECK_EQ("nikhil", value, "Values for Key: a did not match.")
	ck.Put("rohit", "rohit");
	value = ck.Get("rohit");
	util.CHECK_EQ("rohit", value, "Values for Key: b did not match.")
	s1.printKeyValueStore();
	s1.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}


func TestPutWithPrimaryAndBackup(t *testing.T) {
	log.Printf("---------- Test Put With Primary And Backup ----------");
	tag := "putWithPnB";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost)
	s1 := StartServer(vshost, port(tag, 1));
	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Second * 1);
	curr1View := s1.getCurrentView();
	util.CHECK_EQ(2, (int)(curr1View.Viewnum), "Viewnums do not match.");
	ck := MakeClerk(vshost, "clerk1");

	ck.Put("a", "1");
	value := ck.Get("a");
	util.CHECK_EQ("1", value, "Values for Key[a] did not match.");

	ck.Put("b", "2");
	value = ck.Get("b");
	util.CHECK_EQ("2", value, "Values for Key[b] did not match.");

	ck.Put("c", "3");
	value = ck.Get("c");
	util.CHECK_EQ("3", value, "Values for Key[c] did not match.");
	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);

	// Corrupt a value and repeat the Put operations. Write repair must fix
	// everything.
	s2.keyValueStore["a"].lastPutRequestID = 0
	log.Printf("[Test] INFO: Changed put request ID of key[a] to 0.");
	s2.printKeyValueStore();

	ck.Put("a", "1");
	value = ck.Get("a");
	util.CHECK_EQ("1", value, "Values for Key[a] did not match.");

	ck.Put("b", "2");
	value = ck.Get("b");
	util.CHECK_EQ("2", value, "Values for Key[b] did not match.");

	ck.Put("c", "3");
	value = ck.Get("c");
	util.CHECK_EQ("3", value, "Values for Key[c] did not match.");
	compareKeyValueStores(s1.keyValueStore, s2.keyValueStore);

	s1.kill();
	s2.kill();
	vs.Kill();
	time.Sleep(time.Second * 1)
}

func TestBasicFail(t *testing.T) {

	runtime.GOMAXPROCS(4)

	tag := "basic"
	vshost := port(tag + "v", 1)
	vs := viewservice.StartServer(vshost)
	time.Sleep(time.Second)
	ck := MakeClerk(vshost, "clerk1")

	fmt.Printf("Test: Single primary, no backup ...\n")

	s1 := StartServer(vshost, port(tag, 1))

	deadtime := viewservice.PingInterval * viewservice.DeadPings
	time.Sleep(deadtime * 2)
	view := s1.getCurrentView();
	util.CHECK_EQ(1, (int)(view.Viewnum), "Viewnums do not match")

	ck.Put("111", "v1")
	check(ck, "111", "v1")

	ck.Put("2", "v2")
	check(ck, "2", "v2")

	ck.Put("1", "v1a")
	check(ck, "1", "v1a")

	fmt.Printf("  ... Passed\n")

	// add a backup

	fmt.Printf("Test: Add a backup ...\n")

	s2 := StartServer(vshost, port(tag, 2))
	for i := 0; i < viewservice.DeadPings * 2; i++ {
		v := s2.getCurrentView();
		if v.Backup == s2.me {
			break
		}
		time.Sleep(viewservice.PingInterval)
	}

	ck.Put("3", "33")
	check(ck, "3", "33")

    // give the backup time to initialize
	time.Sleep(3 * viewservice.PingInterval)

	ck.Put("4", "44")
	check(ck, "4", "44")

	fmt.Printf("  ... Passed\n")

	// kill the primary

	fmt.Printf("Test: Primary failure ...\n")

	s1.kill()
	for i := 0; i < viewservice.DeadPings * 2; i++ {
		v := s2.getCurrentView();
		if v.Primary == s2.me {
			break
		}
		time.Sleep(viewservice.PingInterval)
	}
	v := s2.getCurrentView();
	util.CHECK_EQ(v.Primary, s2.me, "Primary in new view did not match.")

	check(ck, "1", "v1a")
	check(ck, "3", "33")
	check(ck, "4", "44")

	fmt.Printf("  ... Passed\n")

	// kill solo server, start new server, check that
	// it does not start serving as primary

	fmt.Printf("Test: Kill last server, new one should not be active ...\n")

	s2.kill()
	s3 := StartServer(vshost, port(tag, 3))
	time.Sleep(1 * time.Second)
	get_done := false
	go func() {
		ck.Get("1")
		get_done = true
	}()
	time.Sleep(2 * time.Second)
	if get_done {
		t.Fatalf("ck.Get() returned even though no initialized primary")
	}

	fmt.Printf("  ... Passed\n")

	s1.kill()
	s2.kill()
	s3.kill()
	time.Sleep(time.Second)
	vs.Kill()
	time.Sleep(time.Second)
}


func TestAtMostOnce(t *testing.T) {
	runtime.GOMAXPROCS(4);

	tag := "amo";
	vshost := port(tag + "v", 1);
	vs := viewservice.StartServer(vshost);
	time.Sleep(time.Second);

	fmt.Printf("Test: at-most-once Put; unreliable ...\n");

	const nservers = 2;
	var sa [nservers]*PBServer;
	for i := 0; i < nservers; i++ {
		sa[i] = StartServer(vshost, port(tag, i + 1));
		sa[i].unreliable = true;
	}


	// give p+b time to ack, initialize
	time.Sleep(viewservice.PingInterval * viewservice.DeadPings);

	ck := MakeClerk(vshost, "clerk1");
	k := "counter";
	val := "";
	for i := 0; i < 100; i++ {
		v := strconv.Itoa(i);
		pv := ck.PutHash(k, v);
		if pv != val {
			t.Fatalf("ck.Puthash() returned %v but expected %v\n", pv, val);
		}
		h := hash(val + v);
		val = strconv.Itoa(int(h));
		fmt.Printf("\n\n Expecting: %s \n\n", val);
	}

	v := ck.Get(k);
	if v != val {
		t.Fatalf("ck.Get() returned %v but expected %v\n", v, val);
	}
	fmt.Printf("  ... Passed\n");

	for i := 0; i < nservers; i++ {
		sa[i].kill();
	}
	time.Sleep(time.Second);
	vs.Kill();
	time.Sleep(time.Second);
}


// Put right after a backup dies.
func TestFailPut(t *testing.T) {
	runtime.GOMAXPROCS(4);

	tag := "failput";
	vshost := port(tag+"v", 1);
	// Start Viewservice.
	vs := viewservice.StartServer(vshost);
	time.Sleep(time.Second);

	// Start three KV Servers
	s1 := StartServer(vshost, port(tag, 1));
	time.Sleep(time.Second);
	s2 := StartServer(vshost, port(tag, 2));
	time.Sleep(time.Second);
	s3 := StartServer(vshost, port(tag, 3));

	// Sleep to let the views form.
	for i := 0; i < viewservice.DeadPings * 3; i++ {
		time.Sleep(viewservice.PingInterval);
	}
	time.Sleep(time.Second);
	view1 := s1.getCurrentView();
	view2 := s2.getCurrentView();
	util.CHECK_EQ(view1.Primary, s1.me, "Wrong Primary found in view.");
	util.CHECK_EQ(view1.Backup, s2.me, "Wrong Backup found in view.");
	util.CHECK_EQ(view1.AsString(), view2.AsString(), "View mismatch between primary and backup");
	util.CHECK_EQ((int)(view1.Viewnum), 2, "Wrong view number found.");

	ck := MakeClerk(vshost, "clerk");

	ck.Put("a", "aa");
	ck.Put("b", "bb");
	ck.Put("c", "cc");
	check(ck, "a", "aa");
	check(ck, "b", "bb");
	check(ck, "c", "cc");

	// Kill backup and then immediately Put
	log.Printf("[Test] Immediately put after killing backup %s", s2.me);
	s2.kill();
	ck.Put("a", "aaa");
	check(ck, "a", "aaa");

	// Let new view form.
	for i := 0; i < viewservice.DeadPings * 3; i++ {
		time.Sleep(viewservice.PingInterval);
	}
	// Wait for backup initialization
	time.Sleep(time.Second);
	view1 = s1.getCurrentView();
	view3 := s3.getCurrentView();

	util.CHECK_EQ(view1.Primary, s1.me, "Wrong Primary found in view.");
	util.CHECK_EQ(view1.Backup, s3.me, "Wrong Backup found in view.");
	util.CHECK_EQ(view1.AsString(), view3.AsString(), "View mismatch between primary and backup");
	util.CHECK_EQ((int)(view1.Viewnum), 3, "Wrong view number found.");

	check(ck, "a", "aaa");
	fmt.Printf("  ... Passed\n");

	// kill primary, then immediate Put
	log.Printf("[Test] Immediately put after killing primary %s", s1.me);
	s1.kill();
	ck.Put("b", "bbb");
	check(ck, "b", "bbb");

	// Let new view form.
	for i := 0; i < viewservice.DeadPings * 3; i++ {
		time.Sleep(viewservice.PingInterval);
	}
	time.Sleep(time.Second);

	view3 = s3.getCurrentView();
	util.CHECK_EQ(view3.Primary, s3.me, "Wrong Primary found in view.");
	util.CHECK_EQ(view3.Backup, "", "Wrong Backup found in view.");
	util.CHECK_EQ((int)(view3.Viewnum), 4, "Wrong view number found.");

	check(ck, "a", "aaa");
	check(ck, "b", "bbb");
	check(ck, "c", "cc");
	fmt.Printf("  ... Passed\n");

	s1.kill();
	s2.kill();
	s3.kill();
	time.Sleep(viewservice.PingInterval * 2);
	vs.Kill();
	time.Sleep(time.Second * 1);
}


// do a bunch of concurrent Put()s on the same key,
// then check that primary and backup have identical values.
// i.e. that they processed the Put()s in the same order.
func TestConcurrentSame(t *testing.T) {
	runtime.GOMAXPROCS(4);

	tag := "cs";
	vshost := port(tag+"v", 1);
	vs := viewservice.StartServer(vshost);
	time.Sleep(time.Second);

	fmt.Printf("Test: Concurrent Put()s to the same key ...\n");

	const nservers = 2;
	var sa [nservers]*PBServer;
	for i := 0; i < nservers; i++ {
		sa[i] = StartServer(vshost, port(tag, i+1));
		time.Sleep(time.Millisecond * 500);
	}

	for iters := 0; iters < viewservice.DeadPings * 2; iters++ {
		time.Sleep(viewservice.PingInterval);
	}
	// give p+b time to ack, initialize
	time.Sleep(viewservice.PingInterval * viewservice.DeadPings);

	viewS1 := sa[0].getCurrentView();
	viewS2 := sa[1].getCurrentView();
	util.CHECK_EQ(viewS1.Primary, sa[0].me, "Wrong Primary found in view.");
	util.CHECK_EQ(viewS1.Backup, sa[1].me, "Wrong Backup found in view.");
	util.CHECK_EQ(viewS1.AsString(), viewS2.AsString(), "View mismatch between primary and backup");
	util.CHECK_EQ((int)(viewS1.Viewnum), 2, "Wrong view number found.");

	done := false;
	const nclients = 3;
	const nkeys = 2;
	for xi := 0; xi < nclients; xi++ {
		go func(i int) {
			clerkName := fmt.Sprintf("clerk-%d", i);
			ck := MakeClerk(vshost, clerkName);
			rr := rand.New(rand.NewSource(int64(os.Getpid()+i)));
			for done == false {
				k := strconv.Itoa(rr.Int() % nkeys);
				v := strconv.Itoa(rr.Int());
				ck.Put(k, v);
			}
		}(xi);
	}

	time.Sleep(3 * time.Second);
	done = true;
	time.Sleep(time.Second);

	// The key value stores must be exactly the same.
	compareKeyValueStores(sa[0].keyValueStore, sa[1].keyValueStore);

	// Read from primary
	ck := MakeClerk(vshost, "clerk-Master");
	var vals [nkeys]string;
	for i := 0; i < nkeys; i++ {
		vals[i] = ck.Get(strconv.Itoa(i));
		errorMsg := fmt.Sprintf("Get for index %d failed", i);
		util.CHECK_NE(vals[i], "", errorMsg);
	}

	// The key value stores must be exactly the same.
	compareKeyValueStores(sa[0].keyValueStore, sa[1].keyValueStore);

	// Kill the primary
	log.Printf("[Test] Killing primary %s", sa[0].me);
	sa[0].kill();

	for iters := 0; iters < viewservice.DeadPings * 2; iters++ {
		time.Sleep(viewservice.PingInterval);
	}
	time.Sleep(2 * time.Second);

	viewS2 = sa[1].getCurrentView();
	util.CHECK_EQ(viewS2.Primary, sa[1].me, "Wrong Primary found in view.");
	util.CHECK_EQ(viewS2.Backup, "", "Wrong Backup found in view.");
	util.CHECK_EQ((int)(viewS2.Viewnum), 3, "Wrong view number found.");

	// Read from the new primary which was previously the backup.
	for i := 0; i < nkeys; i++ {
		z := ck.Get(strconv.Itoa(i));
		errorMsg := fmt.Sprintf("Get's from old primary and new primary do not match for index %d", i);
		util.CHECK_EQ(z, vals[i], errorMsg);
	}

	fmt.Printf("  ... Passed\n");

	for i := 0; i < nservers; i++ {
		sa[i].kill();
	}

	time.Sleep(time.Second);
	vs.Kill();
	time.Sleep(time.Second);
}


func TestConcurrentSameUnreliable(t *testing.T) {
	runtime.GOMAXPROCS(4);

	tag := "csu";
	vshost := port(tag+"v", 1);
	vs := viewservice.StartServer(vshost);
	time.Sleep(time.Second);

	fmt.Printf("Test: Concurrent Put()s to the same key; unreliable ...\n");

	const nservers = 2;
	var sa [nservers]*PBServer;
	for i := 0; i < nservers; i++ {
		sa[i] = StartServer(vshost, port(tag, i+1));
		sa[i].unreliable = true
		time.Sleep(time.Millisecond * 500);
	}

	for iters := 0; iters < viewservice.DeadPings * 2; iters++ {
		time.Sleep(viewservice.PingInterval);
	}
	// give p+b time to ack, initialize
	time.Sleep(viewservice.PingInterval * viewservice.DeadPings);

	viewS1 := sa[0].getCurrentView();
	viewS2 := sa[1].getCurrentView();
	util.CHECK_EQ(viewS1.Primary, sa[0].me, "Wrong Primary found in view.");
	util.CHECK_EQ(viewS1.Backup, sa[1].me, "Wrong Backup found in view.");
	util.CHECK_EQ(viewS1.AsString(), viewS2.AsString(), "View mismatch between primary and backup");
	util.CHECK_EQ((int)(viewS1.Viewnum), 2, "Wrong view number found.");

	done := false;
	const nclients = 7;
	const nkeys = 4;
	for xi := 0; xi < nclients; xi++ {
		go func(i int) {
			clerkName := fmt.Sprintf("clerk-%d", i);
			ck := MakeClerk(vshost, clerkName);
			rr := rand.New(rand.NewSource(int64(os.Getpid()+i)));
			for done == false {
				k := strconv.Itoa(rr.Int() % nkeys);
				v := strconv.Itoa(rr.Int());
				ck.Put(k, v);
			}
		}(xi);
	}

	time.Sleep(10 * time.Second);
	done = true;
	time.Sleep(time.Second);

	// The key value stores must be exactly the same.
	compareKeyValueStores(sa[0].keyValueStore, sa[1].keyValueStore);

	// Read from primary
	ck := MakeClerk(vshost, "clerk-Master");
	var vals [nkeys]string;
	for i := 0; i < nkeys; i++ {
		vals[i] = ck.Get(strconv.Itoa(i));
		errorMsg := fmt.Sprintf("Get for index %d failed", i);
		util.CHECK_NE(vals[i], "", errorMsg);
	}

	// The key value stores must be exactly the same.
	compareKeyValueStores(sa[0].keyValueStore, sa[1].keyValueStore);

	// Kill the primary
	log.Printf("[Test] Killing primary %s", sa[0].me);
	sa[0].kill();

	for iters := 0; iters < viewservice.DeadPings * 2; iters++ {
		time.Sleep(viewservice.PingInterval);
	}
	time.Sleep(time.Second);

	viewS2 = sa[1].getCurrentView();
	util.CHECK_EQ(viewS2.Primary, sa[1].me, "Wrong Primary found in view.");
	util.CHECK_EQ(viewS2.Backup, "", "Wrong Backup found in view.");
	util.CHECK_EQ((int)(viewS2.Viewnum), 3, "Wrong view number found.");

	// Read from the new primary which was previously the backup.
	for i := 0; i < nkeys; i++ {
		z := ck.Get(strconv.Itoa(i));
		errorMsg := fmt.Sprintf("Get's from old primary and new primary do not match for index %d", i);
		util.CHECK_EQ(z, vals[i], errorMsg);
	}

	fmt.Printf("  ... Passed\n");

	for i := 0; i < nservers; i++ {
		sa[i].kill();
	}

	time.Sleep(time.Second);
	vs.Kill();
	time.Sleep(time.Second);
}


// constant put/get while crashing and restarting servers
func TestRepeatedCrash(t *testing.T) {
	runtime.GOMAXPROCS(4)

	tag := "rc";
	vshost := port(tag+"v", 1);
	vs := viewservice.StartServer(vshost);
	time.Sleep(time.Second);

	fmt.Printf("Test: Repeated failures/restarts ...\n");

	const nservers = 3;
	var sa [nservers]*PBServer;
	for i := 0; i < nservers; i++ {
		sa[i] = StartServer(vshost, port(tag, i+1));
	}

	for i := 0; i < viewservice.DeadPings; i++ {
		time.Sleep(viewservice.PingInterval);
	}

	// wait a bit for primary to initialize backup
	time.Sleep(viewservice.DeadPings * viewservice.PingInterval);

	done := false;

	log.Printf("[Test] INFO: Starting KV Assassin.");
	go func() {
		// kill and restart servers
		rr := rand.New(rand.NewSource(int64(os.Getpid())));
		for done == false {
			i := rr.Int() % nservers
			log.Printf("[Test] INFO: Killing server %s", sa[i].me);
			sa[i].kill();

			// wait long enough for new view to form, backup to be initialized
			time.Sleep(2 * viewservice.PingInterval * viewservice.DeadPings);

			log.Printf("[Test] INFO: Starting server %s", sa[i].me);
			sa[i] = StartServer(vshost, port(tag, i+1));

			// wait long enough for new view to form, backup to be initialized
			time.Sleep(2 * viewservice.PingInterval * viewservice.DeadPings);
		}
	} ()

	log.Printf("[Test] INFO: Starting clerks.");
	const nth = 2;
	var cha [nth]chan bool;
	for xi := 0; xi < nth; xi++ {
		cha[xi] = make(chan bool);
		go func(i int) {
			ok := false;
			defer func() { cha[i] <- ok } ();
			clerkName := fmt.Sprintf("clerk-%d", i);
			ck := MakeClerk(vshost, clerkName);
			data := map[string]string{};
			rr := rand.New(rand.NewSource(int64(os.Getpid()+i)));
			for done == false {
				k := strconv.Itoa((i * 1000000) + (rr.Int() % 10));
				wanted, ok := data[k];
				if ok {
					v := ck.Get(k);
					errorMsg := fmt.Sprintf("Values for key[%s] did not match. Expected %s, got %s",
						k, wanted, v);
					util.CHECK_EQ(wanted, v, errorMsg);
				}
				nv := strconv.Itoa(rr.Int());
				ck.Put(k, nv);
				data[k] = nv;
				// if no sleep here, then server tick() threads do not get
				// enough time to Ping the viewserver.
				time.Sleep(10 * time.Millisecond);
			}
			ok = true;
		}(xi);
	}

	time.Sleep(20 * time.Second);
	done = true;

	fmt.Printf("  ... Put/Gets done ... \n");

	for i := 0; i < nth; i++ {
		ok := <- cha[i];
		util.CHECK(ok, "Child failed");
	}

	ck := MakeClerk(vshost, "clerk-Master");
	ck.Put("aaa", "bbb");
	util.CHECK_EQ(ck.Get("aaa"), "bbb", "Final Put/Get failed");
	fmt.Printf("  ... Passed\n");

	for i := 0; i < nservers; i++ {
		sa[i].kill();
	}

	time.Sleep(time.Second);
	vs.Kill();
	time.Sleep(time.Second);
}


// constant put/get while crashing and restarting servers
func TestRepeatedCrashUnreliable(t *testing.T) {
	runtime.GOMAXPROCS(4)

	tag := "rcu";
	vshost := port(tag+"v", 1);
	vs := viewservice.StartServer(vshost);
	time.Sleep(time.Second);

	fmt.Printf("Test: Repeated failures/restarts ...\n");

	const nservers = 3;
	var sa [nservers]*PBServer;
	for i := 0; i < nservers; i++ {
		sa[i] = StartServer(vshost, port(tag, i+1));
		sa[i].unreliable = true
	}

	for i := 0; i < viewservice.DeadPings; i++ {
		time.Sleep(viewservice.PingInterval);
	}

	// wait a bit for primary to initialize backup
	time.Sleep(viewservice.DeadPings * viewservice.PingInterval);

	done := false;

	log.Printf("[Test] INFO: Starting KV Assassin.");
	go func() {
		// kill and restart servers
		rr := rand.New(rand.NewSource(int64(os.Getpid())));
		for done == false {
			i := rr.Int() % nservers
			log.Printf("[Test] INFO: Killing server %s", sa[i].me);
			sa[i].kill();

			// wait long enough for new view to form, backup to be initialized
			time.Sleep(2 * viewservice.PingInterval * viewservice.DeadPings);

			log.Printf("[Test] INFO: Starting server %s", sa[i].me);
			sa[i] = StartServer(vshost, port(tag, i+1));

			// wait long enough for new view to form, backup to be initialized
			time.Sleep(2 * viewservice.PingInterval * viewservice.DeadPings);
		}
	} ()

	log.Printf("[Test] INFO: Starting clerks.");
	const nth = 2;
	var cha [nth]chan bool;
	for xi := 0; xi < nth; xi++ {
		cha[xi] = make(chan bool);
		go func(i int) {
			ok := false;
			defer func() { cha[i] <- ok } ();
			clerkName := fmt.Sprintf("clerk-%d", i);
			ck := MakeClerk(vshost, clerkName);
			data := map[string]string{};
			rr := rand.New(rand.NewSource(int64(os.Getpid()+i)));
			for done == false {
				k := strconv.Itoa((i * 1000000) + (rr.Int() % 10));
				wanted, ok := data[k];
				if ok {
					v := ck.Get(k);
					errorMsg := fmt.Sprintf("Values for key[%s] did not match. Expected %s, got %s",
						k, wanted, v);
					util.CHECK_EQ(wanted, v, errorMsg);
				}
				nv := strconv.Itoa(rr.Int());
				ck.Put(k, nv);
				data[k] = nv;
				// if no sleep here, then server tick() threads do not get
				// enough time to Ping the viewserver.
				time.Sleep(10 * time.Millisecond);
			}
			ok = true;
		}(xi);
	}

	time.Sleep(20 * time.Second);
	done = true;

	fmt.Printf("  ... Put/Gets done ... \n");

	for i := 0; i < nth; i++ {
		ok := <- cha[i];
		util.CHECK(ok, "Child failed");
	}

	ck := MakeClerk(vshost, "clerk-Master");
	ck.Put("aaa", "bbb");
	util.CHECK_EQ(ck.Get("aaa"), "bbb", "Final Put/Get failed");
	fmt.Printf("  ... Passed\n");

	for i := 0; i < nservers; i++ {
		sa[i].kill();
	}

	time.Sleep(time.Second);
	vs.Kill();
	time.Sleep(time.Second);
}

