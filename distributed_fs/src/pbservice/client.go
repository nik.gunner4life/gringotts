package pbservice

import "crypto/rand"
import "fmt"
import "math/big"
import "net/rpc"
import (
	"viewservice"
	"time"
	"util"
	"log"
)


func nrand() uint64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return (uint64)(x);
}


type Clerk struct {
	// client's name (host:port)
	me string;
	// viewservice's host:port
	viewserver string;
	// The current view that I have.
	currentView *viewservice.View;
}


func MakeClerk(vshost string, me string) *Clerk {
	ck := new(Clerk);
	ck.me = me;
	ck.viewserver = vshost;
	ck.currentView = &viewservice.View{0, "", ""};
  	return ck
}


//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the reply's contents are only valid if call() returned true.
//
// you should assume that call() will time out and return an
// error after a while if it doesn't get a reply from the server.
//
// please use call() to send all RPCs, in client.go and server.go.
// please don't change this function.
//
func call(srv string, rpcname string,
          args interface{}, reply interface{}) bool {
	c, errx := rpc.Dial("unix", srv)
  	if errx != nil {
	    return false
  	}
  	defer c.Close()

  	err := c.Call(rpcname, args, reply)
  	if err == nil {
    	return true
  	}

  	fmt.Println(err)
  	return false
}

/*
This method makes an RPC to the view server and gets the latest view.
It is also possible for the latest view to be exactly the same as the view
that we hold at present indicating that the view has not changes since the last
time we contacted the viewserver.
*/
func (ck *Clerk) maybeUpdateView() {
	args := &viewservice.PingArgs{ck.me, ck.currentView.Viewnum};
	var reply viewservice.PingReply
	log.Printf("[%s][Ping] INFO: Querying viewservice at %d with viewnum: %d",
		ck.me, (time.Now().UnixNano() / 1000000),
		ck.currentView.Viewnum)
	ok := call(ck.viewserver, "ViewServer.GetView", args, &reply)
	if ok == false {
		log.Printf("[%s][Ping] ERROR: Unable to get updated view.", ck.me);
		return
	}
	view := reply.View;
	ck.currentView = &view;
}


func (ck *Clerk) getCurrentView() *viewservice.View {
	return ck.currentView;
}


/*
Fetch a key's value from the current primary. Keep retrying till we get a
reply from the primary server. Only if the error message is KErrNoKey, then
return with that error. For all other errors, retry.
*/
func (ck *Clerk) Get(key string) string {
	args := &GetArgs{};
	args.RequestID = nrand();
	args.Key = key;

	var reply GetReply;
	// Keep trying till we have a successful return.
	view := ck.getCurrentView();
  	for {
		view = ck.getCurrentView();
		if view.Primary == "" {
			log.Printf("[%s][Get] ERROR: Cannot query key value store. No primary server found. Pinging viewserver for a new view, if any.",
				ck.me)
			ck.maybeUpdateView();
			// To avoid burning CPU cycles, try once every microsecond.
			time.Sleep(100 * time.Millisecond);
			continue;
		}
		log.Printf("[%s][Get] INFO: Initiating get request %d to KV server %s for key[%s].",
			ck.me, args.RequestID, view.Primary, args.Key);
		rpc_ok := util.Call(view.Primary, "PBServer.Get", args, &reply);
		if !rpc_ok {
			// May be the KV server is down. Get the updated view and retry.
			log.Printf("[%s][Get] ERROR: RPC failure to KV server %s. Pinging viewservice for the new view, if any.", ck.me, view.Primary);
			ck.maybeUpdateView();
			// To avoid burning CPU cycles, try once every 10 milliseconds.
			time.Sleep(time.Millisecond * 10);
			continue;
		}
		if reply.Err == KErrNoKey {
			log.Printf("[%s][Get] ERROR: Received %s from KV server %s for request %d",
				ck.me, reply.Err, view.Primary, args.RequestID);
			return reply.Value;
		} else if reply.Err == KSuccess {
			log.Printf("[%s][Get] INFO: Request %d to KV Server %s successful. (Key -> Value) %s -> %s",
				ck.me, args.RequestID, view.Primary, args.Key, reply.Value);
			return reply.Value;
		} else {
			log.Printf("[%s][Get] ERROR: Received %s from KV server %s for request %d. Pinging viewservice for the new view, if any.",
				ck.me, reply.Err, view.Primary, args.RequestID);
			ck.maybeUpdateView();
			// To avoid burning CPU cycles, try once every 10 milliseconds.
			time.Sleep(time.Millisecond * 10);
			continue;
		}
	}
}

//
// tell the primary to update key's value.
// must keep trying until it succeeds.
//
func (ck *Clerk) PutExt(key string, value string, dohash bool) string {
	args := &PutArgs{};
	args.RequestID = nrand();
	args.Key = key;
	args.Value = value;
	args.DoHash = dohash;

	var reply PutReply;
	// Keep trying till we have a successful return.
	view := ck.getCurrentView();
	for {
		view = ck.getCurrentView();
		if view.Primary == "" {
			log.Printf("[%s][Put] ERROR: Cannot query key value store. No primary server found. Pinging viewserver for a new view, if any.",
				ck.me)
			ck.maybeUpdateView();
			// To avoid bombarding the view service, try once 100 milliseconds.
			time.Sleep(100 * time.Millisecond);
			continue;
		}

		log.Printf("[%s][Put] INFO: Initiating put request %d to KV server %s for (%s -> %s)",
			ck.me, args.RequestID, view.Primary, args.Key, args.Value);
		rpc_ok := util.Call(view.Primary, "PBServer.Put", args, &reply);
		if !rpc_ok {
			// May be the KV server is down. Get the updated view and retry.
			log.Printf("[%s][Put] ERROR: RPC failure to KV server %s. Pinging viewservice for the new view, if any.", ck.me, view.Primary);
			ck.maybeUpdateView();
			// To avoid burning CPU cycles, try once every 10 milliseconds.
			time.Sleep(time.Millisecond * 10);
			continue;
		}

		if reply.Err == KSuccess {
			log.Printf("[%s][Put] INFO: Request %d to KV Server %s successful. (Key -> Previous value) %s -> %s",
				ck.me, args.RequestID, view.Primary, args.Key,
				reply.PreviousValue);
			return reply.PreviousValue;
		} else {
			log.Printf("[%s][Put] ERROR: Received %s from KV server %s for request %d. Pinging viewservice for the new view, if any.",
				ck.me, reply.Err, view.Primary, args.RequestID);
			ck.maybeUpdateView();
			// To avoid burning CPU cycles, try once every 10 milliseconds.
			time.Sleep(time.Millisecond * 10);
			continue;
		}
	}
}

func (ck *Clerk) Put(key string, value string) {
	ck.PutExt(key, value, false);
}

func (ck *Clerk) PutHash(key string, value string) string {
	v := ck.PutExt(key, value, true);
  	return v;
}
