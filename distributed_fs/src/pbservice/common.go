package pbservice

import (
	"hash/fnv"
	"viewservice"
)


type Err string


const (
	// Get/Put went through successfully.
  	KSuccess = "OK";
	// Get could not find the key in the store.
  	KErrNoKey = "KErrNoKey";
	// Error on the remote side while getting/putting a key
	KRemoteError = "KRemoteError";
	// Error to indicate that server is not going to respond.
  	KErrWrongServer = "KErrWrongServer";
	// Error pinging view server
	KViewServerPingError = "KViewServerPingError"
)


type PutArgs struct {
  	Key string;
  	Value string;
  	DoHash bool;
	// The request ID generated by the client.
	RequestID uint64;
}


type PutBackupArgs struct {
	Key string;
	NewValue string;
	PrevValue string;
	DoHash bool;
	// The request ID generated by the client.
	RequestID uint64;
	// Put Request ID. Maintains the number of times the key has been updated.
	PutRequestID uint64;
	// The current view of the primary.
	PrimaryView viewservice.View;
}


type PutReply struct {
	// Error returned to clerk in case of success/failure.
  	Err Err;
	// Previous value. Used for PutHash.
  	PreviousValue string;
}


type PutBackupReply struct {
	// Previous value. Used for PutHash.
	PreviousValue string;
	// Error returned by backup.
	Err Err;
}


type GetBackupArgs struct {
	Key string;
	// The request ID generated by the client.
	RequestID uint64;
	// Send the value and putrequest ID as well. If for some reason, the backup
	// has a stale value, this can serve as a fixer.
	Value string;
	PrevValue string;
	PutRequestID uint64;
	// The current view of the server.
	MyView viewservice.View;
}


type GetBackupReply struct {
	// Get the value from backup as well.
	Value string;
	// Error returned by backup.
	Err Err;
}


type GetArgs struct {
  	Key string;
	// The request ID generated by the client.
  	RequestID uint64;
}


type GetReply struct {
	Err Err;
	Value string;
}


type StoreShipmentArgs struct {
	Err Err;
}


type StoreShipmentReply struct {
	Err Err;
}


func hash(s string) uint32 {
  	h := fnv.New32a();
  	h.Write([]byte(s));
  	return h.Sum32();
}


