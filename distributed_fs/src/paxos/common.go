package paxos


const (
	KPrepareOK = "KPrepareAccept"
	KPrepareReject = "KPrepareReject"
	KAcceptOK = "KAcceptOK"
	KAcceptReject = "KAcceptReject"
)


type PrepareArg struct {
	InstanceNum int64;
	// Ballot number of the Paxos transaction.
	Ballot uint64;
}


type PrepareReply struct {
	// Status of the reply. Either KPrepareOK or KPrepareReject.
	Status string;
	// An error message, if any. Empty string otherwise.
	Message string;
	// Largest accept that has been so far by the peer for the given instance.
	LargestAcceptSeenSoFar uint64;
	// Value corresponding to the largest accept seen so far by the peer.
	LargestAcceptValue interface {};
	// Indicates whether an instance has been decided or not.
	Decided bool;
	// The decided value.
	DecidedValue interface {};
	// The instance upto which the peer is now ready to forget. This helps all
	// Paxos peers free up memory for instances that are no longer required.
	// Piggybacking this as part of the message.
	Done int64;
}


type AcceptArg struct {
	InstanceNum int64;
	// Ballot number of the Paxos transaction.
	Ballot uint64;
	// Value that has been chosen by the leader for the given instance.
	Value interface {};
}


type AcceptReply struct {
	// Status of the accept. Either KAcceptOK or KAcceptReject.
	Status string;
	// An error message, if any.
	Message string;
	// The largest instance that has been so far. This is useful for implementing
	// forgetting.
	Max int64;
	// The instance upto which the peer is now ready to forget. This helps all
	// Paxos peers free up memory for instances that are no longer required.
	// Piggybacking this as part of the message.
	Done int64;
}


type DecidedArg struct {
	InstanceNum int64;
	// The value that has been decided for the instance.
	DecidedValue interface {};
}


/*
Empty DecidedReply message.
*/
type DecidedReply struct {}
