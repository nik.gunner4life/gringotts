package paxos


/*******************************************************************************
Paxos library, to be included in an application. Multiple applications will run,
each including a Paxos peer. Manages a sequence of agreed-on values. The set of
peers is fixed. Copes with network failures (partition, msg loss, &c). Does not
store anything persistently, so cannot handle crash+restart.

The application interface:
px = paxos.Make(peers []string, me string)
Start(seq int, v interface{}) -- start agreement on new instance
Status(seq int) (decided bool, v interface{}) -- get info about an instance
Done(seq int) -- ok to forget all instances <= seq
Max() int -- highest instance seq known, or -1
Min() int -- instances before this seq have been forgotten

Paxos Pseudo Code:
proposer(v):
  while not decided:
    choose n, unique and higher than any n seen so far
    send prepare(n) to all servers including self
    if prepare_ok(n_a, v_a) from majority:
      v' = v_a with highest n_a; choose own v otherwise
      send accept(n, v') to all
      if accept_ok(n) from majority:
        send decided(v') to all

acceptor's state:
  n_p (highest prepare seen)
  n_a, v_a (highest accept seen)

acceptor's prepare(n) handler:
  if n > n_p
    n_p = n
    reply prepare_ok(n_a, v_a)
  else
    reply prepare_reject

acceptor's accept(n, v) handler:
  if n >= n_p
    n_p = n
    n_a = n
    v_a = v
    reply accept_ok(n)
  else
    reply accept_reject
*******************************************************************************/


import "sync/atomic"
import "net"
import "net/rpc"
import "log"
import "os"
import "syscall"
import "sync"
import "fmt"
import "math/rand"
import "github.com/op/go-logging"
import (
	"util"
	// "strconv"
	"time"
)


var logger = util.GetLogger("Paxos", logging.DEBUG)

/*
Call() sends an RPC to the rpcname handler on server srv with arguments args,
waits for the reply, and leaves the reply in reply. the reply argument should
be a pointer to a reply structure.

The return value is true if the server responded, and false if call() was not
able to contact the server. In particular, the contents in reply are only valid
if call() returned true.

You should assume that call() will time out and return an error after a while
if it does not get a reply from the server. Please use call() to send all RPCs,
in client.go and server.go.
*/
func call(srv string, name string, args interface{}, reply interface{}) bool {
	c, err := rpc.Dial("unix", srv);
	if err != nil {
		err1 := err.(*net.OpError);
		if err1.Err != syscall.ENOENT && err1.Err != syscall.ECONNREFUSED {
			fmt.Printf("Paxos Dial() failed: %v\n", err1);
		}
		return false;
	}
	defer c.Close();

	err = c.Call(name, args, reply);
	if err == nil {
		return true;
	}

	logger.Error(err);
	return false;
}


/*
Data stored for every live Paxos instance.
*/
type PaxosInstance struct {
	instanceNum int64;
	highestPrepare uint64;
	highestAccept uint64;
	highestAcceptValue interface {};
	decided bool;
	decidedValue interface {};
	mu sync.Mutex;
}


/*
Prints out contents of a Paxos Instance in a thread safe manner.
*/
func (pxi *PaxosInstance) Print() {
	pxi.mu.Lock();
	defer pxi.mu.Unlock();
	// logger.Infof("Acquired lock on instance %v", pxi.instanceNum);
	message := fmt.Sprintf("\nInstance: %d\nHighest Prepare: %d\nHighest Accept: %d\nHighest Accept Value: %v\nDecided: %v\nDecided Value: %v",
		pxi.instanceNum, pxi.highestPrepare, pxi.highestAccept,
		pxi.highestAcceptValue, pxi.decided, pxi.decidedValue);
	logger.Info(message);
}


/*
Returns true if the instance has been decided. false otherwise.
*/
func(pxi *PaxosInstance) isDecided() bool {
	pxi.mu.Lock();
	defer pxi.mu.Unlock();
	return pxi.decided;
}


/*
The main Paxos struct.
*/
type Paxos struct {
  mu sync.Mutex;
  l net.Listener;
  dead bool;
  unreliable bool;
  rpcCount int;
	// The array of Paxos peers.
  peers []string;
	// Peer's index into peers[]
  me int;
	// Ballot number that is incremented atomically.
	ballotNum *uint64;
	// Monotonically increasing transaction ID.
	transactionID *uint64;
	// The map of all active Paxos instances.
	instancesMap map[int64] *PaxosInstance;
	// This lock needs to be taken before creating/deleting an entry in the
	// instances map.
	instanceCDMu sync.Mutex;
	// Timeout for every phase.
	phaseTimeout time.Duration;
	// The max instance number seen by this peer.
	maxInstance *int64;
	// The minimum Done instanceNum of all Paxos peers.
	minInstance *int64;
	// The map of all peers to it's respective Done instance.
	// TODO: We may need to take a lock here later if dynamic node add and remove support is required.
	doneMap map[int] *int64;
}


func (px *Paxos) initMembers() {
	logger.Infof("[%d] Creating Paxos peer: %d", px.me, px.me);
	px.ballotNum = new(uint64);
	*px.ballotNum = (uint64)(px.me);
	px.transactionID = new(uint64);
	*px.transactionID = 0;
	// Initialize the instance map.
	px.instancesMap = make(map[int64]*PaxosInstance);
	// Initialize the done map. Set the done value for all peers as -1.
	px.doneMap = make(map[int] *int64);
	for ii := 0; ii < len(px.peers); ii++ {
		value := new(int64);
		*value = -1;
		px.doneMap[ii] = value;
	}
	// Every phase must complete within the timeout.
	px.phaseTimeout = time.Second * 2;

	px.minInstance = new(int64);
	*px.minInstance = -1;
	px.maxInstance = new(int64);
	*px.maxInstance = -1;

	// Start the background tasks goroutine.
	go px.backgroundTasks();
}


/*
Returns the next ballot number for Paxos. According to Paxos, the ballot number
must always increase and no two peers can ever generate the same ballot number.

We guarantee the uniqueness and increasing order of Paxos ballotNums by
doing the following:
The index of every Paxos peer is a number between 0 and n - 1 where n is
total number of Paxos peers. For every peer the first ballot number for that
peer is set as it's peer index and is incremented by len(peers) to get the next
ballot number. Essentially, this is way of implementing the following:
while true {
	if X % n == i {
  	return X
  }
	else {
		X += 1;
	}
}
This ensures that every peer generates unique numbers with respect to the
others.

Every peer can also be performing multiple Paxos transactions simultaneously on
multiple instances. We also need to ensure that every transaction has it's own
ballot num and that no two transactions use the same ballot numbers. To
guarantee this, we use atomic increments to ensure that every ballot is unique
per peer and per transaction.
*/
func(px *Paxos) getNextBallot() uint64 {
	numPeers := len(px.peers);
	ballot := atomic.AddUint64(px.ballotNum, (uint64)(numPeers));
	return ballot;
}


/*
Generates a monotonically increasing transaction ID.
*/
func(px *Paxos) getNewTransactionID() uint64 {
	return atomic.AddUint64(px.transactionID, (uint64)(1));
}


/*
Checks if the supplied Done value is greater than the current Done value.
If so, it compares and swaps the done value for the given peer.
Returns true if the Done value was updated successfully. false otherwise.
*/
func (px *Paxos) mayBeUpdatePeerDoneValue(peer int, doneValue int64) bool {
	for {
		oldValue := atomic.LoadInt64(px.doneMap[peer]);
		if doneValue > oldValue {
			swapped := atomic.CompareAndSwapInt64(px.doneMap[peer], oldValue,
				doneValue);
			if swapped {
				return true;
			} else {
				continue;
			}
		} else {
			return false;
		}
	}
}


/*
Gets the done value that has been set for this peer.
*/
func (px *Paxos) getMyDoneValue() int64 {
	return atomic.LoadInt64(px.doneMap[px.me]);
}


/*
Returns the max instance seen by the peer so far.
*/
func(px *Paxos) getMaxInstance() int64 {
	return atomic.LoadInt64(px.maxInstance);
}


/*
Checks if the supplied instanceNum is greater than the max instance seen so far.
If so, it compares and swaps the max instance value. Returns true if the max
instance was updated successfully, false otherwise.
*/
func (px *Paxos) maybeUpdateMaxInstance(instanceNum int64) bool {
	for {
		oldValue := px.getMaxInstance();
		if instanceNum > oldValue {
			swapped := atomic.CompareAndSwapInt64(px.maxInstance, oldValue,
				instanceNum);
			if swapped {
				return true;
			} else {
				continue;
			}
		} else {
			return false;
		}
	}
}


/*
Returns the min instance seen across all paxos peers.
*/
func(px *Paxos) getMinInstance() int64 {
	return atomic.LoadInt64(px.minInstance);
}


/*
The goroutine that carries out all background tasks. It runs once a millisecond
and keeps running indefinitely. The following are performed:
	1. Find the minimum done value from all peers and update the minimum instance.
	2. Removes instances from the instances map that are no longer needed(forget)
*/
func(px *Paxos) backgroundTasks() {
	logger.Debug("[%d] Paxos background tasks goroutine has started.", px.me);
	for {
		px.forget();
		time.Sleep(time.Microsecond * 1000);
	}
}


/*
Checks if the instanceNum < minInstanceNum. If so, it returns nil to indicate
that this instance does not and can never exist hereafter. Proceeding beyond
this will break the consistency guarantees of Paxos as instances before
minInstance could have been forgotten by other peers resulting in new values
being chosen for an already decided instance.

If instanceNum >= minInstance, it returns the PaxosInstance object corresponding
to the instanceNum. If a paxos instance object does not exist for the given
instanceNum, then a new instance object will be created and returned.
*/
func(px *Paxos) maybeCreateAndGetInstance(instanceNum int64) *PaxosInstance {
	px.instanceCDMu.Lock();
	defer px.instanceCDMu.Unlock();

	minInstanceNum := px.getMinInstance();
	if instanceNum < minInstanceNum {
		logger.Warningf("Instance %d is lesser than min instance %d. No instance object found",
			instanceNum, minInstanceNum);
		return nil;
	}

	instance, ok := px.instancesMap[instanceNum];
	if !ok {
		// We need to create a new instance here.
		logger.Infof("[%d] Creating new instance: %d", px.me, instanceNum);
		newInstance := new(PaxosInstance);
		newInstance.instanceNum = instanceNum;
		newInstance.highestPrepare = (uint64)(0);
		newInstance.highestAccept = (uint64)(0);
		newInstance.decided = false;
		px.instancesMap[instanceNum] = newInstance;
		return newInstance;
	}
	return instance;
}


/*
Determines and updates the minInstance. It then deletes instances lesser than
the min instance.

This entire method is guarded by the instanceCDMu lock. So when instances are
getting deleted, no other instances can be obtained. All paxos transactions
will be blocked here. This ensures that we don't decide new values for instances
that may have been forgotten by other peers.
*/
func(px *Paxos) forget() {
	px.instanceCDMu.Lock();
	defer px.instanceCDMu.Unlock();

	var minDone int64;
	minPeer := -1;
	count := 0;
	for peer, doneInstancePtr := range px.doneMap {
		count++;
		if count == 1 {
			minDone = atomic.LoadInt64(doneInstancePtr);
			minPeer = peer;
		} else {
			value := atomic.LoadInt64(doneInstancePtr);
			if value < minDone {
				minDone = value;
				minPeer = peer;
			}
		}
	}

	if minDone < 0 {
		// At least one peer's done value has not been received. Cannot
		// forget any values.
		return;
	}

	newMinInstanceNum := minDone + 1;
	// Set the new newMinInstanceNum and forget all instances before it.
	currMinInstance := px.getMinInstance();
	if newMinInstanceNum > currMinInstance {
		atomic.StoreInt64(px.minInstance, newMinInstanceNum);
		for instanceNum, _ := range px.instancesMap {
			if instanceNum < newMinInstanceNum {
				delete(px.instancesMap, instanceNum);
			}
		}
		logger.Debugf("[%d] Chose min value from peer %d, Forgotten all instances before %d",
			px.me, minPeer, newMinInstanceNum);
	}
}


/*
Run Paxos transaction for the given instance number with the given value.
*/
func (px *Paxos) run(instanceNum int64, value interface {}) {
	transactionID := px.getNewTransactionID();
	transactionAttempts := 0;
	for {
		transactionAttempts += 1;
		logger.Debugf("[%d] Starting transaction. Transaction ID: %d, Instance: %d, Trasaction Attempts: %d",
			px.me, transactionID, instanceNum, transactionAttempts);

		// TODO: I think this reduces performance. Will need to get numbers.
		instance := px.maybeCreateAndGetInstance(instanceNum);
		if instance == nil {
			logger.Warningf("[%d] Aborting transaction. No instance found for instance num: %d. Transaction ID: %d",
				px.me, instanceNum, transactionID);
			return;
		}

		// TODO: Don't not need this but it avoids atleast one RPC. Numbers needed!
		if instance.isDecided() {
			logger.Infof("Transaction complete. Instance %d has already been decided. Transaction ID: %d",
				instanceNum, transactionID);
			return;
		}

		// Backoff for a random duration if the previous attempt failed.
		if transactionAttempts > 1 {
			randomSleepTime := rand.Intn(500);
			time.Sleep(time.Microsecond * (time.Duration)(randomSleepTime));
		}

		/************************* Prepare Phase ***************************/
		ballotNum := px.getNextBallot();
		// Execute the prepare phase.
		logger.Debugf("[%d] Starting prepare phase. Transaction ID: %d, Instance: %d, Ballot: %d, Transaction Attempts: %d",
			px.me, transactionID, instanceNum, ballotNum, transactionAttempts);
		prepareReplies := px.sendPrepare(instanceNum, value, ballotNum);

		// Find the count of peers that have replied with KPrepareOK. Also determine
		// the peer that has replied with the highestAccept. If we find that
		// any peer already has decided value, then we no longer need to continue
		// with this transaction.
		prepareOKCount := 0;
		highestAcceptSeen := (uint64)(0);
		highestAcceptReplyMsg := &PrepareReply{};
		peerWithHighestAccept := -1;
		for peer, prepareReply := range prepareReplies {
			if prepareReply == nil {
				logger.Errorf("[%d] Received nil reply from peer. Peer: %d, Transaction ID: %d, Instance: %d, Ballot: %d",
					px.me, peer, transactionID, instanceNum, ballotNum);
				continue;
			}

			// Update the done value for all peers except this peer. This peer's done
			// value will be updated by the application.
			if px.me != peer {
				// TODO: We can perhaps make this a new goroutine. Need numbers!
				px.mayBeUpdatePeerDoneValue(peer, prepareReply.Done);
			}

			// Check if the peer has a decided value for this instance.
			if prepareReply.Decided {
				// We already have a value decided for this instance. We do not need
				// move forward anymore.
				instance = px.maybeCreateAndGetInstance(instanceNum);
				if instance == nil {
					logger.Warningf("[%d] Aborting transaction. No instance found for instance num: %d. Transaction ID: %d. Cannot update decided value.",
						px.me, instanceNum, transactionID);
					return;
				}
				instance.mu.Lock();
				defer instance.mu.Unlock();
				instance.decided = true;
				instance.decidedValue = prepareReply.DecidedValue;
				logger.Infof("[%d] Transaction complete. Peer %d already has a decided value for this instance. Transaction ID: %d, Instance: %d, Ballot: %d, Decided Value: %v",
					px.me, peer, transactionID, instanceNum, ballotNum,
					prepareReply.DecidedValue);
				return;
			}

			// Update the count of peers that have replied with KPrepareOK.
			if prepareReply.Status == KPrepareOK {
				prepareOKCount += 1;
				if prepareReply.LargestAcceptSeenSoFar >= highestAcceptSeen {
					highestAcceptSeen = prepareReply.LargestAcceptSeenSoFar;
					highestAcceptReplyMsg = prepareReply;
					peerWithHighestAccept = peer;
				}
			} else {
				logger.Warningf("[%d] Prepare request rejected. Peer: %d, Transaction ID: %d, Instance: %d, Ballot: %d, Prepare Reply Status: %s, Message: \"%s\"",
					px.me, peer, transactionID, instanceNum, ballotNum, prepareReply.Status,
					prepareReply.Message);
			}
		}

		// Check if the majority have replied with KPrepareOK.
		if prepareOKCount <= (len(px.peers)/2) {
			// Majority of peers have rejected this prepare request. Retry
			// transaction.
			logger.Errorf("[%d] Prepare request rejected by majority! Transaction ID: %d, Instance: %d, Ballot: %d, Value: %v. Retrying.",
				px.me, transactionID, instanceNum, ballotNum, value);
			continue;
		}

		util.CHECK_NE(peerWithHighestAccept, -1,
			"We have quorum but the peerWithHighestAccept index is -1");

		/************************* Accept Phase ***************************/
		// Quorum have accepted the prepare request. Choose the appropriate value
		// for the instance.
		chosenValue := highestAcceptReplyMsg.LargestAcceptValue;
		if highestAcceptReplyMsg.LargestAcceptSeenSoFar == 0 {
			// None of the peers have any value for this instance. Choose the
			// provided value.
			chosenValue = value;
		}
		logger.Debugf("[%d] Majority have replied with KPrepareOK. Starting accept phase. Transaction ID: %d, Instance: %d, Ballot: %d, Transaction Attempts: %d, Chosen Value: %v",
			px.me, transactionID, instanceNum, ballotNum, transactionAttempts,
			chosenValue);

		// Send accept message to all peers and wait for replies.
		acceptReplies := px.sendAccept(instanceNum, chosenValue, ballotNum);

		// Gather replies from all peers.
		acceptCount := 0;
		for peer, acceptReply := range acceptReplies {
			if acceptReply == nil {
				logger.Errorf("[%d] Received nil Accept Reply from peer. Peer: %d, Transaction ID: %d, Instance: %d",
					px.me, peer, transactionID, instanceNum);
				continue;
			}

			// Update the done value for all peers except this peer. This peer's done
			// value will be updated by the application.
			if px.me != peer {
				// TODO: We can perhaps make this a new goroutine. Numbers!!
				px.mayBeUpdatePeerDoneValue(peer, acceptReply.Done);
			}

			// Check if peer has accepted the chosen value.
			if acceptReply.Status == KAcceptOK {
				acceptCount += 1;
			} else {
				logger.Warningf("[%d] Accept request rejected! Peer: %d, Transaction ID: %d, Instance: %d, Ballot: %d, Chosen Value: %v, Status: %s, Message: %s",
					px.me, peer, transactionID, instanceNum, ballotNum, chosenValue,
					acceptReply.Status, acceptReply.Message);
			}
		}

		if acceptCount <= (len(px.peers)/2) {
			// Quorum have rejected the chosen value. Retry transaction.
			logger.Errorf("[%d] Accept request rejected by majority! Transaction ID: %d, Instance: %d, Ballot: %d, Value: %v. Retrying.",
				px.me, transactionID, instanceNum, ballotNum, chosenValue);
			continue;
		}

		/************************* Decided Phase ************************/
		// Majority have accepted this value. This value is now decided. Send out
		// the decided values to all peers. This stage is not needed for Paxos as
		// Paxos will now ensure that chosenValue will now remain the value for this
		// instance for eternity. Therefore it does not matter now if the decided
		// messages are sent or if we collect the replies. This is purely for
		// overall performance improvement as peers do not have to start new
		// Paxos transactions to find decided values for an instance.
		logger.Infof("[%d] Reached consensus! Transaction ID: %d, Instance: %d, Ballot: %d, Value: %v",
			px.me, transactionID, instanceNum, ballotNum, chosenValue);
		decidedArg := &DecidedArg{};
		decidedArg.DecidedValue = chosenValue;
		decidedArg.InstanceNum = instanceNum;
		decidedReply := &DecidedReply{};
		for ii, peer := range px.peers {
			if ii == px.me {
				go px.decidedHandler(decidedArg);
			} else {
				go call(peer, "Paxos.RemoteDecidedHandler", decidedArg, decidedReply);
			}
		}
		return;
	}
}


/*
Send a prepare message asking all peers to promise not to accept any ballot
numbers lesser than the one provided.
*/
func (px *Paxos) sendPrepare(instanceNum int64, value interface {},
	ballotNum uint64) map[int] *PrepareReply {
	// Initialize prepare messages.
	replyChanMap := make(map[int] chan *PrepareReply);
	replyMap := make(map[int] *PrepareReply);

	// Send a prepare message to all peers(including self) simultaneously.
	// Setup the reply map before sending out the messages. This avoids concurrent reads and writes
	// the the map. Was hitting this issue while running ^TestPartition$.
	for ii:=0; ii < len(px.peers); ii++ {
		replyChanMap[ii] = make(chan *PrepareReply);
	}

	for ii := 0; ii < len(px.peers); ii++ {
		args := &PrepareArg{};
		args.InstanceNum = instanceNum;
		args.Ballot = ballotNum;
		reply := &PrepareReply{};
		if ii == px.me {
			go func(preparePeer int) {
				px.prepareHandler(args, reply);
				replyChanMap[preparePeer] <- reply;
			}(ii);
		} else {
			go func(preparePeer int) {
				status := call(px.peers[preparePeer], "Paxos.RemotePrepareHandler",
					args, reply);
				if status {
					replyChanMap[preparePeer] <- reply;
				} else {
					replyChanMap[preparePeer] <- nil;
				}
			}(ii);
		}
	}

	// logger.Debugf("[%d] Starting a new goroutine to collect all results", px.me);
	// Start a goroutine to collect all the results.
	exitChan := make(chan bool);
	exitedChan := make(chan bool);
	doneChan := make(chan bool);
	go func() {
		for {
			for peerIndex, replyChan := range replyChanMap {
				select {
				case reply := <- replyChan:
					replyMap[peerIndex] = reply;
				case <- exitChan:
					// We have been asked to exit. Return.
					exitedChan <- true;
					return
				default:
					// Do nothing. Just move on.
				}
			}

			// Check if we have collected all the results.
			if len(replyMap) == len(px.peers) {
				doneChan <- true;
				<- exitChan;
				exitedChan <- true;
				return;
			}
		}
	}();

	select {
	case <- doneChan:
		// All the replies have been collected. We are good to go.
	case <- time.After(px.phaseTimeout):
		logger.Warningf("[%d] Timed out waiting for prepare replies from all peers for instance %d. Moving on.",
			px.me, instanceNum);
	}

	// Ask the collector goroutine to exit and wait for it to exit.
	exitChan <- true;
	<- exitedChan;
	return replyMap;
}


/*
Send a proposal to all peers asking them to accept the chosen value. Refer
Paxos pseudo code for more context.
*/
func (px *Paxos) sendAccept(instanceNum int64, value interface {},
	ballotNum uint64) map[int] *AcceptReply {
	// Initialize accept messages.
	args := &AcceptArg{};
	args.InstanceNum = instanceNum;
	args.Ballot = ballotNum;
	args.Value = value;
	replyChanMap := make(map[int] chan *AcceptReply);
	replyMap := make(map[int] *AcceptReply);

	// Send an accept message to all peers(including self) simultaneously.
	// Setup the reply map before sending out the messages. This avoids concurrent reads and writes
	// the the map. Was hitting this issue while running ^TestPartition$.
	for ii:=0; ii < len(px.peers); ii++ {
		replyChanMap[ii] = make(chan *AcceptReply);
	}
	for ii := 0; ii < len(px.peers); ii++ {
		reply := &AcceptReply{};
		if ii == px.me {
			go func(acceptPeer int) {
				px.acceptHandler(args, reply);
				replyChanMap[acceptPeer] <- reply;
			}(ii);
		} else {
			go func(acceptPeer int) {
				status := call(px.peers[acceptPeer], "Paxos.RemoteAcceptHandler",
					args, reply);
				if status {
					replyChanMap[acceptPeer] <- reply;
				} else {
					replyChanMap[acceptPeer] <- nil;
				}
			}(ii);
		}
	}

	// Start a goroutine to collect all the results.
	exitChan := make(chan bool);
	exitedChan := make(chan bool);
	doneChan := make(chan bool);
	go func() {
		for {
			for peerIndex, replyChan := range replyChanMap {
				select {
				case reply := <- replyChan:
					replyMap[peerIndex] = reply;
				case <- exitChan:
					// We have been asked to exit. Return.
					exitedChan <- true;
					return
				default:
					// Do nothing. Just move on.
				}
			}

			// Check if we have collected all the results.
			if len(replyMap) == len(px.peers) {
				doneChan <- true;
				<- exitChan;
				exitedChan <- true;
				return;
			}
		}
	}();

	// Return all results that were collected before phase times out.
	select {
	case <- doneChan:
		// All the replies have been collected. We are good to go.
	case <- time.After(px.phaseTimeout):
		logger.Warningf("[%d] Timed out waiting for accept replies from all peers for instance %d. Moving on.",
			px.me, instanceNum);
	}

	// Ask the collector goroutine to exit and wait for it to exit.
	exitChan <- true;
	<- exitedChan;
	return replyMap;
}


/*
This is the peer's prepare handler. The peer implements the following
pseudocode:
if n > n_p
 	n_p = n
  reply prepare_ok(n_a, v_a)
else
	reply prepare_reject

The peer checks if the received ballot is greater than the highest prepare
seen. If so, it promises not to accept any new ballots lesser than the current
received ballot. If not, it rejects the prepare message.
*/
func (px *Paxos) prepareHandler(args *PrepareArg,
	reply *PrepareReply) error {

	instance := px.maybeCreateAndGetInstance(args.InstanceNum);
	if instance == nil {
		reply.Status = KPrepareReject;
		minInstance := px.getMinInstance();
		message := fmt.Sprintf("Cannot find instance. Given instance: %d, Min instance: %d",
			args.InstanceNum, minInstance);
		reply.Message = message;
		reply.Done = px.getMyDoneValue();
		return nil;
	}

	instance.mu.Lock();
	defer instance.mu.Unlock();
	if args.Ballot > instance.highestPrepare {
		// This is the highest prepare seen so far.
		instance.highestPrepare = args.Ballot;
		reply.Status = KPrepareOK;
		reply.LargestAcceptSeenSoFar = instance.highestAccept;
		reply.LargestAcceptValue = instance.highestAcceptValue;
		reply.Message = "";
		reply.Decided = instance.decided;
		reply.DecidedValue = instance.decidedValue;
	} else {
		reply.Status = KPrepareReject;
		reply.Decided = instance.decided;
		reply.DecidedValue = instance.decidedValue;
		message := fmt.Sprintf("Cannot break a higher promise. Given Ballot: %d, Promised Ballot: %d",
			args.Ballot, instance.highestPrepare);
		reply.Message = message;
	}
	reply.Done = px.getMyDoneValue();
	px.maybeUpdateMaxInstance(args.InstanceNum);
	return nil;
}


/*
This is the peer's accept handler. The peer implements the following
pseudocode:
if n >= n_p
	n_p = n
  n_a = n
  v_a = v
  reply accept_ok(n)
else
  reply accept_reject

The peer checks if the received ballot is >= than the highest prepare
seen. If so, it accepts the value for the given instance. If not, it rejects
the accept request.
*/
func (px *Paxos) acceptHandler(args *AcceptArg,
	reply *AcceptReply) error {

	instance := px.maybeCreateAndGetInstance(args.InstanceNum);
	if instance == nil {
		reply.Status = KAcceptReject;
		minInstance := px.getMinInstance();
		message := fmt.Sprintf("Cannot find instance. Given instance: %d, Min instance: %d",
			args.InstanceNum, minInstance);
		reply.Message = message;
		reply.Done = px.getMyDoneValue();
		return nil;
	}

	// We are now sure that the instance exists. The request can now be processed
	// safely.
	instance.mu.Lock();
	defer instance.mu.Unlock();
	if args.Ballot >= instance.highestPrepare {
		// This is the highest prepare seen so far. We can now accept this value.
		instance.highestPrepare = args.Ballot;
		instance.highestAccept = args.Ballot;
		instance.highestAcceptValue = args.Value;
		reply.Status = KAcceptOK;
		reply.Message = "";
	} else {
		reply.Status = KAcceptReject;
		message := fmt.Sprintf("Cannot break a higher promise. Given Ballot: %d, Promised Ballot: %d",
			args.Ballot, instance.highestPrepare);
		reply.Message = message;
	}
	reply.Done = px.getMyDoneValue();
	px.maybeUpdateMaxInstance(args.InstanceNum);
	return nil;
}


/*
The peer's decided handler. The peer updates the decided fields for the given
instance.
*/
func (px *Paxos) decidedHandler(args *DecidedArg) {
	instance := px.maybeCreateAndGetInstance(args.InstanceNum);
	if instance == nil {
		logger.Warningf("[%d] Could not find instance %d. Not setting decided values",
			px.me, args.InstanceNum);
		return;
	}

	instance.mu.Lock();
	defer instance.mu.Unlock();
	// logger.Debugf("[%d] Acquired lock on instance %d", px.me, args.InstanceNum);
	if instance.decided {
		logger.Infof("[%d] Instance %d has already been decided. Not updating again",
			px.me, args.InstanceNum);
		return;
	}
	instance.decided = true;
	instance.decidedValue = args.DecidedValue;
}


/***********
Paxos RPCs
***********/

/*
This is the remote peer's prepare handler.
*/
func (px *Paxos) RemotePrepareHandler(args *PrepareArg,
	reply *PrepareReply) error {
	px.prepareHandler(args, reply);
	return nil;
}


/*
This is the remote peer's accept handler.
*/
func (px *Paxos) RemoteAcceptHandler(args *AcceptArg,
	reply *AcceptReply) error {
	px.acceptHandler(args, reply);
	return nil;
}

/*
This is the remote peer's decided handler.
*/
func (px *Paxos) RemoteDecidedHandler(args *DecidedArg,
	reply *DecidedReply) error {
	px.decidedHandler(args);
	return nil;
}

/******************
End of Paxos RPCs
******************/

/*
The application wants Paxos to start agreement on instance seq, with
proposed value v.Start() returns right away; the application will call
Status() to find out if/when agreement is reached.
*/
func (px *Paxos) Start(instanceNum int64, value interface{}) {
  logger.Infof("[%d] Initiating Paxos transaction for instance: %d, value: [%v]",
		px.me, instanceNum, value);
	go px.run(instanceNum, value);
}


/*
The application is done with all instances <= seq. See the comments for Min()
for more explanation.
*/
func (px *Paxos) Done(instanceNum int64) {
	for {
		oldDoneValue := atomic.LoadInt64(px.doneMap[px.me]);
		if instanceNum > oldDoneValue {
			swapped := atomic.CompareAndSwapInt64(px.doneMap[px.me], oldDoneValue,
				instanceNum);
			if swapped {
				// This peer's Done value has been successfully updated.
				return;
			} else {
				// Retry.
				continue;
			}
		} else {
			// We have a higher Done value. We can ignore this.
			return;
		}
	}
}


/*
Returns the highest instance number seen by the peer so far.
*/
func (px *Paxos) Max() int64 {
	return px.getMaxInstance();
}


/*
Min() should return one more than the minimum among z_i, where z_i is the
highest number ever passed to Done() on peer i. A peers z_i is -1 if it has
never called Done(). Paxos is required to have forgotten all information about
any instances it knows that are < Min().The point is to free up memory in
long-running Paxos-based servers.

Paxos peers need to exchange their highest Done() arguments in order to
implement Min(). These exchanges can be piggybacked on ordinary Paxos agreement
protocol messages, so it is OK if one peers Min does not reflect another Peers
Done() until after the next instance is agreed to.

The fact that Min() is defined as a minimum over *all* Paxos peers means that
Min() cannot increase until all peers have been heard from. So if a peer is dead
or unreachable, other peers Min()s will not increase even if all reachable
peers call Done. The reason for this is that when the unreachable peer comes
back to life, it will need to catch up on instances that it missed -- the other
peers therefor cannot forget these instances.
*/
func (px *Paxos) Min() int64 {
	return px.getMinInstance();
}


/*
The application wants to know whether this peer thinks an instance has been
decided, and if so what the agreed value is. Status() should just inspect the
local peer state; it should not contact other Paxos peers.
*/
func (px *Paxos) Status(instanceNum int64) (bool, interface{}) {

	instance := px.maybeCreateAndGetInstance(instanceNum);
	if instance == nil {
		// Since this instance does not exist, it must have been forgotten.
		// Therefore, we must have reached a consensus for this instance.
		return true, nil;
	}

	instance.mu.Lock();
	defer instance.mu.Unlock();
	if instance.decided {
		return true, instance.decidedValue;
	}
  return false, nil;
}


/*
Tell the peer to shut itself down for testing.
*/
func (px *Paxos) Kill() {
  px.dead = true
  if px.l != nil {
    px.l.Close()
  }
}


/*
The application wants to create a Paxos peer. The ports of all the Paxos peers
(including this one) are in peers[]. This servers port is peers[me].
*/
func Make(peers []string, me int, rpcs *rpc.Server) *Paxos {
  px := &Paxos{};
  px.peers = peers;
  px.me = me;
	px.initMembers()
	logger.Infof("[%d] Initialized peer %d.", px.me, px.me);

  if rpcs != nil {
    // Caller will create socket &c
    rpcs.Register(px);
  } else {
    rpcs = rpc.NewServer();
    rpcs.Register(px);

    // Prepare to receive connections from clients. Change "unix" to "tcp" to
    // use over a network.
    os.Remove(peers[me]); // only needed for "unix"
    l, e := net.Listen("unix", peers[me]);
    if e != nil {
      log.Fatal("listen error: ", e);
    }
    px.l = l;

    // Please do not change any of the following code, or do anything to
    // subvert it.
    
    // Create a thread to accept RPC connections.
    go func() {
      for px.dead == false {
        conn, err := px.l.Accept()
        if err == nil && px.dead == false {
          if px.unreliable && (rand.Int63() % 1000) < 100 {
            // Discard the request.
            conn.Close()
          } else if px.unreliable && (rand.Int63() % 1000) < 200 {
            // Process the request but force discard of reply.
            c1 := conn.(*net.UnixConn)
            f, _ := c1.File()
            err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
            if err != nil {
              fmt.Printf("shutdown: %v\n", err)
            }
            px.rpcCount++
            go rpcs.ServeConn(conn)
          } else {
            px.rpcCount++
            go rpcs.ServeConn(conn)
          }
        } else if err == nil {
          conn.Close()
        }
        if err != nil && px.dead == false {
          fmt.Printf("Paxos(%v) accept: %v\n", me, err.Error())
        }
      }
    }()
  }
  return px
}
