package paxos

import "testing"
import "runtime"
import "strconv"
import "os"
import "time"
import (
	"fmt"
	"util"
	// "sync/atomic"
)
import (
	"math/rand"
)

func port(tag string, host int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  os.Mkdir(s, 0777)
  s += "px-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += tag + "-"
  s += strconv.Itoa(host)
	fmt.Printf("Host is: %s\n", s);
  return s
}

func ndecided(t *testing.T, pxa []*Paxos, seq int64) int {
  count := 0
  var v interface{}
  for i := 0; i < len(pxa); i++ {
    if pxa[i] != nil {
      decided, v1 := pxa[i].Status(seq)
      if decided {
        if count > 0 && v != v1 {
          t.Fatalf("decided values do not match; seq=%v i=%v v=%v v1=%v",
            seq, i, v, v1);
        }
        count++
        v = v1
      }
    }
  }
  return count
}


func wait(t *testing.T, pxa[]*Paxos, seq int64, wanted int) int {
	to := 10 * time.Millisecond
	for iters := 0; iters < 30; iters++ {
		if ndecided(t, pxa, seq) >= wanted {
			break
		}
		time.Sleep(to)
		if to < time.Second {
			to *= 2
		}
	}
	return ndecided(t, pxa, seq)
}


func waitn(t *testing.T, pxa[]*Paxos, seq int64, wanted int) {
  nd := wait(t, pxa, seq, wanted);
	util.CHECK_EQ(nd, wanted,
		fmt.Sprintf("Too few decided. Instance: %d, Num decided: %d, Wanted: %d",
			seq, nd, wanted));
}

func waitmajority(t *testing.T, pxa[]*Paxos, seq int64) {
	majority := (len(pxa) / 2) + 1;
	nd := wait(t, pxa, seq, majority);
	util.CHECK_GE((int64)(nd), (int64)(majority),
		fmt.Sprintf("Too few decided. Instance: %d, Num decided: %d, Wanted: %d",
			seq, nd, majority));
}

func checkmax(t *testing.T, pxa[]*Paxos, seq int64, max int) {
  time.Sleep(3 * time.Second)
  nd := ndecided(t, pxa, seq)
  if nd > max {
    t.Fatalf("too many decided; seq=%v ndecided=%v max=%v", seq, nd, max)
  }
}

func cleanup(pxa []*Paxos) {
  for i := 0; i < len(pxa); i++ {
    if pxa[i] != nil {
      pxa[i].Kill()
    }
  }
}

func noTestSpeed(t *testing.T) {
  runtime.GOMAXPROCS(4)

  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("time", i)
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil)
  }

  t0 := time.Now()

  for i := 0; i < 20; i++ {
    pxa[0].Start((int64)(i), "x");
    waitn(t, pxa, (int64)(i), npaxos)
  }

  d := time.Since(t0)
  fmt.Printf("20 agreements %v seconds\n", d.Seconds())
}


func TestBasic(t *testing.T) {
	runtime.GOMAXPROCS(4);
	const npaxos = 3;
	var pxa []*Paxos = make([]*Paxos, npaxos);
	var pxh []string = make([]string, npaxos);
	defer cleanup(pxa);
  	for i := 0; i < npaxos; i++ {
		pxh[i] = port("basic", i);
	}
	for i := 0; i < npaxos; i++ {
		pxa[i] = Make(pxh, i, nil);
	}

	// Test Single proposer.
	logger.Info("Test: Single proposer ...\n");
	pxa[0].Start(0, "hello");
	waitn(t, pxa, 0, npaxos);
	logger.Info("Test: Single proposer test ... Passed. \n")

	// Test Many proposers with the same value.
	logger.Info("Test: Many proposers, same value ...\n")
	for i := 0; i < npaxos; i++ {
		pxa[i].Start(1, 77)
	}
	waitn(t, pxa, 1, npaxos)
	logger.Info("Test: Many proposers, same value ... Passed. \n")

	// Test Many proposers with different values.
	logger.Info("Test: Many proposers, different values ...\n")
	pxa[0].Start(2, 100)
	pxa[1].Start(2, 101)
	pxa[2].Start(2, 102)
	waitn(t, pxa, 2, npaxos)
	logger.Info("Test: Many proposers, different values ... Passed. \n")

	// Test out of order sequences.
	logger.Info("Test: Out-of-order instances ...\n")
	pxa[0].Start(7, 700)
	pxa[0].Start(6, 600)
	pxa[1].Start(5, 500)
	waitn(t, pxa, 7, npaxos)
	pxa[0].Start(4, 400)
	pxa[1].Start(3, 300)
	waitn(t, pxa, 6, npaxos)
	waitn(t, pxa, 5, npaxos)
	waitn(t, pxa, 4, npaxos)
	waitn(t, pxa, 3, npaxos)

	if pxa[0].Max() != 7 {
		t.Fatalf("wrong Max()")
	}

	for i := 0; i < npaxos; i++ {
		pxa[i].Kill();
	}

	logger.Info("Test Basic Passed\n")
}

func TestBasicForget(t *testing.T) {
	runtime.GOMAXPROCS(4);
	const npaxos = 3;
	var pxa []*Paxos = make([]*Paxos, npaxos);
	var pxh []string = make([]string, npaxos);
	defer cleanup(pxa);
	for i := 0; i < npaxos; i++ {
		pxh[i] = port("basic", i);
	}
	for i := 0; i < npaxos; i++ {
		pxa[i] = Make(pxh, i, nil);
	}

	pxa[0].Start(0, "hello");
	waitn(t, pxa, 0, npaxos);
	chosenMax := (int64)(7);
	chosenMin := (int64)(3);
	pxa[0].Start(6, 600)
	pxa[1].Start(5, 500)
	pxa[0].Start(4, 400)
	pxa[1].Start(3, 300)
	pxa[2].Start(2, 200)
	pxa[2].Start(1, 200)
	waitn(t, pxa, 6, npaxos)
	waitn(t, pxa, 5, npaxos)
	waitn(t, pxa, 4, npaxos)
	waitn(t, pxa, 3, npaxos)
	waitn(t, pxa, 2, npaxos)
	waitn(t, pxa, 1, npaxos)

	// Set Done values.
	pxa[0].Done(chosenMin);
	pxa[1].Done(chosenMin - 1);
	pxa[2].Done(chosenMin + 1);
	// Let Done values propagate. Start a new transaction.
	pxa[1].Start(chosenMax, 701);
	pxa[2].Start(chosenMax, 702);
	pxa[0].Start(chosenMax, 700);
	waitn(t, pxa, chosenMax, npaxos);

	time.Sleep(time.Millisecond * 20);
	for ii := 0; ii < npaxos; ii++ {
		minValue := pxa[ii].Min();
		maxValue := pxa[ii].Max();
		msgMin := fmt.Sprintf("Min value mismatch for peer %d. Got %d, Expected %d",
			ii, minValue, 2);
		msgMax := fmt.Sprintf("Max value mismatch for peer %d. Got %d, Expected %d",
			ii, maxValue, 7);

		util.CHECK_EQ(maxValue, chosenMax, msgMax);
		util.CHECK_EQ(minValue, chosenMin, msgMin);
	}

	// Check if the instances were deleted from the Paxos maps.
	for ii := 0; ii < npaxos; ii++ {
		for jj := 0; jj < (int)(chosenMin); jj++ {
			_, ok := pxa[ii].instancesMap[(int64)(jj)]
			msg := fmt.Sprintf("Instance %d was found in the instances map of peer %d",
				jj, ii);
			util.CHECK(!ok, msg);
		}

	}

	// Kill all Paxos instances.
	for i := 0; i < npaxos; i++ {
		pxa[i].Kill();
	}
	logger.Info("Test Basic Forget Passed\n")
}


func TestDeaf(t *testing.T) {
  runtime.GOMAXPROCS(4);

  const npaxos = 5;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("deaf", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
  }

  fmt.Printf("[Test] Deaf proposer ...\n");

  pxa[0].Start(0, "hello");
  waitn(t, pxa, 0, npaxos);

  os.Remove(pxh[0]);
  os.Remove(pxh[npaxos-1]);

  pxa[1].Start(1, "goodbye");
  waitmajority(t, pxa, 1);
  time.Sleep(1 * time.Second);
	util.CHECK_EQ(ndecided(t, pxa, 1), npaxos - 2,
		"A deaf peer heard about a decision");

  pxa[0].Start(1, "xxx");
  waitn(t, pxa, 1, npaxos-1);
  time.Sleep(1 * time.Second);
	util.CHECK_EQ(ndecided(t, pxa, 1), npaxos - 1,
		"A deaf peer heard about a decision");

  pxa[npaxos-1].Start(1, "yyy");
  waitn(t, pxa, 1, npaxos);

  fmt.Printf("  ... Passed\n");
}

func TestForget(t *testing.T) {
  runtime.GOMAXPROCS(4);

  const npaxos = 6;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("gc", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
  }

  fmt.Printf("Test: Forgetting ...\n");

  // initial Min() correct?
  for i := 0; i < npaxos; i++ {
    m := pxa[i].Min();
		util.CHECK_EQ((int64)(m), (int64)(-1),
			fmt.Sprintf("Wrong initial min value for peer %d", i));
  }

  pxa[0].Start(0, "00")
  pxa[1].Start(1, "11");
  pxa[2].Start(2, "22");
  pxa[0].Start(6, "66");
  pxa[1].Start(7, "77");

  waitn(t, pxa, 0, npaxos);

  for i := 0; i < npaxos; i++ {
    m := pxa[i].Min();
		util.CHECK_EQ(m, (int64)(-1),
			fmt.Sprintf("Wrong min value for peer %d", i));
  }

  waitn(t, pxa, 1, npaxos);

  // Min() correct?
  for i := 0; i < npaxos; i++ {
		m := pxa[i].Min();
		util.CHECK_EQ(m, (int64)(-1),
			fmt.Sprintf("Wrong min value for peer %d", i));
  }

  // everyone Done() -> Min() changes?
	chosenMin := (int64)(2);
  for i := 0; i < npaxos; i++ {
    pxa[i].Done(chosenMin - 1);
  }
	// Update all peers except the one with index 0. The min should not change
	// as peer[0] still needs it.
  for i := 1; i < npaxos; i++ {
    pxa[i].Done(chosenMin);
  }

	logger.Debugf("[Test] Starting new Paxos transactions to let Done values propagate");
  for i := 0; i < npaxos; i++ {
    pxa[i].Start((int64)(8 + i), "xx");
  }

  allok := false;
  for iters := 0; iters < 12; iters++ {
    allok = true;
    for i := 0; i < npaxos; i++ {
      s := pxa[i].Min();
      if s != 2 {
        allok = false;
      }
    }
    if allok {
      break;
    }
    time.Sleep(1 * time.Second);
  }
	util.CHECK(allok, fmt.Sprintf("Peers do not have expected Min value: %d",
			chosenMin));
  fmt.Printf("  ... Passed\n");
}


func TestManyForget(t *testing.T) {
  runtime.GOMAXPROCS(4)

  const npaxos = 3;
	testDoneMap := make(map[int]int);
	for ii := 0; ii < npaxos; ii++ {
		testDoneMap[ii] = -1;
	}
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("manygc", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
    pxa[i].unreliable = true;
  }

  fmt.Printf("[Test] Lots of forgetting ...\n")

  const maxseq = 20;
  done := false;

  go func() {
    na := rand.Perm(maxseq);
    for i := 0; i < len(na); i++ {
      seq := (int64)(na[i]);
      j := (rand.Int() % npaxos);
      v := rand.Int();
			logger.Debugf("[Test] Initiating Paxos transaction from peer %d, with instance %d",
				j, seq);
      pxa[j].Start(seq, v);
      runtime.Gosched();
    }
  }();

  go func() {
    for done == false {
      seq := (int64)(rand.Int() % maxseq);
      i := (rand.Int() % npaxos);
      if seq >= pxa[i].Min() {
        decided, _ := pxa[i].Status(seq);
        if decided {
					if seq <= maxseq - 5 {
						pxa[i].Done(seq);
						oldDone, _ := testDoneMap[i];
						if seq > (int64)(oldDone) {
							testDoneMap[i] = (int)(seq);
						}
					}
        }
      }
      runtime.Gosched();
    }
  }();

  time.Sleep(5 * time.Second);
  done = true;
  for i := 0; i < npaxos; i++ {
    pxa[i].unreliable = false;
  }
  time.Sleep(2 * time.Second);

	for seq := 0; seq < maxseq; seq++ {
    for i := 0; i < npaxos; i++ {
      if (int64)(seq) >= pxa[i].Min() {
        pxa[i].Status((int64)(seq));
      }
    }
  }

	for i := 0; i < npaxos; i++ {
		pxa[i].Start((int64)(maxseq + i + 1), "xx");
	}
	waitn(t, pxa, maxseq + 1, npaxos);
	waitn(t, pxa, maxseq + npaxos - 1, npaxos);

	minDone := -1
	for ii := 0; ii < npaxos; ii++ {
		if ii == 0 {
			minDone = testDoneMap[ii];
		} else {
			if testDoneMap[ii] < minDone { minDone = testDoneMap[ii] }
		}
	}

	for ii := 0; ii < npaxos; ii++ {
		util.CHECK_EQ(pxa[ii].Min(), (int64)(minDone + 1),
			fmt.Sprintf("Peer %d does not have expected min %d", ii, minDone + 1));
	}

  fmt.Printf("  ... Passed\n");
}


// does paxos forgetting actually free the memory?

func TestForgetMem(t *testing.T) {
  runtime.GOMAXPROCS(4);

  fmt.Printf("Test: Paxos frees forgotten instance memory ...\n");

  const npaxos = 3;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("gcmem", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
  }

  pxa[0].Start(0, "x");
  waitn(t, pxa, 0, npaxos);

  runtime.GC();
  var m0 runtime.MemStats;
  runtime.ReadMemStats(&m0);
  // m0.Alloc about a megabyte

  for i := 1; i <= 10; i++ {
    big := make([]byte, 1000000);
    for j := 0; j < len(big); j++ {
      big[j] = byte('a' + rand.Int() % 26);
    }
    pxa[0].Start((int64)(i), string(big));
    waitn(t, pxa, (int64)(i), npaxos);
  }

  runtime.GC();
  var m1 runtime.MemStats;
  runtime.ReadMemStats(&m1);
  // m1.Alloc about 90 megabytes

  for i := 0; i < npaxos; i++ {
    pxa[i].Done(10);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i].Start((int64)(11 + i), "z");
  }
  time.Sleep(2 * time.Second);
  for i := 0; i < npaxos; i++ {
		peerMin := pxa[i].Min();
		util.CHECK_EQ(peerMin, (int64)(11), fmt.Sprintf("Expected Min: %d, Got: %d",
				11, peerMin));
  }

  runtime.GC();
  var m2 runtime.MemStats;
  runtime.ReadMemStats(&m2);
  // m2.Alloc about 10 megabytes

	util.CHECK_LT((int64)(m2.Alloc), (int64)(m1.Alloc / 2),
		"Memory use did not shrink enough");
	fmt.Printf("  ... Passed\n");
}


func TestRPCCount(t *testing.T) {
  runtime.GOMAXPROCS(4);

  fmt.Printf("Test: RPC counts aren't too high ...\n");

  const npaxos = 3;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("count", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
  }

  ninst1 := 5
  seq := 0
  for i := 0; i < ninst1; i++ {
    pxa[0].Start((int64)(seq), "x");
    waitn(t, pxa, (int64)(seq), npaxos);
    seq++;
  }

  time.Sleep(2 * time.Second);

  total1 := 0;
  for j := 0; j < npaxos; j++ {
    total1 += pxa[j].rpcCount;
  }

  // per agreement:
  // 3 prepares
  // 3 accepts
  // 3 decides
  expected1 := ninst1 * npaxos * npaxos;
	util.CHECK_LE((int64)(total1), (int64)(expected1),
		fmt.Sprintf("Too many RPCs for serial Start()s; %v instances, Got: %v, Expected: %v",
			ninst1, total1, expected1));

  ninst2 := 5;
  for i := 0; i < ninst2; i++ {
    for j := 0; j < npaxos; j++ {
      go pxa[j].Start((int64)(seq), j + (i * 10));
    }
    waitn(t, pxa, (int64)(seq), npaxos);
    seq++;
  }

  time.Sleep(2 * time.Second);

  total2 := 0
  for j := 0; j < npaxos; j++ {
    total2 += pxa[j].rpcCount;
  }
  total2 -= total1;

  // worst case per agreement:
  // Proposer 1: 3 prep, 3 acc, 3 decides.
  // Proposer 2: 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
  // Proposer 3: 3 prep, 3 acc, 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
  expected2 := ninst2 * npaxos * 15;
	util.CHECK_LE((int64)(total1), (int64)(expected1),
		fmt.Sprintf("Too many RPCs for concurrent Start()s; %v instances, Got: %v, Expected: %v",
			ninst2, total2, expected2));

  fmt.Printf("  ... Passed\n");
}

//
// many agreements (without failures)
//
func TestMany(t *testing.T) {
  runtime.GOMAXPROCS(4);

  fmt.Printf("Test: Many instances ...\n");

  const npaxos = 3;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("many", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
    pxa[i].Start(0, 0);
  }

  const ninst = 50;
  for seq := 1; seq < ninst; seq++ {
    // only 5 active instances, to limit the number of file descriptors.
    for seq >= 5 && ndecided(t, pxa, (int64)(seq - 5)) < npaxos {
      time.Sleep(20 * time.Millisecond);
    }
    for i := 0; i < npaxos; i++ {
      pxa[i].Start((int64)(seq), (seq * 10) + i);
    }
  }

  for {
    done := true;
    for seq := 1; seq < ninst; seq++ {
      if ndecided(t, pxa, (int64)(seq)) < npaxos {
        done = false;
      }
    }
    if done {
      break;
    }
    time.Sleep(100 * time.Millisecond);
  }

  fmt.Printf("  ... Passed\n");
}

//
// a peer starts up, with proposal, after others decide.
// then another peer starts, without a proposal.
//
func TestOld(t *testing.T) {
  runtime.GOMAXPROCS(4);

  fmt.Printf("Test: Minority proposal ignored ...\n");

  const npaxos = 5;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("old", i);
  }

  pxa[1] = Make(pxh, 1, nil);
  pxa[2] = Make(pxh, 2, nil);
  pxa[3] = Make(pxh, 3, nil);
  pxa[1].Start(1, 111);

  waitmajority(t, pxa, 1);

  pxa[0] = Make(pxh, 0, nil);
  pxa[0].Start(1, 222);

  waitn(t, pxa, 1, npaxos - 1);

  fmt.Printf("  ... Passed\n");
}

//
// many agreements, with unreliable RPC
//
func TestManyUnreliable(t *testing.T) {
  runtime.GOMAXPROCS(4);

  fmt.Printf("Test: Many instances, unreliable RPC ...\n");

  const npaxos = 3;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  var pxh []string = make([]string, npaxos);
  defer cleanup(pxa);

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("manyun", i);
  }
  for i := 0; i < npaxos; i++ {
    pxa[i] = Make(pxh, i, nil);
    pxa[i].unreliable = true;
    pxa[i].Start(0, 0);
  }

  const ninst = 50;
  for seq := 1; seq < ninst; seq++ {
    // only 3 active instances, to limit the
    // number of file descriptors.
    for seq >= 3 && ndecided(t, pxa, (int64)(seq - 3)) < npaxos {
      time.Sleep(20 * time.Millisecond);
    }
    for i := 0; i < npaxos; i++ {
      pxa[i].Start((int64)(seq), (seq * 10) + i);
    }
  }

  for {
    done := true;
    for seq := 1; seq < ninst; seq++ {
      if ndecided(t, pxa, (int64)(seq)) < npaxos {
        done = false;
      }
    }
    if done {
      break;
    }
    time.Sleep(100 * time.Millisecond);
  }

  fmt.Printf("  ... Passed\n");
}


func pp(tag string, src int, dst int) string {
  s := "/var/tmp/824-";
  s += strconv.Itoa(os.Getuid()) + "/";
  s += "px-" + tag + "-";
  s += strconv.Itoa(os.Getpid()) + "-";
  s += strconv.Itoa(src) + "-";
  s += strconv.Itoa(dst);
  return s;
}

func cleanpp(tag string, n int) {
  for i := 0; i < n; i++ {
    for j := 0; j < n; j++ {
      ij := pp(tag, i, j);
      os.Remove(ij);
    }
  }
}

func part(t *testing.T, tag string, npaxos int, p1 []int, p2 []int, p3 []int) {
	cleanpp(tag, npaxos);
  pa := [][]int{p1, p2, p3};
  for pi := 0; pi < len(pa); pi++ {
    p := pa[pi];
    for i := 0; i < len(p); i++ {
      for j := 0; j < len(p); j++ {
        ij := pp(tag, p[i], p[j]);
        pj := port(tag, p[j]);
        err := os.Link(pj, ij);
        if err != nil {
          // one reason this link can fail is if the
          // corresponding Paxos peer has prematurely quit and
          // deleted its socket file (e.g., called px.Kill()).
          t.Fatalf("os.Link(%v, %v): %v\n", pj, ij, err);
        }
      }
    }
  }
}

func TestPartition(t *testing.T) {
  runtime.GOMAXPROCS(4);

  tag := "partition";
  const npaxos = 5;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  defer cleanup(pxa);
  defer cleanpp(tag, npaxos);

  for i := 0; i < npaxos; i++ {
    var pxh []string = make([]string, npaxos);
    for j := 0; j < npaxos; j++ {
      if j == i {
        pxh[j] = port(tag, i);
      } else {
        pxh[j] = pp(tag, i, j);
      }
    }
    pxa[i] = Make(pxh, i, nil);
  }
  defer part(t, tag, npaxos, []int{}, []int{}, []int{});
  seq := 0;

	fmt.Printf("Test: No decision if partitioned ...\n");
  part(t, tag, npaxos, []int{0,2}, []int{1,3}, []int{4});
  pxa[1].Start((int64)(seq), 111);
  checkmax(t, pxa, (int64)(seq), 0);
  fmt.Printf("  ... Passed\n");

  fmt.Printf("Test: Decision in majority partition ...\n");
  part(t, tag, npaxos, []int{0}, []int{1,2,3}, []int{4});
  time.Sleep(2 * time.Second);
  waitmajority(t, pxa, (int64)(seq));
  fmt.Printf("  ... Passed\n");

  fmt.Printf("Test: All agree after full heal ...\n");
  pxa[0].Start((int64)(seq), 1000); // poke them
  pxa[4].Start((int64)(seq), 1004);
  part(t, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{});
  waitn(t, pxa, (int64)(seq), npaxos);
  fmt.Printf("  ... Passed\n");

  fmt.Printf("Test: One peer switches partitions ...\n");
  for iters := 0; iters < 20; iters++ {
    seq++;
    part(t, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{});
		chosenValue := seq * 10;
    pxa[0].Start((int64)(seq), chosenValue);
    pxa[3].Start((int64)(seq), chosenValue + 1);
    waitmajority(t, pxa, (int64)(seq));
    if ndecided(t, pxa, (int64)(seq)) > 3 {
      t.Fatalf("too many decided");
    }

    part(t, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{});
		pxa[npaxos - 1].Start((int64)(seq), chosenValue + 2);
    waitn(t, pxa, (int64)(seq), npaxos);
		for ii := 0; ii < len(pxa); ii++ {
			_, decidedValue := pxa[ii].Status((int64)(seq))
			util.CHECK_EQ(chosenValue, decidedValue,
				fmt.Sprintf("Chosen value: %d does not match Decided Value: %d for peer: %d",
					chosenValue, decidedValue, ii));
		}
  }
  fmt.Printf("  ... Passed\n");

  fmt.Printf("Test: One peer switches partitions, unreliable ...\n");
  for iters := 0; iters < 20; iters++ {
    seq++;

    for i := 0; i < npaxos; i++ {
      pxa[i].unreliable = true;
    }

    part(t, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
    for i := 0; i < npaxos; i++ {
      pxa[i].Start((int64)(seq), (seq * 10) + i);
    }
    waitn(t, pxa, (int64)(seq), 3);
    if ndecided(t, pxa, (int64)(seq)) > 3 {
      t.Fatalf("too many decided");
    }

    part(t, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{});
    for i := 0; i < npaxos; i++ {
      pxa[i].unreliable = false;
    }
    waitn(t, pxa, (int64)(seq), 5);
  }
  fmt.Printf("  ... Passed\n");
}

func TestLots(t *testing.T) {
  runtime.GOMAXPROCS(4);

  fmt.Printf("Test: Many requests, changing partitions ...\n");

  tag := "lots";
  const npaxos = 5;
  var pxa []*Paxos = make([]*Paxos, npaxos);
  defer cleanup(pxa);
  defer cleanpp(tag, npaxos);

  for i := 0; i < npaxos; i++ {
    var pxh []string = make([]string, npaxos);
    for j := 0; j < npaxos; j++ {
      if j == i {
        pxh[j] = port(tag, i);
      } else {
        pxh[j] = pp(tag, i, j);
      }
    }
    pxa[i] = Make(pxh, i, nil);
    pxa[i].unreliable = true;
  }
  defer part(t, tag, npaxos, []int{}, []int{}, []int{});

  done := false;

  // re-partition periodically
  ch1 := make(chan bool);
  go func() {
    defer func(){ ch1 <- true }();
    for done == false {
      var a [npaxos]int;
      for i := 0; i < npaxos; i++ {
        a[i] = (rand.Int() % 3);
      }
      pa := make([][]int, 3);
      for i := 0; i < 3; i++ {
        pa[i] = make([]int, 0);
        for j := 0; j < npaxos; j++ {
          if a[j] == i {
            pa[i] = append(pa[i], j);
          }
        }
      }
      part(t, tag, npaxos, pa[0], pa[1], pa[2]);
      time.Sleep(time.Duration(rand.Int63() % 200) * time.Millisecond);
    }
  }();

  seq := 0;

  // periodically start a new instance
  ch2 := make(chan bool)
  go func () {
    defer func() { ch2 <- true } ();
    for done == false {
      // how many instances are in progress?
      nd := 0;
      for i := 0; i < seq; i++ {
        if ndecided(t, pxa, (int64)(i)) == npaxos {
          nd++;
        }
      }
      if seq - nd < 10 {
        for i := 0; i < npaxos; i++ {
          pxa[i].Start((int64)(seq), rand.Int() % 10);
        }
        seq++;
      }
      time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond);
    }
  }();

  // periodically check that decisions are consistent
  ch3 := make(chan bool);
  go func() {
    defer func() { ch3 <- true }();
    for done == false {
      for i := 0; i < seq; i++ {
        ndecided(t, pxa, (int64)(i));
      }
      time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond);
    }
  }();

  time.Sleep(20 * time.Second);
  done = true;
  <- ch1;
  <- ch2;
  <- ch3;

  // repair, then check that all instances decided.
  for i := 0; i < npaxos; i++ {
    pxa[i].unreliable = false;
  }
  part(t, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{});
  time.Sleep(5 * time.Second);

  for i := 0; i < seq; i++ {
    waitmajority(t, pxa, (int64)(i));
  }

  fmt.Printf("  ... Passed\n");
}
