package util

import "log"
import "fmt"


/*
Check that value is true. If not, panic.
*/
func CHECK(a bool, errorMessage string) {
	if !a {
		s := fmt.Sprintf("CHECK failed: %v. %s", a, errorMessage)
		log.Panicf(s)
	}

}


/*
Check that the values are the same. If not, panic.
*/
func CHECK_EQ(a interface{}, b interface{}, errorMessage string) {
	if !(a == b) {
		s := fmt.Sprintf("CHECK_EQ failed: %v == %v. %s", a, b, errorMessage)
		log.Panicf(s)
	}
}


/*
Check that the values are not the same. If they are the same, panic.
*/
func CHECK_NE(a interface{}, b interface{}, errorMessage string) {
	if !(a != b) {
		// They are equal.
		s := fmt.Sprintf("CHECK_NE failed: %v != %v. %s", a, b, errorMessage)
		log.Panicf(s)
	}
}


/*
Check that the value of a > b. If not, panic.
*/
func CHECK_GT(a int64, b int64, errorMessage string) {
	if !(a > b) {
		s := fmt.Sprintf("CHECK_GT failed: %d > %d. %s", a, b, errorMessage)
		log.Panicf(s)
	}
}


/*
Check if a < b. If not, panic.
*/
func CHECK_LT(a int64, b int64, errorMessage string) {
	if !(a < b) {
		s := fmt.Sprintf("CHECK_LT failed: %d < %d. %s", a, b, errorMessage)
		log.Panicf(s)
	}
}

/*
Check if a >= b. If not, panic.
*/
func CHECK_GE(a int64, b int64, errorMessage string) {
	if !(a >= b) {
		s := fmt.Sprintf("CHECK_GE failed: %v >= %v. %s", a, b, errorMessage)
		log.Panicf(s)
	}
}


/*
Check if a <= b. If not, panic.
*/
func CHECK_LE(a int64, b int64, errorMessage string) {
	if !(a <= b) {
		s := fmt.Sprintf("CHECK_LE failed: %d <= %d. %s", a, b, errorMessage)
		log.Panicf(s)
	}
}
