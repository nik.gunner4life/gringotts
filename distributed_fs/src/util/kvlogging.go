package util


import "github.com/op/go-logging"


func GetLogger(moduleName string, level logging.Level) (*logging.Logger) {
	var logger = logging.MustGetLogger(moduleName);

	// Example format string. Everything except the message has a custom color
	// which is dependent on the log level. Many fields have a custom output
	// formatting too, eg. the time returns the hour down to the milli second.
	format := logging.MustStringFormatter(
		`[%{time:01-02 15:04:05.000} %{shortfile}] %{level:.8s}: %{message}`,
	);
	logging.SetFormatter(format)
	logging.SetLevel(level, moduleName);
	return logger;
}
