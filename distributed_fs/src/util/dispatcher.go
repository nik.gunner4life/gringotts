package util

/*
Dispatcher executes the ops given to it in the same order as they were
submitted. The dispatcher is asynchronous i.e. the Op is added to the queue of
Ops to be performed and if total number of active goroutines does not exceed
maxRoutines, then the Op is started at once. Otherwise, the dispatcher waits
till the number of active goroutines falls below maxRoutines. This helps
control the number of goroutines that are executing at any point of time which
helps in avoiding a lot of context switches.

The dispatcher currently only works with the base Op interface defined here.
All ops submitted to the dispatcher must implement Op interface. Every Op is
first executed and once the Op finishes, the DoneCallback is executed to
indicate the Op has completed.

Go routines that are waiting for an Op to complete must ensure that the logic
is encoded in DoneCallback. For example, an Op can simply set an Event object
or use channels for this.

All methods exposed by the dispatcher are thread safe.
*/

import "github.com/op/go-logging"
import "sync/atomic"
import "time"

// Dispatcher constants.
const (
	// Error message to return when no new Ops can be added to the dispatcher.
	KDispatcherFull = "KDispatcherFull";
	// Success Message
	KSuccess = "KSuccess"
)

// Create a logger object
var logger = GetLogger("util-dispatcher", logging.DEBUG)


/*
Base Op Type that the dispatcher can work with. All Ops submitted to the
dispatcher must inherit/implement this base Op struct.
 */
type Op interface {
	Execute();
	DoneCallback();
}

/*
The data structure that defines the members and methods of the dispatcher.
*/
type Dispatcher struct {
	// Maximum number of goroutines that can be forked by the dispatcher.
	maxRoutines uint32;
	// Total number of goroutines that are active at any given point of time.
	numActiveRoutines uint32;
	// Incremental Op ID that is generated for every Op submitted to the
	// dispatcher.
	opID *uint64;
	// Max channel size i.e. total number of Ops that are in flight/queued with
	// the dispatcher.
	maxOpChannelSize uint32;
	// The incoming channel where Ops are submitted. The farmer goroutine
	// checks the incoming channel and spawns the Op if a goroutine is
	// available.
	incomingChannel chan Op;
	// The channel where the farmer goroutine dumps Op IDs that have completed.
	opDoneChannel chan uint64;
}

/*
Create and initialize a new dispatcher object.
*/
func NewDispatcher(maxGoRoutines uint32, queueSize uint32) *Dispatcher {
	dispatcher := new(Dispatcher);
	dispatcher.initDispatcher(maxGoRoutines, queueSize);
	return dispatcher;
}

/*
Initializes the dispatcher object.
*/
func (dispatcher *Dispatcher) initDispatcher(maxRoutines uint32, queueSize uint32) {
	dispatcher.maxRoutines = maxRoutines;
	dispatcher.opID = new(uint64);
	*dispatcher.opID = 0;
	dispatcher.maxOpChannelSize = queueSize;
	dispatcher.incomingChannel = make(chan Op,
																		dispatcher.maxOpChannelSize);
	dispatcher.opDoneChannel = make(chan uint64, dispatcher.maxOpChannelSize);
	dispatcher.numActiveRoutines = 0;
	// Start the farmer goroutine.
	go dispatcher.executeOps()
}

/*
Returns the next Op ID. Op IDs are incremental and every op executed by
the dispatcher has a unique op id.
*/
func (dispatcher *Dispatcher) getNextOpID() uint64 {
	opID := atomic.AddUint64(dispatcher.opID, (uint64)(1))
	return opID;
}

/*
Executes the given Op using a new goroutine. Once the Op finishes, the callback
function is invoked after which the Op ID sent over the opDoneChannel to
indicate the Op has finished executing.
*/
func (dispatcher *Dispatcher) startOp(op Op, opID uint64) {
	go func() {
		op.Execute();
		op.DoneCallback();
		dispatcher.opDoneChannel <- opID;
	}()
}

/*
Executes the ops that are present in the incoming channel. It farsm out
ops as long as numActiveRoutines is lesser than the maxRoutines. If not, the
farmer goroutine waits till one of the active Ops finishes before scheduling
the next op.
*/
func (dispatcher *Dispatcher) executeOps() {
	for {
		// Check if any ops have finished. If so, reclaim those goroutines and
		// update books accordingly.
		select {
		case <-dispatcher.opDoneChannel:
			// The op with ID doneOpID has completed. Decrement the number of active
			// goroutines.
			dispatcher.numActiveRoutines--;
		default:
			// Did not find any op that has finished. Moving on.
		}

		// Check if any new op can be spawned. If so, start the next op.
		if dispatcher.numActiveRoutines < dispatcher.maxRoutines {
			// We have some routines that we can still use.
			select {
			case op := <-dispatcher.incomingChannel:
				opID := dispatcher.getNextOpID();
				// Increment the number of active goroutines.
				dispatcher.numActiveRoutines++;
				// Starts the Op execution and returns i.e. does not wait for the Op
				// to finish.
				dispatcher.startOp(op, opID);
			default:
				// There is no op in the incoming channel. Do nothing.
			}
		}
	}
}

/*
Adds an op to the dispatcher. Returns an KDispatcherFull if the Op could not be
added to the dispatcher queue.
*/
func (dispatcher *Dispatcher) AddOp(op Op) (string) {
	select {
	case dispatcher.incomingChannel <- op:
		// Successfully added the op the channel.
		return KSuccess;
	default:
		// Unable to add op to the channel. This can happen when the channel is
		// full.
		return KDispatcherFull;
	}
}

/*
This function is similar to AddOp except that it gives up trying to add the op
only after a specified timeout(in microseconds). Returns KSuccess if the Op
was successfully added to the dispatcher queue. Returns KDispatcherFull if the
op could not be added.
*/
func (dispatcher *Dispatcher) AddOpOrTimeout(op Op,	timeout uint64) (string) {
	select {
	case dispatcher.incomingChannel <- op:
		// Successfully added the op the channel.
		return KSuccess;
	case <-time.After(time.Microsecond * (time.Duration)(timeout)):
		// Unable to add op to the channel. This can happen when the channel is
		// full.
		return KDispatcherFull;
	}
}
