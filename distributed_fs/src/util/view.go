package util

type View struct {
	Viewnum uint;
	Primary string;
	Backup string;
}


type PingArgs struct {
	Me string;
	Viewnum uint;
}


type PingReply struct {
	View View;
}
