package util


/*
Event is a thread safe data structure that goroutines can use to synchronize
between themselves. It is similar in behavior to the multiprocessing.Event data
structure in python.

The event object can be set or cleared and can be tested to check if it is set
or not. The event object exposes the following methods:
	Examples:
		event = NewEvent();

		To set the event:
		event.Set()

		To clear the event:
		event.Clear()

		To check if the event is set,
		event.IsSet()
		This returns true, if the event is set and false otherwise.

		To check if the event is unset/clear,
		event.IsClear()
		This returns true if the event is not set and false otherwise.

All methods exposed by event are thread safe.
*/


import "sync"


type Event struct {
	value bool;
	eventLock sync.Mutex
}

func NewEvent() *Event {
	event := new(Event);
	event.value = false;
	return event;
}

/*
Sets the event.
*/
func(event *Event) Set() {
	event.eventLock.Lock()
	defer event.eventLock.Unlock()
	event.value = true;
}

/*
Clears the event.
*/
func(event *Event) Clear() {
	event.eventLock.Lock()
	defer event.eventLock.Unlock()
	event.value = false;
}

/*
Checks if the event is set.
*/
func(event *Event) IsSet() bool {
	event.eventLock.Lock()
	defer event.eventLock.Unlock()
	event.value = false;
	return event.value;
}

/*
Checks if the event is unset.
*/
func(event *Event) IsClear() bool {
	event.eventLock.Lock()
	defer event.eventLock.Unlock()
	event.value = false;
	return !event.value;
}

