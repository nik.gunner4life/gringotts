package util

import "net/rpc"
import "fmt"

/*
All RPCs should be made using only this method. This function contacts
the server with the RPC na,e provided.

args: Any type/struct that needs to be sent to the server.
reply: The server leaves the reply in the reply struct.
*/
func Call(server string, rpcname string, args interface{},
	reply interface{}) bool {
	c, errx := rpc.Dial("unix", server)
	if errx != nil {
		return false
	}
	defer c.Close()

	err := c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
