package util

import "testing"
import "fmt"
import "time"

type TestOp struct {
	id int;
	sleepTime time.Duration;
	Done chan bool;
}

func NewTestOp(id int, execSleepTime time.Duration) *TestOp {
	testOp := new(TestOp);
	testOp.id = id;
	testOp.sleepTime = execSleepTime;
	testOp.Done = make(chan bool);
	return testOp;
}

func (testOp *TestOp) Execute() {
	logger.Debugf("Op %d has started executing. Sleeping for %v", testOp.id,
		testOp.sleepTime);
	time.Sleep(testOp.sleepTime);
	logger.Debugf("Op %d has finished executing", testOp.id);
}

func (testOp *TestOp) DoneCallback() {
	testOp.Done <- true;
	logger.Debugf("Done Event has been set for Op %d", testOp.id);
}


func TestDispatcher(t *testing.T) {
	// Test basic functionality. Dispatch Ops and check to see if they have
	// completed.
	func() {
		logger.Infof("Testing dispatcher ordering")
		dispatcher := NewDispatcher((uint32)(4), (uint32)(4));
		testOp1 := NewTestOp(1, (time.Millisecond * 100));
		testOp2 := NewTestOp(2, (time.Millisecond * 200));
		testOp3 := NewTestOp(3, (time.Millisecond * 300));
		result := dispatcher.AddOp(testOp1);
		CHECK_EQ(result, KSuccess, "Unable to add op 1 to dispatcher");
		result = dispatcher.AddOp(testOp2);
		CHECK_EQ(result, KSuccess, "Unable to add op 2 to dispatcher");
		result = dispatcher.AddOp(testOp3);
		CHECK_EQ(result, KSuccess, "Unable to add op 3 to dispatcher");
		order := 0;
		// Wait for all ops to finish.
		for {
			select {
			case res1 := <-testOp1.Done:
				order++;
				CHECK(res1, "Result from Op 1 was not true");
				msg := fmt.Sprintf("Op %d finished at %d", testOp1.id, order);
				CHECK_EQ(order, 1, msg);
			case res2 := <-testOp2.Done:
				order++;
				CHECK(res2, "Result from Op 2 was not true");
				msg := fmt.Sprintf("Op %d finished at %d", testOp2.id, order);
				CHECK_EQ(order, 2, msg);
			case res3 := <-testOp3.Done:
				order++;
				CHECK(res3, "Result from Op 2 was not true")
				msg := fmt.Sprintf("Op %d finished at %d", testOp3.id, order);
				CHECK_EQ(order, 3, msg);
				return
			}
		}
	}()
	logger.Infof("Dispatcher ordering test passed")

	func() {
		logger.Infof("Testing full dispatcher")
		dispatcher := NewDispatcher((uint32)(3), (uint32)(3));
		testOp1 := NewTestOp(1, (time.Millisecond * 100));
		testOp2 := NewTestOp(2, (time.Millisecond * 200));
		testOp3 := NewTestOp(3, (time.Millisecond * 300));
		testOp4 := NewTestOp(4, (time.Millisecond * 400));
		result := dispatcher.AddOp(testOp1);
		CHECK_EQ(result, KSuccess, "Unable to add op 1 to dispatcher");
		result = dispatcher.AddOp(testOp2);
		CHECK_EQ(result, KSuccess, "Unable to add op 2 to dispatcher");
		result = dispatcher.AddOp(testOp3);
		CHECK_EQ(result, KSuccess, "Unable to add op 3 to dispatcher");
		result = dispatcher.AddOp(testOp4);
		CHECK_EQ(result, KDispatcherFull, "Added Op 4 even though it should not have been allowed");
		order := 0;
		// Wait for all ops to finish.
		for {
			select {
			case res1 := <-testOp1.Done:
				order++;
				CHECK(res1, "Result from Op 1 was not true");
				msg := fmt.Sprintf("Op %d finished at %d", testOp1.id, order);
				CHECK_EQ(order, 1, msg);
				logger.Debug("Adding op4 to the test")
				result = dispatcher.AddOp(testOp4);
				CHECK_EQ(result, KSuccess, "Unable to add op 4 to dispatcher");
			case res2 := <-testOp2.Done:
				order++;
				CHECK(res2, "Result from Op 2 was not true");
				msg := fmt.Sprintf("Op %d finished at %d", testOp2.id, order);
				CHECK_EQ(order, 2, msg);
			case res3 := <-testOp3.Done:
				order++;
				CHECK(res3, "Result from Op 2 was not true")
				msg := fmt.Sprintf("Op %d finished at %d", testOp3.id, order);
				CHECK_EQ(order, 3, msg);
			case res4 := <-testOp4.Done:
				order++;
				CHECK(res4, "Result from Op 4 was not true")
				msg := fmt.Sprintf("Op %d finished at %d", testOp4.id, order);
				CHECK_EQ(order, 4, msg);
				return
			}
		}

	}()
	logger.Infof("Full dispatcher test passed")

	logger.Infof("Done with dispatcher tests.")
}
