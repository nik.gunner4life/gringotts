package main

// All imports.
import "os"
import "fmt"
import "mapreduce"
import "container/list"
import "strconv"
import "strings"
import "regexp"

// our simplified version of MapReduce does not supply a
// key to the Map function, as in the paper; only a value,
// which is a part of the input file contents
// Description: This function will split the entire string
// into a splice of words and using a map, we need to get
// a count of every word in string. And then return a list
// of KeyValue pairs. Look at KeyValue pair in mapreduce.go.
func Map(value string) *list.List {
	// Generate three regexes to assist in splitting the words.
	newLineRegex, _ := regexp.Compile("\\n")
	wordCheckRegex, _ := regexp.Compile("[a-zA-Z]+")
	cleanWordRegex, _ := regexp.Compile("\\W*")

	// Convert all newline characters to spaces
	safeValue := newLineRegex.ReplaceAllString(value, " ")

	// Split the string by space
	words := strings.Split(safeValue, " ")
	// Generate a regular expression to remove all special characters
	// from the word. This will keep it clean.
	wordMap := make(map[string]int)
	// Get a count of every word in words
	for _, word := range words {
		// Remove all special characters from the word
		word = cleanWordRegex.ReplaceAllString(word, "")
		// If a word is just blanks, then ignore it.
		if !wordCheckRegex.MatchString(word) {
			continue
		}

		// The word is valid. Update wordMap count appropriately.
		if _, rv := wordMap[word]; rv {
			wordMap[word] += 1
		} else {
			wordMap[word] = 1
		}
	}
	// Convert wordMap to a list of KeyValue Pairs and return it.
	wordCountList := list.New()
	for word, count := range wordMap {
		var pair mapreduce.KeyValue
		pair.Key = word
		pair.Value = strconv.Itoa(count)
		wordCountList.PushBack(pair)
	}
	return wordCountList
}

/*
Calculate the sum of values.
 */
func Reduce(key string, values *list.List) string {
	sum := 0
	for element := values.Front(); element != nil; element = element.Next() {
		// The .(int) is type conversion. Because, for go, the value is an interface
		// As a result, operators will not be permitted. Hence to access the value
		// we need to assert the type so that go can understand it. Rather neat!
		value, err := strconv.Atoi(element.Value.(string))
		if err != nil {
			fmt.Println("Invalid string was passed.")
			return "-1"
		}
		sum = sum + value
	}
	sumString := strconv.Itoa(sum)
	return sumString
}

// Can be run in 3 ways:
// 1) Sequential (e.g., go run wc.go master x.txt sequential)
// 2) Master (e.g., go run wc.go master x.txt localhost:7777)
// 3) Worker (e.g., go run wc.go worker localhost:7777 localhost:7778 &)
func main() {
	if len(os.Args) != 4 {
    	fmt.Printf("%s: see usage comments in file\n", os.Args[0])
  	} else if os.Args[1] == "master" {
	    if os.Args[3] == "sequential" {
	      mapreduce.RunSingle(3, 3, os.Args[2], Map, Reduce)
	    } else {
	      mr := mapreduce.MakeMapReduce(3, 3, os.Args[2], os.Args[3])
	      // Wait until MR is done
	      <- mr.DoneChannel
	    }
  	} else {
	    mapreduce.RunWorker(os.Args[2], os.Args[3], Map, Reduce, 200)
  	}
}
