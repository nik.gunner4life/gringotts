package main

/*******************************************************************************
Author		: Nikhil Srivatsan Srinivasan
Date		: 06/26/2015
Description : Start the magistrate and mudders.
*******************************************************************************/

import "os"
import (
	"jaynestown"
	"log"
)


func main() {
	if os.Args[1] == "magistrate" {
		canton := jaynestown.MakeCanton(os.Args[2]);
		mr := jaynestown.InitMapReduce(5, 3, os.Args[3]);
		signal := make(chan bool, 1);
		canton.RunMagistrate(mr, signal);
		<- signal;
		status := make(chan bool, 1);
		log.Printf("Starting multiple iterations")
		go jaynestown.KeepDoingJobs(canton, mr, 50, status);
		<- status;
	} else {
		jaynestown.RunMudder(os.Args[2], os.Args[3], jaynestown.MapFunc,
			jaynestown.ReduceFunc);
	}
}
